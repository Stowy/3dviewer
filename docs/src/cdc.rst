Cahier des charges
==================

Objectif
--------

Il existe beaucoup de programmes qui permettent d’afficher des modèles 3D.
Cela dit, j'aimerais beaucoup explorer l’aspect mathématique de la projection d’objets 3D sur un plan 2D.
Dans le cadre de ce projet, j’effectuerai une application qui permettra de visualiser des modèles 3D dans une application Windows Form.

Le but de ``3D Viewer`` est donc de pouvoir charger un modèle 3D stocké au format ``.ply`` et de l'affiché.
Le modèle est ensuite affiché dans une scène ou il est possible de charger plusieurs autres modèles.
Ces modèles sont également éclairés par une ou plusieurs sources de lumières.
Les scènes ou sont disposé les modèles peuvent être enregistrés afin d'être chargées par la suite.

Prérequis
----------

* Connaissances en algèbre linéaire
* Savoir faire de la POO en ``C#``
* Savoir utiliser les ``Windows Form``

Spécifications
---------------

Chargement des modèles 3D
^^^^^^^^^^^^^^^^^^^^^^^^^^

*  3D Viewer sera capable de charger des modèles 3D dans une scène.

*  Ces modèles 3D sont au format ``.ply``.

*  Ils peuvent être chargés à l’aide d’un menu qui se trouve dans la
   partie supérieure de l’application.

*  Ce menu est un MenuStrip. Le chargement sera dans "File -> Load 3D
   Model...".

*  Seul un modèle 3D peut être chargé à la fois. Cela dit, plusieurs
   peuvent se trouver dans la scène en même temps.

*  Une fois le modèle chargé il se place au point d’origine (le centre)
   de la scène et peut maintenant être transformé par l’utilisateur.

Gestion des scènes
^^^^^^^^^^^^^^^^^^^

*  Les scènes sont un endroit où l’utilisateur peut ajouter des modèles
   et leur faire subir des transformations diverses.

*  Il est possible d’ajouter plusieurs objets dans une scène.

*  Chaque objet peut être modifié individuellement.

*  La scène contient une caméra qui peut être bougée afin de changer
   l’angle de vue de celle-ci.

*  Il est possible de sauvegarder les scènes avec toutes les
   informations qu’elles contiennent (par exemple: positions des modèles
   et de la caméra). La sauvegarde se faire avec ``ctrl+s`` ou dans
   "File -> Save Scene...".

*  Les scènes sont stockées au format ``.json``.

*  Il est également possible de charger une scène dans l’application.
   Celle-ci doit avoir été exportée par l’application au format
   ``.json`` précédemment.

*  Si l’utilisateur essaie de charger une scène alors que la scène
   active n’est pas sauvegardée, l’application propose à l’utilisateur
   de sauvegarder celle-ci.

Différence en objet et modèles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*  Une scène contient une liste d’objet.

*  Un objet peut être trois choses :

   *  Un modèle 3D

   *  Une caméra

   *  Une source de lumière

*  Les objets peuvent tous subir des transformations.

Éclairage
^^^^^^^^^^

Il sera également possible d’ajouter une source de lumière dans les
scènes. Elles pourront être déplacées afin d’illuminer un objet à un
angle différent. Cette lumière impactera la façon dont l’objet sera
rendu afin de simuler l’effet de la lumière sur celui-ci comme illustré
dans la figure ci-desous. Cette façon d’éclairer
s’appelle *ombrage plat* (ou *flat shading* en anglais).

.. figure:: ./img/flatShading.png
   :alt: Illustration de l’ombrage plat.
   :scale: 60%
   :align: center

   Illustration de l’ombrage plat.

Liste des transformations possibles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*  Translation

*  Rotation

*  Homothétie

Ces transformations pourront être appliquées à chaque objet de la scène,
à l’exception de l’homothétie pour la caméra et de l’homothétie et de la
rotation pour la source de lumière.

User Stories
^^^^^^^^^^^^

#. En tant que user je veux pouvoir charger un modèle 3D au format
   ``.ply``.

#. En tant que user je veux que le modèle 3D se trouve à l’origine de la
   scène quand je l’ajoute.

#. En tant que user je veux pouvoir déplacer mon modèle 3D.

#. En tant que user je veux pouvoir tourner mon modèle 3D.

#. En tant que user je veux pouvoir agrandir ou rétrécir mon modèle 3D.

#. En tant que user je veux pouvoir déplacer et tourner la caméra de la
   scène.

#. En tant que user je veux pouvoir déplacer la source de lumière.

#. En tant que user je veux pouvoir sauvegarder la scène.

#. En tant que user je veux pouvoir charger une scène.

#. En tant que user je veux pouvoir sélectionner un objet pour lui faire
   subir des transformations.

Installation
^^^^^^^^^^^^

L'application peut être téléchargée depuis la page release du ``GitLab`` du projet.
Elle sera sous la forme d'un ``.exe`` qui pourra être lancé sans installateur.

Mindmap
-------

.. figure:: ./img/mindmap.png
   :alt: Mindmap du projet.
   :align: center

   Mindmap du projet.

Méthodologie
-------------

Ce projet utilise la méthodologie Scrum. Le travail de diplôme étant séparé par des évaluations intermédiaires, je les utiliserais comme fin de sprint.

Livrables
---------

#. Mindmap
#. Planning
#. Cahier des charges
#. Rapport de projet
#. Manuel utilisateur
#. Journal de travail
#. Poster
#. Résumé / Abstract

Contact
------------

.. list-table::
   :header-rows: 1
   :align: center

   * - Status
     - Prénom
     - Nom
     - Couriel
   * - Éleve
     - Fabian
     - Huber
     - fabian.hbr@eduge.ch
   * - Maître d'apprentissage
     - Pascal
     - Bonvin
     - pascal.bonvin@edu.ge.ch

Dates importantes
-----------------

-  **Lundi 19 avril 2021** : Début du travail de diplôme

-  **Vendredi 30 avril 2021** : Évaluation intermédiaire 1

-  **Vendredi 14 mai 2021** : Rendu du rapport intermédiaire + poster

-  **Vendredi 14 mai 2021** : Rendu version intermédiaire du résumé et
   de l’abstract (pour conseils par l’enseignant d’anglais)

-  **Lundi 17 mai 2021** : Évaluation intermédiaire 2

-  **Jeudi 20 mai 2021 après-midi** : Soirée poster : amis, famille,
   CFC, experts

   -  **14h00** : Visite des classes de 1re année (cf. SG)

   -  **16h30** : Amis, famille, experts, enseignants Tech ES…

   -  **18h00** : Fin de la soirée poster

-  **Lundi 31 mai 2021** : Évaluation intermédiaire 3

-  **Vendredi 11 juin 2021** : Rendu du travail avant 12h00

-  **Jeudi 17 juin 2021** : Défenses à blanc + harmonisation des notes

-  **Lundi 21 juin ou mardi 22 juin 2021** : Défenses de diplôme

