*********************
Analyse fonctionnelle
*********************

Présentation de l'interface
============================

Lorsque l'utilisateur lance l'application, ceci est la fenêtre qui s'affiche :

.. figure:: ./img/launch3DViewer.png
   :alt: Fenêtre de 3D Viewer.
   :scale: 60%
   :align: center

   Fenêtre de 3D Viewer.

L'image au centre est le rendu du moteur 3D.
Au centre (0, 0, 0) de la scène se trouvent les axes du repère.

En haut à gauche de ce rendu se trouve la position de la caméra dans le monde et sa rotation.

.. figure:: ./img/cameraInfo.png
   :alt: Informations de la camera.
   :align: center

   Informations de la camera.

Au-dessus de ce texte se trouve le bouton pour sauvegarder l'image qui est actuellement rendue.

Tout à droite se trouvent les boutons qui permettent de choisir quelle démonstration on veut voir.

.. figure:: ./img/demoSelector.png
   :alt: Choix de la démo.
   :align: center

   Choix de la démo.

Le bouton "Load Model" n'est visible que durant la démo "Model Demo".
Il permet de choisir le modèle qu'on veut afficher dans cette démo.

Les types de démonstrations sont :

* **Model Demo** : Démonstration de la lecture et affichage d'un modèle.
* **Backhoe Demo** : Démonstration de l'affichage d'une pelle mécanique animée.
* **Hand Demo** : Démonstration de l'affichage d'une main animée.

Les modèles qui peuvent être chargés doivent être au format ``.ply`` et exportés par Blender.
Cette opération est expliquée plus tard.

En bas, se trouvent différent boutons pour choisir ce que l'on veut voir dans le rendu.

.. figure:: ./img/downButtons.png
   :alt: Boutons d'en bas.
   :align: center

   Boutons d'en bas.

Le bouton "Color" permet de changer la couleur de la lumière qui est appliquée quand le modèle est affiché avec la case "Fill" cochée.
Par défaut, la lumière est blanche.

.. figure:: ./img/whiteLight.png
   :alt: Lumière blanche.
   :scale: 60%
   :align: center

   Lumière blanche.

Voici ce que ça donne avec une lumière bleue par exemple

.. figure:: ./img/blueLight.png
   :alt: Lumière bleue.
   :scale: 60%
   :align: center

   Lumière bleue.

La case "Show normals" permet d'afficher ou non les normales des faces.

.. figure:: ./img/normalsShown.png
   :alt: Affichage des normales.
   :scale: 60%
   :align: center

   Affichage des normales.

La case "fill" permet de remplir les faces du modèle avec l'éclairage.

La case "wire" permet d'afficher le "Wireframe" (fil de fer) du modèle. Il est vert.

La case "timer" permet d'activer ou de désactiver le rafraichissement automatique de l'image.
Lorsque cette case n'est plus cochée, le bouton "Next Frame" peut être utilisé pour afficher l'image suivante.

Le premier drop down box permet de choisir le type de backface culling (cacher les faces qui font dos à la caméra).

* **None** : Aucun backface culling.
* **Normals** : Regarde l'orientation des normales par rapport à la caméra. Marche bien pour une projection orthographique, mais pas perspective.
* **Clockwise** Regarde si les sommets des faces projetés sont dans le sens des aiguilles d'une montre ou pas. Marche avec tous les types de projections.

.. figure:: ./img/cullingNormals.png
   :alt: Backface culling avec les normales.
   :scale: 60%
   :align: center

   Backface culling avec les normales.

.. figure:: ./img/cullingClockwise.png
   :alt: Backface culling avec l'orientation des faces projetées.
   :scale: 60%
   :align: center

   Backface culling avec l'orientation des faces projetées.

Le deuxième drop down box permet de choisir le type de projection.

* **Perspective** : Les objets qui sont plus éloignés sont plus petits.
* **Orthographic** : L'objet ne rétrécit pas en fonction de la distance et garde sa "vraie" forme.

.. figure:: ./img/orthographicProj.png
   :alt: Projection orthographique.
   :scale: 60%
   :align: center

   Projection orthographique.

Pour finir, le slide-bar tout à droite permet de changer le "scale" (homothétie) de l'objet présent sur la scène.

Création des fichiers ``.ply``
===============================

Prérequis
----------

* Avoir un modèle 3D compatible avec Blender

Importer le modèle dans Blender
---------------------------------

.. note:: Vous pouvez aussi créer votre modèle dans Blender si vous le souhaitez.

#. Ouvrez blender et supprimez le cube par défaut avec la touche "Del".
#. Allez dans "File -> Import" puis choississez le format du modèle voulu.
#. Selectionnez votre fichier.

Exporter le modèle en ``.ply``
-------------------------------

#. Selectionnez le modèle en cliquant dessus.
#. Allez dans "File -> Export -> Stanford (.ply)".
#. Paramétrez l'export comme ceci : 
      .. figure:: ./img/blenderSettings.png
         :alt: Paramètres Blender.
#. Cliquez sur "Export PLY".
