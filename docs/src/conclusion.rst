**********
Conclusion
**********

Résultat
=========

Mon projet s'est un petit peu éloigné du cahier des charges durant le développement.
En effet, j'ai plutôt choisi d'utiliser l'application comme une démonstration de ce qui est possible avec le moteur 3D plutôt que de pouvoir interagir avec toutes les fonctionnalités de celui-ci. Aussi, si j'avais voulu intégrer toutes les features du cahier des charges, je n'aurais pas eu le temps.

Il manque : 

* Pouvoir déplacer les objets. Il faudrait avoir une vue de l'arbre des entités pour les sélectionner, un peu comme dans Unity.
* Avoir plusieurs sources de lumière et pouvoir les déplacer (donc pas attachée à la caméra).
* Sérialisation et désérialisation de la scène en ``.json``.

Améliorations
==============

Il y a quelques bugs qui restent, notamment : 

* Quand une face se trouve pile-poil face à la caméra, elle devient noire.
* Quand on éclaire les faces avec une autre couleur que blanc, certaines sont quand même éclairées en blanc.

On pourrait également améliorer ces points :

* Être plus souple lors de la lecture du ``.ply``.
* Faire des tests unitaires.
* Utiliser une librairie graphique pour améliorer les performances.
* Pouvoir bouger la caméra avec la souris.
* Charger d'autres formats 3D (``.fbx``, ``.obj``, etc).
* Afficher une texture sur les modèles.

Bilan personnel
===============

Je suis très content du travail effectué au bout de ces 9 semaines.
Notamment due au fait que j'aimerais travailler dans l'industrie des jeux vidéos, et la 3D est une bonne compétence à avoir.
Aussi, je vais intégrer la SAE Institue en Games Programming l'année prochaine, ou je ferais aussi beaucoup de 3D, ce projet m'a donc permis de prendre de l'avance sur ce sujet.

De plus, j'ai vraiment beaucoup aimé apprendre touts les éléments mathématiques nécéssaires à ce projet.
Cela dit, je n'avais pas vraiment le niveau mathématique au début du projet et il faudrait qu'il soit aussi bien meilleur pour pouvoir continuer sur des éléments plus complexes.

Remerciements
=============

Merci à M. Bonvin de m'avoir suivi durant ce projet et a Gawen, Jonathan, David et Lorenzo pour avoir été des camarades de classe cool.