*********************
Étude d'opportunité
*********************

Il existe beaucoup de programmes qui permettent d'afficher de la 3D, 
mais ce n'est pas très grave, par ce que le but de mon projet est vraiment de découvrir l'aspect mathématique derrière ce sujet.
Malgré ça, je vais quand même lister quelques programmes qui permettent de le faire afin de pouvoir comparer mon projet à ceux-ci.

Unity
=====

Unity est un moteur qui permet de faire des jeux 2D et 3D en C#.
Il est assez populaire, car il est gratuit pour les personnes seules et qu'il est assez facile d'utilisation.

Unreal Engine
=============

L'Unreal Engine est un moteur de jeux qui permet également de faire de la 2D et de la 3D.
Il est possible d'utiliser du C++ ou un système de blueprint afin de coder des jeux.
Il a des capacités graphiques plus avancées qu'Unity et est globalement plus complexe.

Blender
=======

Blender est un logiciel de modélisation 3D qui permet également de faire des rendus et de l'animation.
Il possède deux moteurs de rendus principaux : Cycles et Eevee.

Cycles est le moteur le plus réaliste.
Il essaie vraiment d'imiter le vrai mouvement de la lumière.
Tandis que Eevee fait des rendus plus rapides qui sont plus comparables à ce que ferait un moteur comme Unreal Engine.

3D Viewer
=========

Windows 10 possède une application intégrée afin de pouvoir visualiser des modèles 3D.
Malheureusement, elle possède le même nom que mon application et je m'en suis rendu compte trop tard.
Elle peut afficher des modèles avec des textures et des animations et à également une gestion de la lumière avec des ombres portées.

Comparaison avec 3D Viewer
==========================

3D Viewer ne possède pas les mêmes capacités que tous les autres programmes dans cette liste, et globalement c'est voulu.
La chose qui est commune dans tous ces projets, c'est qu'ils utilisent la carte graphique, ce qui est quasiment obligatoire quand on veut faire de la 3D de nos jours.
Au début, mon projet devait se faire avec OpenGL, mais M. Bonvin m'a recommandé de ne pas l'utiliser afin d'être sûr que je comprenais vraiment tout ce que je faisais.
C'est pour ça que mon projet n'a pas de choses comme les textures, ombres, etc.