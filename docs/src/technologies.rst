Technologies utilisées
============================

Application
-----------

C#
^^^
``C#`` (lu C Sharp, comme la note de musique en anglais), est un langage de programmation orienté objet commercialisé par Microsoft depuis 2002.
Il est régulièrement mis à jour, sa dernière version est la 9.0. Sortie en septembre 2020, c'est celle-ci que j'ai utilisée pour cette application.

J'ai choisi ce langage, car c'est celui où je suis le plus à l'aise. De plus, son intégration avec ``Visual Studio`` rend son utilisation très agréable.

Le but de cette application est de faire un maximum de choses moi même, je n'ai donc pas utilisé de librairies externes autres que celles fournies par ``C#`` et ``.NET``.


StyleCop.Analysers
^^^^^^^^^^^^^^^^^^
`StyleCop.Analysers <https://github.com/DotNetAnalyzers/StyleCopAnalyzers>`__ est une librairie qui permet d'enforcer un style de programmation dans un projet ``C#``.
L'objectif est que ma manière de programmer reste consistante durant toute la durée du projet afin de rendre le code le plus propre possible.

Documentation
-------------

Sphinx
^^^^^^

`Sphinx <https://www.sphinx-doc.org/en/master/>`__ est un outil qui permet de générer de la documentation.
C'est l'outil que ``readthedocs`` utilises. La documentation s'écrit en ``reStructuredText`` mais peut également être écrite en ``Markdown``.
J'ai personellement opté pour le premier choix.
Ce programme à d'abord été créé pour la documentation de ``Python``.

Doxygen
^^^^^^^

`Doxygen <https://www.doxygen.nl/>`__ est un logiciel qui permet de générer un documentation à partir de code source.
Il peut lire les commentaires XML que visual studio permet d'écrire.
J'utilise Doxygen pour générer des fichiers XML que Doxyrest lira.

Doxyrest
^^^^^^^^

`Doxyrest <https://github.com/vovkos/doxyrest>`__ est une logiciel qui permet de convertir la sortie XML de Doxygen en documents reStructuredText.
J'utilise ceci afin que la documentation Doxygen puisse être lisible sur readthedocs.
Le seul problème de ce système est que du coup je n'ai plus les diagrammes de classes.

Typora
^^^^^^

`Typora <https://www.sphinx-doc.org/en/master/>`__ est un éditeur de ``Markdown`` disponible sur Windows, macOS et Linux.
Il permet d'éditer un document à la manière "What You See Is What You Get".
C'est l'outil que j'utilise pour écrire le logbook.

Pandoc
^^^^^^

`Pandoc <https://pandoc.org/>`__ est un outil de conversion d'un format de balisage à un autre.
Il est également intégré à Typora afin de pouvoir exporter les documents ``Markdown`` dans différents formats.