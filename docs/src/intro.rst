Introduction
============

Résumé
--------
Il existe beaucoup de programmes qui peuvent afficher des modèles 3D.
Cela dit, j'aimerais beaucoup explorer l’aspect mathématique de la projection d’objets 3D sur un plan 2D.
Dans le cadre de ce projet, j’effectuerai une application qui permettra de visualiser des modèles 3D dans une Windows Form.

Le but de ``3D Viewer`` est donc de pouvoir charger un modèle 3D stocké au format ``.ply`` et de l'afficher.
Le modèle est ensuite placé dans une scène où il est possible de charger plusieurs autres modèles.
Ces modèles sont également éclairés par une ou plusieurs sources de lumière.
Les scènes où sont disposés les modèles peuvent être enregistrées afin d'être chargées par la suite.

Abstract
--------

There are many programs that can display 3D models.
That said, I'd really like to explore the mathematical aspect of projecting 3D objects on a 2D plane.
During this project I'll create an application that will be able to visualize 3D models in a Windows From.

The goal of ``3D Viewer`` is to be able to load a 3D model stored in the ``.ply`` format and to show it.
The model is then shown in a scene where it is possible to load other models.
These models are also lit up by one or multiple light sources.
The scenes where the models are laid out can be saved to be loaded in the future.

Poster
------

.. figure:: ./img/Poster3DViewer.png
   :alt: Poster de 3D Viewer.
   :align: center

   Poster de 3D Viewer.

Liens utiles
------------

* `GitLab <https://gitlab.com/Stowy/3dviewer>`_
* `Trello <https://trello.com/b/nnWVZ4GT/3dviewer>`_
* `Read the Docs <3dviewer.readthedocs.io/>`_