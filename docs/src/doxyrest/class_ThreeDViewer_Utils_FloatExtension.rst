.. index:: pair: class; ThreeDViewer.Utils.FloatExtension
.. _doxid-class_three_d_viewer_1_1_utils_1_1_float_extension:

class ThreeDViewer.Utils.FloatExtension
=======================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Class that contains all the extensions for the float class. :ref:`More...<details-class_three_d_viewer_1_1_utils_1_1_float_extension>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class FloatExtension
	{
	public:
		// methods
	
		static float :ref:`Map<doxid-class_three_d_viewer_1_1_utils_1_1_float_extension_1abcb1bf488b9e5f5d0ac3c5d412ef8d1c>`(this float s, float a1, float a2, float b1, float b2);
	};
.. _details-class_three_d_viewer_1_1_utils_1_1_float_extension:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Class that contains all the extensions for the float class.

Methods
-------

.. index:: pair: function; Map
.. _doxid-class_three_d_viewer_1_1_utils_1_1_float_extension_1abcb1bf488b9e5f5d0ac3c5d412ef8d1c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static float Map(this float s, float a1, float a2, float b1, float b2)

Linearly maps the value s that's in the range [a1, a2] to the range [b1, b2].



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- s

		- The value to map.

	*
		- a1

		- The first limit of the range that s belongs to.

	*
		- a2

		- The second limit of the range that s belongs to.

	*
		- b1

		- The first limit of the ouput range.

	*
		- b2

		- The second limit of the ouput range.



.. rubric:: Returns:

The mapped value in the [b1, b2] range.

