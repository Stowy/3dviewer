.. index:: pair: enum; CullingMode
.. _doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59:

enum ThreeDViewer.CullingMode
=============================

Overview
~~~~~~~~

Defines the way that backface will be culled. :ref:`More...<details-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59>`

.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	enum CullingMode
	{
	    :ref:`None<doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59a6adf97f83acf6453d4a6a4b1070f3754>`      = 0,
	    :ref:`Normals<doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59a4ab971a51f0335cbf8d9c2c65d379e99>`   = 1,
	    :ref:`Clockwise<doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59aba360a794737bcc8657a5b6e870d7ba8>` = 2,
	};

.. _details-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Defines the way that backface will be culled.

Enum Values
-----------

.. index:: pair: enumvalue; None
.. _doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59a6adf97f83acf6453d4a6a4b1070f3754:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	None

No backface culling.

.. index:: pair: enumvalue; Normals
.. _doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59a4ab971a51f0335cbf8d9c2c65d379e99:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Normals

Cull with the normals of the face.

.. index:: pair: enumvalue; Clockwise
.. _doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59aba360a794737bcc8657a5b6e870d7ba8:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Clockwise

Cull by checking if the faces are in clockwise order or not once projected.

