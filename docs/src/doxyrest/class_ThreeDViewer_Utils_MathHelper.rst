.. index:: pair: class; ThreeDViewer.Utils.MathHelper
.. _doxid-class_three_d_viewer_1_1_utils_1_1_math_helper:

class ThreeDViewer.Utils.MathHelper
===================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Class that contains methods to help with mathematical things. :ref:`More...<details-class_three_d_viewer_1_1_utils_1_1_math_helper>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class MathHelper
	{
	public:
		// fields
	
		static readonly double :ref:`PiOver2<doxid-class_three_d_viewer_1_1_utils_1_1_math_helper_1ad7b6c73b5c0194c0e250949dd7951849>` = Math.PI / 2;

		// methods
	
		static double :ref:`RadToDeg<doxid-class_three_d_viewer_1_1_utils_1_1_math_helper_1a48784f6736302d9d6b784233fca39a9a>`(double radians);
		static double :ref:`DegToRad<doxid-class_three_d_viewer_1_1_utils_1_1_math_helper_1af6acc51ea4c939578ed176ea476b77fa>`(double degrees);
	};
.. _details-class_three_d_viewer_1_1_utils_1_1_math_helper:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Class that contains methods to help with mathematical things.

Fields
------

.. index:: pair: variable; PiOver2
.. _doxid-class_three_d_viewer_1_1_utils_1_1_math_helper_1ad7b6c73b5c0194c0e250949dd7951849:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly double PiOver2 = Math.PI / 2

PI divided by two.

Methods
-------

.. index:: pair: function; RadToDeg
.. _doxid-class_three_d_viewer_1_1_utils_1_1_math_helper_1a48784f6736302d9d6b784233fca39a9a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double RadToDeg(double radians)

Converts radians to degree.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- radians

		- Radians to convert.



.. rubric:: Returns:

The angle in degree.

.. index:: pair: function; DegToRad
.. _doxid-class_three_d_viewer_1_1_utils_1_1_math_helper_1af6acc51ea4c939578ed176ea476b77fa:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double DegToRad(double degrees)

Converts degrees to radians.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- degrees

		- Degress to convert.



.. rubric:: Returns:

The angle in radians.

