.. index:: pair: class; ThreeDViewer.Mathematics.NormalizeVectorException
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception:

class ThreeDViewer.Mathematics.NormalizeVectorException
=======================================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Exception that can happen during the normalization of a vector. :ref:`More...<details-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class NormalizeVectorException: Exception
	{
	public:
		// methods
	
		:ref:`NormalizeVectorException<doxid-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception_1a205a4972e67b723c90226ccd5be7ed38>`();
		:ref:`NormalizeVectorException<doxid-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception_1a066e12978062d00f847912c7d3a12e78>`(string message);
	};
.. _details-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Exception that can happen during the normalization of a vector.

Methods
-------

.. index:: pair: function; NormalizeVectorException
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception_1a205a4972e67b723c90226ccd5be7ed38:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	NormalizeVectorException()

Initializes a new instance of the NormalizeVectorException class.

.. index:: pair: function; NormalizeVectorException
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception_1a066e12978062d00f847912c7d3a12e78:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	NormalizeVectorException(string message)

Initializes a new instance of the NormalizeVectorException class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- message

		- Message to display in the exception.

