.. index:: pair: class; ThreeDViewer.FrmMain
.. _doxid-class_three_d_viewer_1_1_frm_main:

class ThreeDViewer.FrmMain
==========================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Main form of the app. :ref:`More...<details-class_three_d_viewer_1_1_frm_main>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class FrmMain: Form
	{
	public:
		// methods
	
		:ref:`FrmMain<doxid-class_three_d_viewer_1_1_frm_main_1a1ac1fbc0c540a242c32acd909fb7b0b2>`();
	};
.. _details-class_three_d_viewer_1_1_frm_main:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Main form of the app.

Methods
-------

.. index:: pair: function; FrmMain
.. _doxid-class_three_d_viewer_1_1_frm_main_1a1ac1fbc0c540a242c32acd909fb7b0b2:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	FrmMain()

Initializes a new instance of the :ref:`FrmMain <doxid-class_three_d_viewer_1_1_frm_main>` class.

