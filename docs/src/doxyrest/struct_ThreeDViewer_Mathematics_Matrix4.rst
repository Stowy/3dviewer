.. index:: pair: struct; ThreeDViewer.Mathematics.Matrix4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4:

struct ThreeDViewer.Mathematics.Matrix4
=======================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents a matrix of size 4x4. :ref:`More...<details-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	struct Matrix4: IEquatable< Matrix4 >
	{
		// fields
	
		static const int :ref:`Size<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7cc35b7c53944358836a5b762eb6eb4d>` = 4;
		static readonly Matrix4 :ref:`Identity<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a5fd96eff998bf3fafbf0905a02093d57>` = new(1, 0, 0, 0,             0, 1, 0, 0,             0, 0, 1, 0,             0, 0, 0, 1);
		static readonly Matrix4 :ref:`ProjectionOrthoX<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ad8e64bfb65f8ac3b902aaf9bbfd6c44c>` = new(0, 0, 0, 0,             0, 1, 0, 0,             0, 0, 1, 0,             0, 0, 0, 1);
		static readonly Matrix4 :ref:`ProjectionOrthoY<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a3cbe38f34b8ed372692dfc6bca5468c1>` = new(1, 0, 0, 0,             0, 0, 0, 0,             0, 0, 1, 0,             0, 0, 0, 1);
		static readonly Matrix4 :ref:`ProjectionOrthoZ<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a095adb906e2c4038e6bcfc9af7537d2f>` = new(1, 0, 0, 0,             0, 1, 0, 0,             0, 0, 0, 0,             0, 0, 0, 1);
		static readonly Matrix4 :ref:`Zero<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adf534f81e86796dea34802f9e1e0467d>` = new(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

		// properties
	
		double[,] :ref:`Values<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a5c982f2e008a818247c3eb43a3e8686c>`;
		double :ref:`V00<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a28dc8b38b3f41cb9dab48c9b14211eaf>`;
		double :ref:`V01<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aadcfc3a6cf24d47aad9e08b04a561a6d>`;
		double :ref:`V02<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a5617f97177d2550611da17f949e69429>`;
		double :ref:`V03<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a0b311ac1e3adda35fcd37d3839eaa908>`;
		double :ref:`V10<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a92f8cdcb273f522950d77be7c2b8a252>`;
		double :ref:`V11<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a00122148451014044b3afc5e869cad21>`;
		double :ref:`V12<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a745ec8fafb9a568dc350fcfdb371f378>`;
		double :ref:`V13<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aaeb2073afa4a16f63cad3b735e11efa9>`;
		double :ref:`V20<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adb52bcc8edca906c476be84f484b3285>`;
		double :ref:`V21<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ae3457b2a6c6a1c78c31438c06d25a12e>`;
		double :ref:`V22<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7dfe5f32a230fb58b6ccdae2778affd0>`;
		double :ref:`V23<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1acdf5da118744fa49ede68d0cbcbde159>`;
		double :ref:`V30<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a313c0ab7e33a7407dba251ce9d2c8888>`;
		double :ref:`V31<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9bbacb2e32e4840c0067ff1c8ea136b1>`;
		double :ref:`V32<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ad12b3b66cabd05acd74fe1a3f72e83f6>`;
		double :ref:`V33<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a789efd1fa53a0b8e22c849928590f1a0>`;
		double :ref:`Determinant<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9df03e18934d2e32d1051f29c1ca3286>`;
		Matrix4 :ref:`Normalized<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a53814d602b1f207ae1caf5ded9b1f9fb>`;
		double :ref:`Trace<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a1f64b84cec2a7200de102ca7501c02dd>`;
		double :ref:`this[int rowIndex, int columnIndex]<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aee48ffb982877224be0e11681d4bcd8d>`;

		// methods
	
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a12d43777377f4367bd4dda919f4bd237>`(double values[,]);
	
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a59b677aac0986b94cc2298b8c2537567>`(
			double value00,
			double value01,
			double value02,
			double value03,
			double value10,
			double value11,
			double value12,
			double value13,
			double value20,
			double value21,
			double value22,
			double value23,
			double value30,
			double value31,
			double value32,
			double value33
			);
	
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7635cbf1ae01742e5aa3eb0f1dcab114>`(Matrix4 matrix);
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a23896b0f4496b63e534fe09b4bb7a1c2>`(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] values, bool isColums = true);
		Matrix4 :ref:`Transpose<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ae0f4add6ea77dc817e1f9fb4fd64b26b>`();
		override bool :target:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ac34775f8455e8dfff61990ae9eafe67d>`(object other);
		bool :target:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aef83a6cd3dcecf24dadea314da4a594a>`(Matrix4 other);
		bool :ref:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adf8b33806177fc8a2c855e304cb3e851>`(object other, double tolerance);
		bool :ref:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9d1f3dc070c0e90b62964bd9556183eb>`(Matrix4 other, double tolerance);
		override int :target:`GetHashCode<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a1a59339ab816ef61655b7beb2fdb9180>`();
		Matrix4 :ref:`Invert<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a18da053c65ff5d8f190376a502ffc34c>`();
		override string :target:`ToString<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1acbf40e4c93fe3a5f6f6ab5497b7fc333>`();
		static Matrix4 :ref:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a8b0740a33f4942966e878f258a0856ad>` (Matrix4 mat, double scalar);
		static Matrix4 :ref:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adfa485b3800dd1f6f622c61eff203f09>` (double scalar, Matrix4 mat);
		static Matrix4 :ref:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ad86aecd28afa089c42b1888bcf1f1cca>` (Matrix4 mat, double scalar);
		static Matrix4 :ref:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a2a685ea5aebea8e8137c10e8c0d88270>` (double scalar, Matrix4 mat);
		static Matrix4 :ref:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1afc1f76d702217f6db0db2f05225c59fc>` (Matrix4 mat, double scalar);
		static Matrix4 :ref:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9f4530739e83c48cec28cad4322b8023>` (double scalar, Matrix4 mat);
		static Matrix4 :ref:`operator/<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a113635bd69779f66e8173d0632248bb5>` (Matrix4 mat, double scalar);
		static Matrix4 :ref:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a1e47412857257850a420384ab9e115ce>` (Matrix4 mat);
		static Matrix4 :ref:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a269e3eb3d9ba921c89c4752a9091865c>` (Matrix4 left, Matrix4 right);
		static Matrix4 :ref:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a0f3a42a8620661661791c4545ac221ab>` (Matrix4 left, Matrix4 right);
		static Matrix4 :ref:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a8bc4c17045da1d2c772e4affbc2f2281>` (Matrix4 left, Matrix4 right);
		static bool :ref:`operator ==<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a3d1155eea951b068b4d8f824fe8d17e5>` (Matrix4 left, Matrix4 right);
		static bool :ref:`operator !=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a369c38ece3198d83580ae614557ae534>` (Matrix4 left, Matrix4 right);
		static Matrix4 :ref:`CreateRotationX<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a2bc6c638669240a86cc5fb51f21ac3aa>`(double angle);
		static Matrix4 :ref:`CreateRotationY<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aa524b2db5e2f2016e0f516c168ea6988>`(double angle);
		static Matrix4 :ref:`CreateRotationZ<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aaa4ea4836bb7447fcd646bc860765cc5>`(double angle);
		static Matrix4 :ref:`CreateRotation<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ac9ca8247784f51ebd2da20160ff7fb2f>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` rotation);
		static Matrix4 :ref:`CreateRotationAbitraryAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a04e51d56929871a43cc17800723017df>`(:ref:`Axis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis>` rotationAxis, double angle);
		static Matrix4 :ref:`CreateScale<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a960e5aa1083e3b1f3c5e9d076822ce4d>`(double scaleFactor);
	
		static Matrix4 :ref:`CreatePerspectiveOffCenter<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a43d78f9c6272ade9d96a665c49017cfa>`(
			double left,
			double right,
			double bottom,
			double top,
			double depthNear,
			double depthFar
			);
	
		static Matrix4 :ref:`CreatePerspectiveFieldOfView<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ac8c7ff849b6f3f65b280869e852459e3>`(
			double fovy,
			double aspect,
			double depthNear,
			double depthFar
			);
	
		static Matrix4 :ref:`CreateTranslation<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aecdf94eae4505ec69c3bab89bac04f4f>`(double x, double y, double z);
		static Matrix4 :ref:`CreateTranslation<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adb2427941c84272ef1e93e07611af316>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` vector);
		static Matrix4 :ref:`LookAt<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7e90a275e17eff3a0aee0f04bb315714>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` eye, :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` target, :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` up);
		static Matrix4 :ref:`Transpose<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7324860705ad973a858d8379888ea90a>`(Matrix4 mat);
		static Matrix4 :ref:`Normalize<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a41bebd15d2bfe0b5f5b6ab5a19680c5f>`(Matrix4 mat);
	};
.. _details-struct_three_d_viewer_1_1_mathematics_1_1_matrix4:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents a matrix of size 4x4.

Every coordinates are in [y, x] format (or [row, column]).

Fields
------

.. index:: pair: variable; Size
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7cc35b7c53944358836a5b762eb6eb4d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static const int Size = 4

Size of the matrix.

.. index:: pair: variable; Identity
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a5fd96eff998bf3fafbf0905a02093d57:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Matrix4 Identity = new(1, 0, 0, 0,             0, 1, 0, 0,             0, 0, 1, 0,             0, 0, 0, 1)

The identity matrix.

.. index:: pair: variable; ProjectionOrthoX
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ad8e64bfb65f8ac3b902aaf9bbfd6c44c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Matrix4 ProjectionOrthoX = new(0, 0, 0, 0,             0, 1, 0, 0,             0, 0, 1, 0,             0, 0, 0, 1)

Matrix for projection onto the x = 0 plane.

.. index:: pair: variable; ProjectionOrthoY
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a3cbe38f34b8ed372692dfc6bca5468c1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Matrix4 ProjectionOrthoY = new(1, 0, 0, 0,             0, 0, 0, 0,             0, 0, 1, 0,             0, 0, 0, 1)

Matrix for projection onto the y = 0 plane.

.. index:: pair: variable; ProjectionOrthoZ
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a095adb906e2c4038e6bcfc9af7537d2f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Matrix4 ProjectionOrthoZ = new(1, 0, 0, 0,             0, 1, 0, 0,             0, 0, 0, 0,             0, 0, 0, 1)

Matrix for projection onto the z = 0 plane.

.. index:: pair: variable; Zero
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adf534f81e86796dea34802f9e1e0467d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Matrix4 Zero = new(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

The zero matrix.

Properties
----------

.. index:: pair: property; Values
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a5c982f2e008a818247c3eb43a3e8686c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double[,] Values

Gets the values of the matrix.

.. index:: pair: property; V00
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a28dc8b38b3f41cb9dab48c9b14211eaf:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V00

Gets the value at 0, 0 on the matrix.

.. index:: pair: property; V01
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aadcfc3a6cf24d47aad9e08b04a561a6d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V01

Gets the value at 0, 1 on the matrix.

.. index:: pair: property; V02
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a5617f97177d2550611da17f949e69429:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V02

Gets the value at 0, 2 on the matrix.

.. index:: pair: property; V03
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a0b311ac1e3adda35fcd37d3839eaa908:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V03

Gets the value at 0, 3 on the matrix.

.. index:: pair: property; V10
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a92f8cdcb273f522950d77be7c2b8a252:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V10

Gets the value at 1, 0 on the matrix.

.. index:: pair: property; V11
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a00122148451014044b3afc5e869cad21:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V11

Gets the value at 1, 1 on the matrix.

.. index:: pair: property; V12
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a745ec8fafb9a568dc350fcfdb371f378:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V12

Gets the value at 1, 2 on the matrix.

.. index:: pair: property; V13
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aaeb2073afa4a16f63cad3b735e11efa9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V13

Gets the value at 1, 3 on the matrix.

.. index:: pair: property; V20
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adb52bcc8edca906c476be84f484b3285:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V20

Gets the value at 2, 0 on the matrix.

.. index:: pair: property; V21
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ae3457b2a6c6a1c78c31438c06d25a12e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V21

Gets the value at 2, 1 on the matrix.

.. index:: pair: property; V22
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7dfe5f32a230fb58b6ccdae2778affd0:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V22

Gets the value at 2, 2 on the matrix.

.. index:: pair: property; V23
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1acdf5da118744fa49ede68d0cbcbde159:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V23

Gets the value at 2, 3 on the matrix.

.. index:: pair: property; V30
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a313c0ab7e33a7407dba251ce9d2c8888:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V30

Gets the value at 3, 0 on the matrix.

.. index:: pair: property; V31
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9bbacb2e32e4840c0067ff1c8ea136b1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V31

Gets the value at 3, 1 on the matrix.

.. index:: pair: property; V32
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ad12b3b66cabd05acd74fe1a3f72e83f6:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V32

Gets the value at 3, 2 on the matrix.

.. index:: pair: property; V33
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a789efd1fa53a0b8e22c849928590f1a0:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double V33

Gets the value at 3, 3 on the matrix.

.. index:: pair: property; Determinant
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9df03e18934d2e32d1051f29c1ca3286:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Determinant

Gets the determinant of the matrix.

.. index:: pair: property; Normalized
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a53814d602b1f207ae1caf5ded9b1f9fb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4 Normalized

Gets the normalized matrix.

.. index:: pair: property; Trace
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a1f64b84cec2a7200de102ca7501c02dd:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Trace

Gets the trace of the matrix, the sum of the values along the diagonal axis.

.. index:: pair: property; this[int rowIndex, int columnIndex]
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aee48ffb982877224be0e11681d4bcd8d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double this[int rowIndex, int columnIndex]

Gets the value at the specified row and column.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- rowIndex

		- The index of the row.

	*
		- columnIndex

		- The index of the column.



.. rubric:: Returns:

The element at the given row and column index.

Methods
-------

.. index:: pair: function; Matrix4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a12d43777377f4367bd4dda919f4bd237:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4(double values[,])

Initializes a new instance of the :ref:`Matrix4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- values

		- Values of the matrix. Should be 4x4.

.. index:: pair: function; Matrix4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a59b677aac0986b94cc2298b8c2537567:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4(
		double value00,
		double value01,
		double value02,
		double value03,
		double value10,
		double value11,
		double value12,
		double value13,
		double value20,
		double value21,
		double value22,
		double value23,
		double value30,
		double value31,
		double value32,
		double value33
		)

Initializes a new instance of the :ref:`Matrix4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- value00

		- Value of the matrix in the 0, 0 position.

	*
		- value01

		- Value of the matrix in the 0, 1 position.

	*
		- value02

		- Value of the matrix in the 0, 2 position.

	*
		- value03

		- Value of the matrix in the 0, 3 position.

	*
		- value10

		- Value of the matrix in the 1, 0 position.

	*
		- value11

		- Value of the matrix in the 1, 1 position.

	*
		- value12

		- Value of the matrix in the 1, 2 position.

	*
		- value13

		- Value of the matrix in the 1, 3 position.

	*
		- value20

		- Value of the matrix in the 2, 0 position.

	*
		- value21

		- Value of the matrix in the 2, 1 position.

	*
		- value22

		- Value of the matrix in the 2, 2 position.

	*
		- value23

		- Value of the matrix in the 2, 3 position.

	*
		- value30

		- Value of the matrix in the 3, 0 position.

	*
		- value31

		- Value of the matrix in the 3, 1 position.

	*
		- value32

		- Value of the matrix in the 3, 2 position.

	*
		- value33

		- Value of the matrix in the 3, 3 position.

.. index:: pair: function; Matrix4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7635cbf1ae01742e5aa3eb0f1dcab114:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4(Matrix4 matrix)

Initializes a new instance of the :ref:`Matrix4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- matrix

		- Matrix struct to take the values from.

.. index:: pair: function; Matrix4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a23896b0f4496b63e534fe09b4bb7a1c2:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] values, bool isColums = true)

Initializes a new instance of the :ref:`Matrix4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` struct.

///



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- values

		- Values of the Matrix.

	*
		- isColums

		- Tells if the vector is a column vector or a row vector.

.. index:: pair: function; Transpose
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ae0f4add6ea77dc817e1f9fb4fd64b26b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4 Transpose()

Computes the transpose of this matrix.



.. rubric:: Returns:

The transpose of this matrix.

.. index:: pair: function; Equals
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adf8b33806177fc8a2c855e304cb3e851:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool Equals(object other, double tolerance)

Comparator within a tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- Matrix to compare with.

	*
		- tolerance

		- Tolerance to apply in the comparison.



.. rubric:: Returns:

True if two matrices are equal withing a tolerance.

.. index:: pair: function; Equals
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9d1f3dc070c0e90b62964bd9556183eb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool Equals(Matrix4 other, double tolerance)

Comparator within a tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- Matrix to compare with.

	*
		- tolerance

		- Tolerance to apply in the comparison.



.. rubric:: Returns:

True if two matrices are equal withing a tolerance.

.. index:: pair: function; Invert
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a18da053c65ff5d8f190376a502ffc34c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Matrix4 Invert()

Inverts this instance.

Taken from here : `https://github.com/opentk/opentk/blob/master/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1582 <https://github.com/opentk/opentk/blob/master/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1582>`__.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- InvalidOperationException

		- Thrown when the matrix is singular and cannot be inverted.



.. rubric:: Returns:

The inverted matrix.

.. index:: pair: function; operator+
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a8b0740a33f4942966e878f258a0856ad:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator + (Matrix4 mat, double scalar)

Adds a matrix and a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to add to.

	*
		- scalar

		- Scalar to add to the matrix.



.. rubric:: Returns:

The added matrix.

.. index:: pair: function; operator+
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adfa485b3800dd1f6f622c61eff203f09:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator + (double scalar, Matrix4 mat)

Adds a matrix and a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to add to.

	*
		- scalar

		- Scalar to add to the matrix.



.. rubric:: Returns:

The added matrix.

.. index:: pair: function; operator-
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ad86aecd28afa089c42b1888bcf1f1cca:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator - (Matrix4 mat, double scalar)

Substracts a matrix and a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to substract to.

	*
		- scalar

		- Scalar to substract to the matrix.



.. rubric:: Returns:

The substracted matrix.

.. index:: pair: function; operator-
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a2a685ea5aebea8e8137c10e8c0d88270:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator - (double scalar, Matrix4 mat)

Substracts a matrix and a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to substract to.

	*
		- scalar

		- Scalar to substract to the matrix.



.. rubric:: Returns:

The substracted matrix.

.. index:: pair: function; operator*
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1afc1f76d702217f6db0db2f05225c59fc:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator * (Matrix4 mat, double scalar)

Multiplies a matrix and a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to multiply.

	*
		- scalar

		- Scalar to multiply with the matrix.



.. rubric:: Returns:

The multiplied matrix.

.. index:: pair: function; operator*
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9f4530739e83c48cec28cad4322b8023:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator * (double scalar, Matrix4 mat)

Multiplies a matrix and a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to multiply.

	*
		- scalar

		- Scalar to multiply with the matrix.



.. rubric:: Returns:

The multiplied matrix.

.. index:: pair: function; operator/
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a113635bd69779f66e8173d0632248bb5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator/ (Matrix4 mat, double scalar)

Divides a matrix by a scalar.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to divide.

	*
		- scalar

		- Scalar to divide the matrix with.



.. rubric:: Returns:

The divided matrix.

.. index:: pair: function; operator-
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a1e47412857257850a420384ab9e115ce:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator - (Matrix4 mat)

Inverses the sign of a matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to inverse the sign of.



.. rubric:: Returns:

The sign inverted matrix.

.. index:: pair: function; operator+
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a269e3eb3d9ba921c89c4752a9091865c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator + (Matrix4 left, Matrix4 right)

Adds two matrices.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- left

		- The matrix on the left.

	*
		- right

		- The matrix on the right.



.. rubric:: Returns:

The added matrix.

.. index:: pair: function; operator-
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a0f3a42a8620661661791c4545ac221ab:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator - (Matrix4 left, Matrix4 right)

Substracts two matrices.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- left

		- The matrix on the left.

	*
		- right

		- The matrix on the right.



.. rubric:: Returns:

The substracted matrix.

.. index:: pair: function; operator*
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a8bc4c17045da1d2c772e4affbc2f2281:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 operator * (Matrix4 left, Matrix4 right)

Multiplies two matrices.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- left

		- The left matrix.

	*
		- right

		- The right matrix.



.. rubric:: Returns:

The dot product of the matrices.

.. index:: pair: function; operator==
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a3d1155eea951b068b4d8f824fe8d17e5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool operator == (Matrix4 left, Matrix4 right)

Check if two matrices are equal.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- left

		- The left matrix.

	*
		- right

		- The right matrix.



.. rubric:: Returns:

True if the matrices are equal.

.. index:: pair: function; operator!=
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a369c38ece3198d83580ae614557ae534:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool operator != (Matrix4 left, Matrix4 right)

Check if two matrices are not equal.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- left

		- The left matrix.

	*
		- right

		- The right matrix.



.. rubric:: Returns:

True if the matrices are not equal.

.. index:: pair: function; CreateRotationX
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a2bc6c638669240a86cc5fb51f21ac3aa:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateRotationX(double angle)

Builds a rotation matrix for a rotation around x-axis.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- angle

		- The counter clockwise angle in radians.



.. rubric:: Returns:

The rotation matrix.

.. index:: pair: function; CreateRotationY
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aa524b2db5e2f2016e0f516c168ea6988:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateRotationY(double angle)

Builds a rotation matrix for a rotation around y-axis.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- angle

		- The counter clockwise angle in radians.



.. rubric:: Returns:

The rotation matrix.

.. index:: pair: function; CreateRotationZ
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aaa4ea4836bb7447fcd646bc860765cc5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateRotationZ(double angle)

Builds a rotation matrix for a rotation around z-axis.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- angle

		- The counter clockwise angle in radians.



.. rubric:: Returns:

The rotation matrix.

.. index:: pair: function; CreateRotation
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ac9ca8247784f51ebd2da20160ff7fb2f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateRotation(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` rotation)

Builds a combined rotation matrix around the X, Y and Z axes. Angles in radians.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- rotation

		- Rotation that will be applied in the matrix.



.. rubric:: Returns:

The rotation matrix.

.. index:: pair: function; CreateRotationAbitraryAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a04e51d56929871a43cc17800723017df:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateRotationAbitraryAxis(:ref:`Axis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis>` rotationAxis, double angle)

Builds a rotation matrix around an arbitrary axis.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- rotationAxis

		- The arbitrary axis to rotate about.

	*
		- angle

		- The angle of the rotation.



.. rubric:: Returns:

The constructed rotation matrix.

.. index:: pair: function; CreateScale
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a960e5aa1083e3b1f3c5e9d076822ce4d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateScale(double scaleFactor)

Builds a scale matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- scaleFactor

		- The scaling factor for this matrix.



.. rubric:: Returns:

The scale matrix.

.. index:: pair: function; CreatePerspectiveOffCenter
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a43d78f9c6272ade9d96a665c49017cfa:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreatePerspectiveOffCenter(
		double left,
		double right,
		double bottom,
		double top,
		double depthNear,
		double depthFar
		)

Creates an perspective projection matrix.

Taken from here : `https://github.com/opentk/opentk/blob/082c8d228d0def042b11424ac002776432f44f47/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1024 <https://github.com/opentk/opentk/blob/082c8d228d0def042b11424ac002776432f44f47/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1024>`__.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- left

		- Left edge of the view frustum.

	*
		- right

		- Right edge of the view frustum.

	*
		- bottom

		- Bottom edge of the view frustum.

	*
		- top

		- Top edge of the view frustum.

	*
		- depthNear

		- Distance to the near clip plane.

	*
		- depthFar

		- Distance to the far clip plane.

	*
		- System.ArgumentOutOfRangeException

		- 
		  Thrown under the following conditions:
		  
		  * depthNear is negative or zero
		  
		  * depthFar is negative or zero
		  
		  * depthNear is larger than depthFar



.. rubric:: Returns:

A perspective projection matrix.

.. index:: pair: function; CreatePerspectiveFieldOfView
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1ac8c7ff849b6f3f65b280869e852459e3:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreatePerspectiveFieldOfView(
		double fovy,
		double aspect,
		double depthNear,
		double depthFar
		)

Creates a perspective projection matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- fovy

		- Angle of the field of view in the y direction (in radians).

	*
		- aspect

		- Aspect ratio of the view (width / height).

	*
		- depthNear

		- Distance to the near clip plane.

	*
		- depthFar

		- Distance to the far clip plane.

	*
		- System.ArgumentOutOfRangeException

		- 
		  Thrown under the following conditions:
		  
		  * fovy is zero, less than zero or larger than Math.PI
		  
		  * aspect is negative or zero
		  
		  * depthNear is negative or zero
		  
		  * depthFar is negative or zero
		  
		  * depthNear is larger than depthFar



.. rubric:: Returns:

A perspective projection matrix.

.. index:: pair: function; CreateTranslation
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1aecdf94eae4505ec69c3bab89bac04f4f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateTranslation(double x, double y, double z)

Build a translation matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- x

		- X translation.

	*
		- y

		- Y translation.

	*
		- z

		- Z translation.



.. rubric:: Returns:

The resulting :ref:`Matrix4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` instance.

.. index:: pair: function; CreateTranslation
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1adb2427941c84272ef1e93e07611af316:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 CreateTranslation(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` vector)

Builds a translation matrix from a vector.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- vector

		- The translation vector.



.. rubric:: Returns:

The resulting :ref:`Matrix4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` instance.

.. index:: pair: function; LookAt
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7e90a275e17eff3a0aee0f04bb315714:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 LookAt(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` eye, :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` target, :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` up)

Builds a world space to view space matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- eye

		- Eye (camera) position in world space.

	*
		- target

		- Target position in world space.

	*
		- up

		- Up vector in world space (should not be parallel to the camera direction, that is target - eye).



.. rubric:: Returns:

A Matrix that transforms world space to camera (view) space.

.. index:: pair: function; Transpose
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a7324860705ad973a858d8379888ea90a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 Transpose(Matrix4 mat)

Computes the transpose of the given matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to compute the transpose of.



.. rubric:: Returns:

The transpose of the given matrix.

.. index:: pair: function; Normalize
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a41bebd15d2bfe0b5f5b6ab5a19680c5f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Matrix4 Normalize(Matrix4 mat)

Divides each element in the Matrix by the :ref:`Determinant <doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4_1a9df03e18934d2e32d1051f29c1ca3286>`.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mat

		- Matrix to normalize.



.. rubric:: Returns:

The normalized matrix.

