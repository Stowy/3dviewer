.. index:: pair: class; ThreeDViewer.EntityCollection
.. _doxid-class_three_d_viewer_1_1_entity_collection:

class ThreeDViewer.EntityCollection
===================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Class containing a list of entities and a reference to its parents. :ref:`More...<details-class_three_d_viewer_1_1_entity_collection>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class EntityCollection: IEnumerable< Entity >
	{
	public:
		// properties
	
		int :ref:`Count<doxid-class_three_d_viewer_1_1_entity_collection_1ae091c20f4653a4989193decef93609d0>`;
		List<:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`> :ref:`AbsoluteFaces<doxid-class_three_d_viewer_1_1_entity_collection_1a07bab16a1284a5cfce93c5bb402dea9d>`;
		:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` :ref:`this[int index]<doxid-class_three_d_viewer_1_1_entity_collection_1aa104fc6b8f55e37bc7408bd590a37b9d>`;

		// methods
	
		:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection_1a9cda393be80dd3f56a8d7bc5be835841>`(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` parent);
		:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection_1a048a5d04c5fe0a597b0021d5b613955d>`(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` parent, List<:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>`> entities);
		void :ref:`Add<doxid-class_three_d_viewer_1_1_entity_collection_1a4841d60925db880f1485dc26df75e261>`(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` entity);
		void :ref:`Remove<doxid-class_three_d_viewer_1_1_entity_collection_1af454a22f602ea9b21b93983e55e7fc39>`(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` entity);
		void :ref:`Clear<doxid-class_three_d_viewer_1_1_entity_collection_1a80a9242e5764de7419fecec84ffbeda1>`();
		IEnumerator<:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>`> :target:`GetEnumerator<doxid-class_three_d_viewer_1_1_entity_collection_1a3c98ce82749b1f78007b4758a4a5f902>`();
	};
.. _details-class_three_d_viewer_1_1_entity_collection:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Class containing a list of entities and a reference to its parents.

Properties
----------

.. index:: pair: property; Count
.. _doxid-class_three_d_viewer_1_1_entity_collection_1ae091c20f4653a4989193decef93609d0:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	int Count

Gets the number of entities contained in the collection.

.. index:: pair: property; AbsoluteFaces
.. _doxid-class_three_d_viewer_1_1_entity_collection_1a07bab16a1284a5cfce93c5bb402dea9d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	List<:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`> AbsoluteFaces

Gets all the absolute faces in the collection (this includes the children of the entities).

.. index:: pair: property; this[int index]
.. _doxid-class_three_d_viewer_1_1_entity_collection_1aa104fc6b8f55e37bc7408bd590a37b9d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` this[int index]

Gets the entity in the specified index.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- index

		- Index of the entity.



.. rubric:: Returns:

The specified entity.

Methods
-------

.. index:: pair: function; EntityCollection
.. _doxid-class_three_d_viewer_1_1_entity_collection_1a9cda393be80dd3f56a8d7bc5be835841:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	EntityCollection(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` parent)

Initializes a new instance of the :ref:`EntityCollection <doxid-class_three_d_viewer_1_1_entity_collection>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- parent

		- Parent of this collection.

.. index:: pair: function; EntityCollection
.. _doxid-class_three_d_viewer_1_1_entity_collection_1a048a5d04c5fe0a597b0021d5b613955d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	EntityCollection(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` parent, List<:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>`> entities)

Initializes a new instance of the :ref:`EntityCollection <doxid-class_three_d_viewer_1_1_entity_collection>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- parent

		- Parent of this collection.

	*
		- entities

		- Entities stored in this collection.

.. index:: pair: function; Add
.. _doxid-class_three_d_viewer_1_1_entity_collection_1a4841d60925db880f1485dc26df75e261:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void Add(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` entity)

Adds an entity to the list. Checks if the entity is in the list first and updates it's parent when added.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- entity

		- :ref:`Entity <doxid-class_three_d_viewer_1_1_entity>` to add in the list.

.. index:: pair: function; Remove
.. _doxid-class_three_d_viewer_1_1_entity_collection_1af454a22f602ea9b21b93983e55e7fc39:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void Remove(:ref:`Entity<doxid-class_three_d_viewer_1_1_entity>` entity)

Removes an entity in the list. Sets the parent of the entity as null.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- entity

		- :ref:`Entity <doxid-class_three_d_viewer_1_1_entity>` to remove from the list.

.. index:: pair: function; Clear
.. _doxid-class_three_d_viewer_1_1_entity_collection_1a80a9242e5764de7419fecec84ffbeda1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void Clear()

Removes all the elements from the collection.

