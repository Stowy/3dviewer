.. index:: pair: struct; ThreeDViewer.Mathematics.Vector3
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3:

struct ThreeDViewer.Mathematics.Vector3
=======================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents a 3D vector. :ref:`More...<details-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	struct Vector3:
	    IComparable,
	    IComparable< Vector3 >,
	    IEquatable< Vector3 >
	{
		// fields
	
		static readonly Vector3 :ref:`Origin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aedd2e4506db7637e2aa26e9e210174cb>` = new(0, 0, 0);
		static readonly Vector3 :ref:`XAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3332951e14eff97bc693066699e5c4f8>` = new(1, 0, 0);
		static readonly Vector3 :ref:`YAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aadeb7d13480705e944cf04e6041a3352>` = new(0, 1, 0);
		static readonly Vector3 :ref:`ZAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1adad52c19af0cf9f68b9ae2f4a63467ad>` = new(0, 0, 1);
		static readonly Vector3 :ref:`MinValue<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a546d78b538d49550b140ed33409b526c>` = new(double.MinValue, double.MinValue, double.MinValue);
		static readonly Vector3 :ref:`MaxValue<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a8c38258858c7b75cf4378d236755fcf2>` = new(double.MaxValue, double.MaxValue, double.MaxValue);
		static readonly Vector3 :ref:`Epsilon<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a51133d3709a1015fb7b7e25a7890bb69>` = new(double.Epsilon, double.Epsilon, double.Epsilon);
		static readonly Vector3 :ref:`Zero<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a03171f23e5a54113a32fc36fb8c60df1>` = :ref:`Origin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aedd2e4506db7637e2aa26e9e210174cb>`;
		static readonly Vector3 :ref:`NaN<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af46ea6abcad9a06f86ea2cff90d5754b>` = new(double.NaN, double.NaN, double.NaN);

		// properties
	
		double :ref:`X<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ade4a44b832868474780f821c926b14c4>`;
		double :ref:`Y<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a22ff70098643ac7b6268714b7b67a7af>`;
		double :ref:`Z<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aaca56cfb5732e7d7269479a05caad526>`;
		double[] :ref:`Array<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3e6e5cf4c27f19c3bb56867011b8e8ac>`;
		double :ref:`Magnitude<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ad37bca099a3d0c0cca75af2bdb7fdd9a>`;
		double :ref:`SqrMagnitude<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af33a9f1dfe82251f36a59e730e17beee>`;
		Vector3 :ref:`Normalized<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a74296fdc67232303b335605bbd7a362d>`;
		double :ref:`this[int index]<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a54ce64a641147e82046a3eb41591a76e>`;

		// methods
	
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6f1ce66def987ad013e03e30eea657c5>`(double x, double y, double z);
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af516a01256cf943a13518293725d4201>`(double[] xyz);
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a81cd67a3a28ade1c47c5e4340d35cded>`(Vector3 v1);
		double :ref:`Dot<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a7babf7d17ba4d2c371bf8589089fe0a4>`(Vector3 other);
		Vector3 :ref:`Cross<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a56cfc74d99d52a8b4468376887fc0725>`(Vector3 other);
		bool :ref:`IsUnitVector<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6b33d54bbba30bcf6950b20aa068a17c>`();
		bool :ref:`IsUnitVector<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6e3420288c232d6e9a3bfca9e4f7a893>`(double tolerance);
		Vector3 :ref:`Normalize<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a9b2b4e480475efd52e3e2331d9d73e56>`();
		bool :ref:`IsNaN<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a47b8be657aebcee2297f9ac15115bacd>`();
		Vector3 :ref:`NormalizeOrDefault<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a03879d591aba5b65063a4ad809e35cb2>`();
		double :ref:`Distance<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0b3dc455c65a22da7d204944de125802>`(Vector3 other);
		double :ref:`SumComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a2419c7ee7b527e7c0271d2ddb288cabe>`();
		Vector3 :ref:`SqrComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0997528537a48e3a81beb5146587e53f>`();
		override string :target:`ToString<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3674df5811e1d9dd4a990d6e17e6a68a>`();
		override bool :target:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ac09e151daa4c875bf0bee5612d35c037>`(object other);
		bool :target:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aee339cf38d3ce665ea7505b469cad1bf>`(Vector3 other);
		bool :ref:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0af5c098911ee0b0d35cf1b97fdb8e12>`(object other, double tolerance);
		bool :ref:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aa876c08e7272878b96320e8186a4f3e0>`(Vector3 other, double tolerance);
		override int :target:`GetHashCode<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aefbb8eca09b1534921b82df06b9d7814>`();
		int :target:`CompareTo<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ae3078ab4291a3f44b3a4dafcff7945ae>`(object other);
		int :target:`CompareTo<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a2e95c9019594d727643cae693852466b>`(Vector3 other);
		int :ref:`CompareTo<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a413f38c83fa3ba783df598e5ac9d5b15>`(object other, double tolerance);
		bool :ref:`IsBackFace<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aafc9eb2192d92091fd38c7bae262791e>`(Vector3 lineOfSight);
		PointF :ref:`ToPointF<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ac9b743e30bd897a3f7f0e0b32cb9c03b>`();
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` :ref:`ToVector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a21078c9b3625d41c53f070b79c9a5565>`();
		static Vector3 :target:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a10ad437fe2f7ee437199552e51e3358b>` (Vector3 v1, Vector3 v2);
		static Vector3 :target:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1adb96f64368e686c4ec906ae1801c2914>` (Vector3 v1, Vector3 v2);
		static Vector3 :target:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a1344353cfce4ea87210b7a860dc21590>` (Vector3 v1);
		static Vector3 :target:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ada42e330ad843eb6f16c2b554c888bba>` (Vector3 v1);
		static bool :target:`operator <<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a40f4854ca91762b6a1ce0cd981fa41fd>` (Vector3 v1, Vector3 v2);
		static bool :target:`operator <=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aac387c8dcf8b7260b48aea590e192b83>` (Vector3 v1, Vector3 v2);
		static bool :target:`operator ><doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aaa10b4911579eed106ca09932cac64b9>` (Vector3 v1, Vector3 v2);
		static bool :target:`operator >=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a941cc42f4cbeed11be3575bf58ad11f8>` (Vector3 v1, Vector3 v2);
		static bool :target:`operator ==<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a93ebe262af30734115507f3b1f8a1354>` (Vector3 v1, Vector3 v2);
		static bool :target:`operator !=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a52a8970faea6eb64a9d6a796069c5b9e>` (Vector3 v1, Vector3 v2);
		static Vector3 :target:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a2520417926f6aa453518fccff4af1797>` (Vector3 v1, double s2);
		static Vector3 :target:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0a7525227be3d355cc06b55fe7322cba>` (double s1, Vector3 v2);
		static Vector3 :target:`operator/<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aa716d19a666be51c0e3124c6fdb372e6>` (Vector3 v1, double s2);
		static Vector3 :target:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aba69ffdbe243d46a7fa38b27f683bb1d>` (Vector3 v1, Vector3 v2);
		static double :ref:`Dot<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6c3b11423f30a32fec795a463018cb67>`(Vector3 v1, Vector3 v2);
		static Vector3 :ref:`Cross<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ab41e6b1e33ac7129dcdc945cb84b53a4>`(Vector3 v1, Vector3 v2);
		static bool :ref:`IsUnitVector<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a532b16ff1a54fdfb8c57d71d37ae4fa9>`(Vector3 v1);
		static bool :ref:`IsUnitVector<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a031228d96103401c68af98509b168074>`(Vector3 v1, double tolerance);
		static Vector3 :ref:`Normalize<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a041a25552fa8898a859bb74a94b758ca>`(Vector3 v1);
		static bool :ref:`IsNaN<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1affd7142a868f81e89bab3b10a282800f>`(Vector3 v1);
		static Vector3 :ref:`NormalizeOrDefault<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aed10b23a2b6735ce03a4c03653fd6022>`(Vector3 v1);
		static double :ref:`Angle<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a59bd99accbd2780edabc4687dbaffab6>`(Vector3 v1, Vector3 v2);
		static double :ref:`Abs<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3be25dad85db9b59dc85532348595dc6>`(Vector3 v1);
		static double :ref:`Distance<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af609ccaa9df0d64b90e4521254c97a07>`(Vector3 v1, Vector3 v2);
		static double :ref:`SumComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a66b4889e553db33c24718a69786ba87d>`(Vector3 v1);
		static Vector3 :ref:`PowComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a878b50fac3f164bd03924f1471ec7c5d>`(Vector3 v1, double power);
		static Vector3 :ref:`SqrtComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a14d27575a4fb2450847ac61e9200ef6b>`(Vector3 v1);
		static Vector3 :ref:`SqrComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0e9814d18e30c310a6b760ca757af6a1>`(Vector3 v1);
		static bool :ref:`IsBackFace<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0beda1c711b8d77865dfe97a3a45a8c5>`(Vector3 normal, Vector3 lineOfSight);
	};
.. _details-struct_three_d_viewer_1_1_mathematics_1_1_vector3:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents a 3D vector.

Based on this tutorial : `https://www.codeproject.com/articles/17425/a-vector-type-for-c <https://www.codeproject.com/articles/17425/a-vector-type-for-c>`__.

Fields
------

.. index:: pair: variable; Origin
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aedd2e4506db7637e2aa26e9e210174cb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 Origin = new(0, 0, 0)

An origin vector. Is equal to Vector(0, 0, 0). Same as Zero.

.. index:: pair: variable; XAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3332951e14eff97bc693066699e5c4f8:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 XAxis = new(1, 0, 0)

Represents the X axis. Is equal to Vector(1, 0, 0).

.. index:: pair: variable; YAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aadeb7d13480705e944cf04e6041a3352:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 YAxis = new(0, 1, 0)

Represents the Y axis. Is equal to Vector(0, 1, 0).

.. index:: pair: variable; ZAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1adad52c19af0cf9f68b9ae2f4a63467ad:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 ZAxis = new(0, 0, 1)

Represents the Z axis. Is equal to Vector(0, 0, 1).

.. index:: pair: variable; MinValue
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a546d78b538d49550b140ed33409b526c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 MinValue = new(double.MinValue, double.MinValue, double.MinValue)

A vector with every components equal to double.MinValue.

.. index:: pair: variable; MaxValue
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a8c38258858c7b75cf4378d236755fcf2:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 MaxValue = new(double.MaxValue, double.MaxValue, double.MaxValue)

A vector with every components equal to double.MaxValue.

.. index:: pair: variable; Epsilon
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a51133d3709a1015fb7b7e25a7890bb69:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 Epsilon = new(double.Epsilon, double.Epsilon, double.Epsilon)

A vector with every components equal to double.Epsilon.

.. index:: pair: variable; Zero
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a03171f23e5a54113a32fc36fb8c60df1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 Zero = :ref:`Origin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aedd2e4506db7637e2aa26e9e210174cb>`

A zero vector. Is equal to Vector(0, 0, 0). Same as Origin.

.. index:: pair: variable; NaN
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af46ea6abcad9a06f86ea2cff90d5754b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector3 NaN = new(double.NaN, double.NaN, double.NaN)

A vector with every components equal to double.NaN.

Properties
----------

.. index:: pair: property; X
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ade4a44b832868474780f821c926b14c4:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double X

Gets the X component of the vector.

.. index:: pair: property; Y
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a22ff70098643ac7b6268714b7b67a7af:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Y

Gets the Y component of the vector.

.. index:: pair: property; Z
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aaca56cfb5732e7d7269479a05caad526:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Z

Gets the Z component of the vector.

.. index:: pair: property; Array
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3e6e5cf4c27f19c3bb56867011b8e8ac:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double[] Array

Gets the vector as an array.

.. index:: pair: property; Magnitude
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ad37bca099a3d0c0cca75af2bdb7fdd9a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Magnitude

Gets the magnitude (aka. length or absolute value) of the vector.

.. index:: pair: property; SqrMagnitude
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af33a9f1dfe82251f36a59e730e17beee:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double SqrMagnitude

Gets the squared magnitude of this vector, can be used for better performance than Magnitude.

.. index:: pair: property; Normalized
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a74296fdc67232303b335605bbd7a362d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3 Normalized

Gets this vector but normalized.

.. index:: pair: property; this[int index]
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a54ce64a641147e82046a3eb41591a76e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double this[int index]

An index accessor for a vector, mapping index [0] -> X, [1] -> Y and [2] -> Z.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- index

		- The array index referring to a component within the vector (i.e. x, y, z).



.. rubric:: Returns:

Returns X if 0, Y if 1 and Z if 2.

Methods
-------

.. index:: pair: function; Vector3
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6f1ce66def987ad013e03e30eea657c5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3(double x, double y, double z)

Initializes a new instance of the :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- x

		- X component of the vector.

	*
		- y

		- Y component of the vector.

	*
		- z

		- Z component of the vector.

.. index:: pair: function; Vector3
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af516a01256cf943a13518293725d4201:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3(double[] xyz)

Initializes a new instance of the :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- xyz

		- Array with the X, Y and Z components fo the vector.

	*
		- System.ArgumentException

		- Thrown if the array argument does not contain exactly three components.

.. index:: pair: function; Vector3
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a81cd67a3a28ade1c47c5e4340d35cded:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3(Vector3 v1)

Initializes a new instance of the :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` representing the new values for the vector.

.. index:: pair: function; Dot
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a7babf7d17ba4d2c371bf8589089fe0a4:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Dot(Vector3 other)

Determines the dot product of two vectors.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The vector to multiply by.



.. rubric:: Returns:

Returns a scalar representing the dot product of the two vectors.

.. index:: pair: function; Cross
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a56cfc74d99d52a8b4468376887fc0725:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3 Cross(Vector3 other)

Determine the cross product of two Vectors. Determine the vector product. Determine the normal vector (:ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` 90° to the plane).



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The vector to multiply by.



.. rubric:: Returns:

:ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` representig the cross product of the two vectors.

.. index:: pair: function; IsUnitVector
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6b33d54bbba30bcf6950b20aa068a17c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsUnitVector()

Checks if the vector is a unit vector. Checks if the vector has be normalized. Checks if the vector has a magnitude of 1.



.. rubric:: Returns:

Returns true if the vector is a unit vector.

.. index:: pair: function; IsUnitVector
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6e3420288c232d6e9a3bfca9e4f7a893:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsUnitVector(double tolerance)

Checks if the vector is a unit vector within a tolerance. Checks if the vector has been normalized within a tolerance. Checks if the vector has a magnitude of 1 within a tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- tolerance

		- The tolerance to use when comparing the magnitude.



.. rubric:: Returns:

Returns true if the vector is a unit vector.

.. index:: pair: function; Normalize
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a9b2b4e480475efd52e3e2331d9d73e56:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3 Normalize()

Gets the normalized unit vector with a magnitude of one.

Exceptions will be thrown if the vector being normalized has a magnitude of 0 or of NaN.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- NormalizeVectorException

		- Thrown when the normalisation of a zero magnitude vector is attempted.

	*
		- NormalizeVectorException

		- Thrown when the normalisation of a NaN magnitude vector is attempted.



.. rubric:: Returns:

Returns the normalized vector.

.. index:: pair: function; IsNaN
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a47b8be657aebcee2297f9ac15115bacd:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsNaN()

Checks if any component of a vector is Not A Number (NaN).



.. rubric:: Returns:

Returns true if the vector has NaN components.

.. index:: pair: function; NormalizeOrDefault
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a03879d591aba5b65063a4ad809e35cb2:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3 NormalizeOrDefault()

Gets the normalized unit vector with a magnitude of one.



.. rubric:: Returns:

Returns Vector (0, 0, 0) if the magnitude is zero, Vector (NaN, NaN, NaN) if magnitude is NaN, or normalized vector.

.. index:: pair: function; Distance
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0b3dc455c65a22da7d204944de125802:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Distance(Vector3 other)

Computes the distance between two vectors.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The vector to find the distance to.



.. rubric:: Returns:

Returns the distance between two vectors.

.. index:: pair: function; SumComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a2419c7ee7b527e7c0271d2ddb288cabe:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double SumComponents()

Sums the components of the vector.



.. rubric:: Returns:

The sums of the vector's X, Y and Z components.

.. index:: pair: function; SqrComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0997528537a48e3a81beb5146587e53f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector3 SqrComponents()

The vectors's components squared.



.. rubric:: Returns:

The squared vectors.

.. index:: pair: function; Equals
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0af5c098911ee0b0d35cf1b97fdb8e12:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool Equals(object other, double tolerance)

Comparator within a tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The other object to compare to.

	*
		- tolerance

		- The tolerance to use when comparing the vector components.



.. rubric:: Returns:

True if two objects are Vector3s and are equal within a tolerance.

.. index:: pair: function; Equals
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aa876c08e7272878b96320e8186a4f3e0:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool Equals(Vector3 other, double tolerance)

Comparator within a tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The other vector to compare to.

	*
		- tolerance

		- The tolerance to use when comparing the vector components.



.. rubric:: Returns:

True if two vectors are equal within a tolerance.

.. index:: pair: function; CompareTo
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a413f38c83fa3ba783df598e5ac9d5b15:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	int CompareTo(object other, double tolerance)

Compares the magnitude of this instance against the magnitude of the supplied vector.

Comparing two vectors has no meaning, we are comparing the magnitude of two vectors for convinience. It would be more accurate to compare the magnitudes explicitly using v1.Magnitude.CompareTo(v2.Magnitude).



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The vector to compare this instance with.

	*
		- tolerance

		- Tolerence to use when comparing the two vectors.



.. rubric:: Returns:

-1: The magnitude of this instance is less than the others magnitude. 0: The magnitude of this instance equals the magnitude of the other. 1: The magnitude of this instance is greater than the magnitude of the other.

.. index:: pair: function; IsBackFace
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aafc9eb2192d92091fd38c7bae262791e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsBackFace(Vector3 lineOfSight)

Checks if a face normal vector represents back face. Checks if a face is visible, given the line of sight.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- lineOfSight

		- The unit vector representing the direction of sight from a virtual camera.



.. rubric:: Returns:

True if the vector (as a normal) represents a back-face.

.. index:: pair: function; ToPointF
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ac9b743e30bd897a3f7f0e0b32cb9c03b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	PointF ToPointF()

Converts this vector to a PointF. Only keeps the X and Y components of this vector.



.. rubric:: Returns:

A point with the X and Y components of this vector.

.. index:: pair: function; ToVector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a21078c9b3625d41c53f070b79c9a5565:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` ToVector4()

Converts this vector to a :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`. The :ref:`Vector4.W <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1afcbb8559459e42f366dec03462f830fc>` component will be equal to 1.



.. rubric:: Returns:

The :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` as a :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`.

.. index:: pair: function; Dot
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a6c3b11423f30a32fec795a463018cb67:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double Dot(Vector3 v1, Vector3 v2)

Determines the dot product of two vectors.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to multiply.

	*
		- v2

		- The vector to multiply by.



.. rubric:: Returns:

Returns a scalar representing the dot product of the two vectors.

.. index:: pair: function; Cross
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1ab41e6b1e33ac7129dcdc945cb84b53a4:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector3 Cross(Vector3 v1, Vector3 v2)

Determine the cross product of two Vectors. Determine the vector product. Determine the normal vector (:ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` 90° to the plane).



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to multiply.

	*
		- v2

		- The vector to multiply by.



.. rubric:: Returns:

:ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` representig the cross product of the two vectors.

.. index:: pair: function; IsUnitVector
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a532b16ff1a54fdfb8c57d71d37ae4fa9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool IsUnitVector(Vector3 v1)

Checks if the vector is a unit vector. Checks if the vector has be normalized. Checks if the vector has a magnitude of 1.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to be checked for normalization.



.. rubric:: Returns:

Returns true if the vector is a unit vector.

.. index:: pair: function; IsUnitVector
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a031228d96103401c68af98509b168074:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool IsUnitVector(Vector3 v1, double tolerance)

Checks if the vector is a unit vector within a tolerance. Checks if the vector has been normalized within a tolerance. Checks if the vector has a magnitude of 1 within a tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to be checked for normalization.

	*
		- tolerance

		- The tolerance to use when comparing the magnitude.



.. rubric:: Returns:

Returns true if the vector is a unit vector.

.. index:: pair: function; Normalize
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a041a25552fa8898a859bb74a94b758ca:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector3 Normalize(Vector3 v1)

Gets the normalized unit vector with a magnitude of one.

Exceptions will be thrown if the vector being normalized has a magnitude of 0 or of NaN.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to be normalized.

	*
		- NormalizeVectorException

		- Thrown when the normalisation of a zero magnitude vector is attempted.

	*
		- NormalizeVectorException

		- Thrown when the normalisation of a NaN magnitude vector is attempted.



.. rubric:: Returns:

Returns the normalized vector.

.. index:: pair: function; IsNaN
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1affd7142a868f81e89bab3b10a282800f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool IsNaN(Vector3 v1)

Checks if any component of a vector is Not A Number (NaN).



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector checked for NaN components.



.. rubric:: Returns:

Returns true if the vector has NaN components.

.. index:: pair: function; NormalizeOrDefault
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1aed10b23a2b6735ce03a4c03653fd6022:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector3 NormalizeOrDefault(Vector3 v1)

Gets the normalized unit vector with a magnitude of one.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to be normalized.



.. rubric:: Returns:

Returns Vector (0,0,0) if the magnitude is zero, Vector (NaN, NaN, NaN) if magnitude is NaN, or normalized vector.

.. index:: pair: function; Angle
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a59bd99accbd2780edabc4687dbaffab6:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double Angle(Vector3 v1, Vector3 v2)

Computes the angle between two vectors.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to discern the angle from.

	*
		- v2

		- The vector to discern the angle to.



.. rubric:: Returns:

Returns the angle between the two vectors.

.. index:: pair: function; Abs
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a3be25dad85db9b59dc85532348595dc6:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double Abs(Vector3 v1)

Finds the absolute value of a vector. Finds the magnitude of a vector.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to get the magnitude from.



.. rubric:: Returns:

Returns a double representig the magnitude of the vector.

.. index:: pair: function; Distance
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1af609ccaa9df0d64b90e4521254c97a07:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double Distance(Vector3 v1, Vector3 v2)

Computes the distance between two vectors.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to find the distance from.

	*
		- v2

		- The vector to find the distance to.



.. rubric:: Returns:

Returns the distance between two vectors.

.. index:: pair: function; SumComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a66b4889e553db33c24718a69786ba87d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double SumComponents(Vector3 v1)

Sums the components of the vector.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector whose scalar components to sum.



.. rubric:: Returns:

The sums of the vector's X, Y and Z components.

.. index:: pair: function; PowComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a878b50fac3f164bd03924f1471ec7c5d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector3 PowComponents(Vector3 v1, double power)

The individual multiplication to a power of the vectors's components.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector whose scalar components to multiply by a power.

	*
		- power

		- The power by which to multiply the components.



.. rubric:: Returns:

The multiplied vector.

.. index:: pair: function; SqrtComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a14d27575a4fb2450847ac61e9200ef6b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector3 SqrtComponents(Vector3 v1)

The individual square root of a vectors's components.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector whose scalar components to square root.



.. rubric:: Returns:

The rooted vector.

.. index:: pair: function; SqrComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0e9814d18e30c310a6b760ca757af6a1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector3 SqrComponents(Vector3 v1)

The vectors's components squared.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector whose scalar components are to square.



.. rubric:: Returns:

The squared vectors.

.. index:: pair: function; IsBackFace
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3_1a0beda1c711b8d77865dfe97a3a45a8c5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool IsBackFace(Vector3 normal, Vector3 lineOfSight)

Checks if a face normal vector represents back face. Checks if a face is visible, given the line of sight.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- normal

		- The vector representing the face normal :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`.

	*
		- lineOfSight

		- The unit vector representing the direction of sight from a virtual camera.



.. rubric:: Returns:

True if the vector (as a normal) represents a back-face.

