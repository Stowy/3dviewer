.. index:: pair: namespace; ThreeDViewer.Utils
.. _doxid-namespace_three_d_viewer_1_1_utils:

namespace ThreeDViewer.Utils
============================

.. toctree::
	:hidden:

	class_ThreeDViewer_Utils_DoubleExtension.rst
	class_ThreeDViewer_Utils_FloatExtension.rst
	class_ThreeDViewer_Utils_MathHelper.rst
	class_ThreeDViewer_Utils_PointFExtension.rst




.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	namespace Utils {

	// classes

	class :ref:`DoubleExtension<doxid-class_three_d_viewer_1_1_utils_1_1_double_extension>`;
	class :ref:`FloatExtension<doxid-class_three_d_viewer_1_1_utils_1_1_float_extension>`;
	class :ref:`MathHelper<doxid-class_three_d_viewer_1_1_utils_1_1_math_helper>`;
	class :ref:`PointFExtension<doxid-class_three_d_viewer_1_1_utils_1_1_point_f_extension>`;

	} // namespace Utils
