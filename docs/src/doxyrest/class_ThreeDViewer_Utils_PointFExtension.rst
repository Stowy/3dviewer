.. index:: pair: class; ThreeDViewer.Utils.PointFExtension
.. _doxid-class_three_d_viewer_1_1_utils_1_1_point_f_extension:

class ThreeDViewer.Utils.PointFExtension
========================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Class that contains all the extensions for the PointF class. :ref:`More...<details-class_three_d_viewer_1_1_utils_1_1_point_f_extension>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class PointFExtension
	{
	public:
		// methods
	
		static PointF :ref:`MapToPixel<doxid-class_three_d_viewer_1_1_utils_1_1_point_f_extension_1a617e4b518350be2d898941079895238c>`(
			this PointF point,
			Size mathematicalSize,
			Size imageSize
			);
	
		static IList<PointF> :ref:`MapToPixel<doxid-class_three_d_viewer_1_1_utils_1_1_point_f_extension_1afbd700b1680373bf40a20b882a0834a1>`(
			this IList<PointF> points,
			Size mathematicalSize,
			Size imageSize
			);
	};
.. _details-class_three_d_viewer_1_1_utils_1_1_point_f_extension:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Class that contains all the extensions for the PointF class.

Methods
-------

.. index:: pair: function; MapToPixel
.. _doxid-class_three_d_viewer_1_1_utils_1_1_point_f_extension_1a617e4b518350be2d898941079895238c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static PointF MapToPixel(
		this PointF point,
		Size mathematicalSize,
		Size imageSize
		)

Maps the point from it's mathematical size to pixels on the screen.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- point

		- Point to map.

	*
		- mathematicalSize

		- Size of the mathematical space that will be projected.

	*
		- imageSize

		- Size of the image that the point will be projected on.



.. rubric:: Returns:

The mapped point.

.. index:: pair: function; MapToPixel
.. _doxid-class_three_d_viewer_1_1_utils_1_1_point_f_extension_1afbd700b1680373bf40a20b882a0834a1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static IList<PointF> MapToPixel(
		this IList<PointF> points,
		Size mathematicalSize,
		Size imageSize
		)

Maps the list of points from their mathematical size to pixels on the screen.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- points

		- Points to map.

	*
		- mathematicalSize

		- Size of the mathematical space that will be projected.

	*
		- imageSize

		- Size of the image that the point will be projected on.



.. rubric:: Returns:

The mapped points.

