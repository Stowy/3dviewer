.. index:: pair: class; ThreeDViewer.Entity
.. _doxid-class_three_d_viewer_1_1_entity:

class ThreeDViewer.Entity
=========================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents an entity that can be show with the renderer and that has children. :ref:`More...<details-class_three_d_viewer_1_1_entity>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class Entity
	{
	public:
		// properties
	
		:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` :ref:`Model<doxid-class_three_d_viewer_1_1_entity_1afb894e51ec3c6fa2731232793f69bb3f>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Position<doxid-class_three_d_viewer_1_1_entity_1a34d913417a1ac4f7088eece1331c45da>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Rotation<doxid-class_three_d_viewer_1_1_entity_1ae6c102e4f6a5c65452fb52ce1162e2c8>`;
		double :ref:`Scale<doxid-class_three_d_viewer_1_1_entity_1a765c076809a4e72454db8edb9f1ed51e>`;
		:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` :ref:`Children<doxid-class_three_d_viewer_1_1_entity_1a93f50a1698caedf63f04d76b8dc7c28e>`;
		Entity :ref:`Parent<doxid-class_three_d_viewer_1_1_entity_1adbfac1dd7ee5484d8105b9f33a9dce32>`;
		bool :ref:`IsRoot<doxid-class_three_d_viewer_1_1_entity_1ac788f34978e377a82450cea56bf3c002>`;
		bool :ref:`IsLeaf<doxid-class_three_d_viewer_1_1_entity_1a556592dbc223b7af45d684efbcefa394>`;
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` :ref:`Transformation<doxid-class_three_d_viewer_1_1_entity_1a0a4384c84dcea9094ed6363d59aa6e67>`;
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` :ref:`AbsoluteTransformation<doxid-class_three_d_viewer_1_1_entity_1ac82f1632906e726ec3d84b477e9fb741>`;
		:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` :ref:`AbsoluteModel<doxid-class_three_d_viewer_1_1_entity_1af9590971619c3746edd61e76efc3d832>`;

		// methods
	
		:ref:`Entity<doxid-class_three_d_viewer_1_1_entity_1ae39538c4c37720835c20c041e6aa016c>`();
		:ref:`Entity<doxid-class_three_d_viewer_1_1_entity_1a41e10d54840b9f40897463f53cd461e0>`(:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` model, Entity parent);
		:ref:`Entity<doxid-class_three_d_viewer_1_1_entity_1a379b84e2ec944aabb188c312fac75c1a>`(:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` model);
	};
.. _details-class_three_d_viewer_1_1_entity:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents an entity that can be show with the renderer and that has children.

Properties
----------

.. index:: pair: property; Model
.. _doxid-class_three_d_viewer_1_1_entity_1afb894e51ec3c6fa2731232793f69bb3f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` Model

Gets or sets the 3D model of this entity.

.. index:: pair: property; Position
.. _doxid-class_three_d_viewer_1_1_entity_1a34d913417a1ac4f7088eece1331c45da:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Position

Gets or sets the position of this entity.

.. index:: pair: property; Rotation
.. _doxid-class_three_d_viewer_1_1_entity_1ae6c102e4f6a5c65452fb52ce1162e2c8:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Rotation

Gets or sets the rotation in radian of this entity.

.. index:: pair: property; Scale
.. _doxid-class_three_d_viewer_1_1_entity_1a765c076809a4e72454db8edb9f1ed51e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Scale

Gets or sets the scale of this entity.

.. index:: pair: property; Children
.. _doxid-class_three_d_viewer_1_1_entity_1a93f50a1698caedf63f04d76b8dc7c28e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` Children

Gets the children of this entity.

.. index:: pair: property; Parent
.. _doxid-class_three_d_viewer_1_1_entity_1adbfac1dd7ee5484d8105b9f33a9dce32:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Entity Parent

Gets or sets the parent of this entity.

.. index:: pair: property; IsRoot
.. _doxid-class_three_d_viewer_1_1_entity_1ac788f34978e377a82450cea56bf3c002:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsRoot

Gets a value indicating whether this entity is root or not.

.. index:: pair: property; IsLeaf
.. _doxid-class_three_d_viewer_1_1_entity_1a556592dbc223b7af45d684efbcefa394:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsLeaf

Gets a value indicating whether this entity is a leaf or not.

.. index:: pair: property; Transformation
.. _doxid-class_three_d_viewer_1_1_entity_1a0a4384c84dcea9094ed6363d59aa6e67:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` Transformation

Gets the relative transformation matrix of this entity. Contains the scale, rotation and translation of this entity.

.. index:: pair: property; AbsoluteTransformation
.. _doxid-class_three_d_viewer_1_1_entity_1ac82f1632906e726ec3d84b477e9fb741:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` AbsoluteTransformation

Gets the absolute transformation matrix of this entity. Contains the scale, rotation and translation of this entity multiplied by his parent's. Gets :ref:`Transformation <doxid-class_three_d_viewer_1_1_entity_1a0a4384c84dcea9094ed6363d59aa6e67>` if :ref:`IsRoot <doxid-class_three_d_viewer_1_1_entity_1ac788f34978e377a82450cea56bf3c002>` is false.

.. index:: pair: property; AbsoluteModel
.. _doxid-class_three_d_viewer_1_1_entity_1af9590971619c3746edd61e76efc3d832:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` AbsoluteModel

Gets the absolute model of this entity. Use this model to project and draw the entity.

Methods
-------

.. index:: pair: function; Entity
.. _doxid-class_three_d_viewer_1_1_entity_1ae39538c4c37720835c20c041e6aa016c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Entity()

Initializes a new instance of the :ref:`Entity <doxid-class_three_d_viewer_1_1_entity>` class.

.. index:: pair: function; Entity
.. _doxid-class_three_d_viewer_1_1_entity_1a41e10d54840b9f40897463f53cd461e0:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Entity(:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` model, Entity parent)

Initializes a new instance of the :ref:`Entity <doxid-class_three_d_viewer_1_1_entity>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- model

		- 3D model of this entity.

	*
		- parent

		- Parent of this entity.

.. index:: pair: function; Entity
.. _doxid-class_three_d_viewer_1_1_entity_1a379b84e2ec944aabb188c312fac75c1a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Entity(:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` model)

Initializes a new instance of the :ref:`Entity <doxid-class_three_d_viewer_1_1_entity>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- model

		- 3D model of this entity.

