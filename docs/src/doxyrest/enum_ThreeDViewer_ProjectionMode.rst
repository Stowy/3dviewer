.. index:: pair: enum; ProjectionMode
.. _doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59:

enum ThreeDViewer.ProjectionMode
================================

Overview
~~~~~~~~

Defines the projection matrix that will be used. :ref:`More...<details-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59>`

.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	enum ProjectionMode
	{
	    :ref:`Perspective<doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59aa80420eef88d11f77532f1b9cb467fa3>`  = 0,
	    :ref:`Orthographic<doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59a03424250432f2aa71de95579d2c0eaeb>` = 1,
	};

.. _details-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Defines the projection matrix that will be used.

Enum Values
-----------

.. index:: pair: enumvalue; Perspective
.. _doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59aa80420eef88d11f77532f1b9cb467fa3:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Perspective

Project with a perspective projection matrix.

.. index:: pair: enumvalue; Orthographic
.. _doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59a03424250432f2aa71de95579d2c0eaeb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Orthographic

Project with an orthographic projection matrix.

