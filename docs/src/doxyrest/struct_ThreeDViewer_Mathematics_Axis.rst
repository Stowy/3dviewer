.. index:: pair: struct; ThreeDViewer.Mathematics.Axis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis:

struct ThreeDViewer.Mathematics.Axis
====================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Struct representing an axis via two points. :ref:`More...<details-struct_three_d_viewer_1_1_mathematics_1_1_axis>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	struct Axis
	{
		// properties
	
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Point0<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a7e5b825d1e9d9d8c396740df312df93a>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Point1<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a1091e28513c61ca85bde0e10e160d6be>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`FromOrigin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a34e7f5784ea0549e88fee76f3f3a570d>`;

		// methods
	
		:ref:`Axis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a13b49d46475df632b4e09dbd84e7e66b>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` point0, :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` point1);
	};
.. _details-struct_three_d_viewer_1_1_mathematics_1_1_axis:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Struct representing an axis via two points.

Properties
----------

.. index:: pair: property; Point0
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a7e5b825d1e9d9d8c396740df312df93a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Point0

Gets the first point of the axis.

.. index:: pair: property; Point1
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a1091e28513c61ca85bde0e10e160d6be:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Point1

Gets the second point of the axis.

.. index:: pair: property; FromOrigin
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a34e7f5784ea0549e88fee76f3f3a570d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` FromOrigin

Gets the axis with a point starting from the origin. Calculated by : :ref:`Point1 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a1091e28513c61ca85bde0e10e160d6be>` - :ref:`Point0 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a7e5b825d1e9d9d8c396740df312df93a>`.

Methods
-------

.. index:: pair: function; Axis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis_1a13b49d46475df632b4e09dbd84e7e66b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Axis(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` point0, :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` point1)

Initializes a new instance of the :ref:`Axis <doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- point0

		- First point of the axis.

	*
		- point1

		- Second point of the axis.

