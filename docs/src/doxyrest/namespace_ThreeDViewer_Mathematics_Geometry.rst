.. index:: pair: namespace; ThreeDViewer.Mathematics.Geometry
.. _doxid-namespace_three_d_viewer_1_1_mathematics_1_1_geometry:

namespace ThreeDViewer.Mathematics.Geometry
===========================================

.. toctree::
	:hidden:

	class_ThreeDViewer_Mathematics_Geometry_Face.rst
	class_ThreeDViewer_Mathematics_Geometry_Model.rst




.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	namespace Geometry {

	// classes

	class :ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`;
	class :ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>`;

	} // namespace Geometry
