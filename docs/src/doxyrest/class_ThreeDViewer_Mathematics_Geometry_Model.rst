.. index:: pair: class; ThreeDViewer.Mathematics.Geometry.Model
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model:

class ThreeDViewer.Mathematics.Geometry.Model
=============================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents a 3D :ref:`Model <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>`. :ref:`More...<details-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class Model
	{
	public:
		// properties
	
		:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`[] :ref:`Faces<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a1e458874e3f0c6f66f46a2c60809d09b>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Center<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1ab056aee60c14dffa4be606ebc49f2ab5>`;

		// methods
	
		:ref:`Model<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a400f82daa799880806a48a12c32a2592>`(:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`[] faces);
		static Model :target:`operator *<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a4085b180b89fc8aa8b25f680ba0b69bc>` (Model model, :ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` matrix);
		static Model :ref:`FromPly<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1aba7204cae26debd226b761cd433feeac>`(string path);
	
		static Model :ref:`CreateRectangularCuboidCentered<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a58a6d4ebc74935b4664bf93288272906>`(
			double height,
			double width,
			double depth
			);
	
		static Model :ref:`CreateRectangularCuboidOffCenter<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1aa1a0224a6cc3fec21f8c08cc4ddfab84>`(
			double height,
			double width,
			double depth
			);
	};
.. _details-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents a 3D :ref:`Model <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>`.

Properties
----------

.. index:: pair: property; Faces
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a1e458874e3f0c6f66f46a2c60809d09b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`[] Faces

Gets or sets the faces of the model.

.. index:: pair: property; Center
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1ab056aee60c14dffa4be606ebc49f2ab5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Center

Gets the center of the model.

Methods
-------

.. index:: pair: function; Model
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a400f82daa799880806a48a12c32a2592:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Model(:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`[] faces)

Initializes a new instance of the :ref:`Model <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- faces

		- Faces of the model.

.. index:: pair: function; FromPly
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1aba7204cae26debd226b761cd433feeac:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Model FromPly(string path)

Creates a model from a ply file.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- path

		- The path of the ply file.

	*
		- FileNotFoundException

		- Thrown if the file doesn't exist.

	*
		- FileFormatException

		- Thrown if the file is not in a good format.



.. rubric:: Returns:

The model in the ply file.

.. index:: pair: function; CreateRectangularCuboidCentered
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1a58a6d4ebc74935b4664bf93288272906:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Model CreateRectangularCuboidCentered(
		double height,
		double width,
		double depth
		)

Creates a rectangular cuboid from the specified sizes centered on itself.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- height

		- Height of the cuboid.

	*
		- width

		- Width of the cuboid.

	*
		- depth

		- Depth of the cuboid.



.. rubric:: Returns:

The created cuboid.

.. index:: pair: function; CreateRectangularCuboidOffCenter
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_model_1aa1a0224a6cc3fec21f8c08cc4ddfab84:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Model CreateRectangularCuboidOffCenter(
		double height,
		double width,
		double depth
		)

Creates a rectangular cuboid from the specified sizes centered on its corner.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- height

		- Height of the cuboid.

	*
		- width

		- Width of the cuboid.

	*
		- depth

		- Depth of the cuboid.



.. rubric:: Returns:

The created cuboid.

