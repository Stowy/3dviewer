.. index:: pair: class; ThreeDViewer.Renderer
.. _doxid-class_three_d_viewer_1_1_renderer:

class ThreeDViewer.Renderer
===========================

.. toctree::
	:hidden:

Overview
~~~~~~~~

The class that renders a 3D model on a System.Drawing.Bitmap. :ref:`More...<details-class_three_d_viewer_1_1_renderer>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class Renderer
	{
	public:
		// fields
	
		static const double :ref:`CameraMoveSpeed<doxid-class_three_d_viewer_1_1_renderer_1a1fd01190f3439826bd0243119d7caa5f>` = 0.5d;
		static const double :ref:`CameraLookSpeed<doxid-class_three_d_viewer_1_1_renderer_1a86cb2812e9fc117bfd5eefb0cd2a739a>` = 1d;
		static const double :ref:`AxesSize<doxid-class_three_d_viewer_1_1_renderer_1a1979dd305aa79add79fd02b7bfb504bd>` = 3d;

		// properties
	
		bool :ref:`ShowFill<doxid-class_three_d_viewer_1_1_renderer_1a8a2df6905d5dac19424fcbf51b0aff87>`;
		bool :ref:`ShowWireframe<doxid-class_three_d_viewer_1_1_renderer_1a796a77ae0473b9afa868710181a1a4e0>`;
		bool :ref:`ShowNormals<doxid-class_three_d_viewer_1_1_renderer_1aa0303512fd981fb5f438791659b12ba3>`;
		Size :ref:`MathematicalSize<doxid-class_three_d_viewer_1_1_renderer_1a7fdbba75d4ba1c21c7b64adda075c5b5>`;
		Size :ref:`ImageSize<doxid-class_three_d_viewer_1_1_renderer_1a35aebf49f8f0f0f757872a4f5435b60f>`;
		Pen :ref:`OutlineColor<doxid-class_three_d_viewer_1_1_renderer_1a4a4ff841f7c7ed0753607129eaf60e94>`;
		Bitmap :ref:`Bitmap<doxid-class_three_d_viewer_1_1_renderer_1a9eca504d1233c39272fe4e41a933a673>`;
		:ref:`CullingMode<doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59>` :ref:`CullingMode<doxid-class_three_d_viewer_1_1_renderer_1a3932309534ae8bc9d5ac697c0cf9d1b6>`;
		:ref:`ProjectionMode<doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59>` :ref:`ProjectionMode<doxid-class_three_d_viewer_1_1_renderer_1aab64e5a33dfb869cb2fca73c8279f715>`;
		:ref:`Scene<doxid-class_three_d_viewer_1_1_scene>` :ref:`Scene<doxid-class_three_d_viewer_1_1_renderer_1ac5ca1a43cb0119dd394f97a1b773596a>`;
		Color :ref:`LightColor<doxid-class_three_d_viewer_1_1_renderer_1a80c45e6c589385a88c45d98893da2a76>`;

		// methods
	
		:ref:`Renderer<doxid-class_three_d_viewer_1_1_renderer_1a7e49b692940de6aeaf4d936420e8d7b2>`(
			Size mathematicalSize,
			Size imageSize,
			bool showFill = true,
			bool showWireframe = true
			);
	
		void :ref:`GoDown<doxid-class_three_d_viewer_1_1_renderer_1af22bfcf2f5e7e240a47f0e6e5b2365b7>`();
		void :ref:`GoUp<doxid-class_three_d_viewer_1_1_renderer_1afe5349e638864a2ce87159ded1a4e1f3>`();
		void :ref:`GoForward<doxid-class_three_d_viewer_1_1_renderer_1ae3419cc3937e8ed90d5f324460877ba8>`();
		void :ref:`GoBackward<doxid-class_three_d_viewer_1_1_renderer_1aa5514ebcbdec3552e608f55e4618ff9b>`();
		void :ref:`GoLeft<doxid-class_three_d_viewer_1_1_renderer_1acfa70bfa8335b79425ad0d5e282c8f32>`();
		void :ref:`GoRight<doxid-class_three_d_viewer_1_1_renderer_1aea3cf51fa44c7d4bbcf8d72b3f6a0f24>`();
		void :ref:`LookLeft<doxid-class_three_d_viewer_1_1_renderer_1ad002130d9a5e900ba7e988580954629b>`();
		void :ref:`LookRight<doxid-class_three_d_viewer_1_1_renderer_1a66442497c6467246f525d16f0f9fced9>`();
		void :ref:`LookUp<doxid-class_three_d_viewer_1_1_renderer_1aaba8aebb660b812934eaf41c728df3eb>`();
		void :ref:`LookDown<doxid-class_three_d_viewer_1_1_renderer_1ab1ed813f5225ed1969fa3642f3f03ae7>`();
		void :ref:`RenderNextFrame<doxid-class_three_d_viewer_1_1_renderer_1a8bf88066f666f67b7be23e863e6f6aba>`();
	};
.. _details-class_three_d_viewer_1_1_renderer:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

The class that renders a 3D model on a System.Drawing.Bitmap.

Fields
------

.. index:: pair: variable; CameraMoveSpeed
.. _doxid-class_three_d_viewer_1_1_renderer_1a1fd01190f3439826bd0243119d7caa5f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static const double CameraMoveSpeed = 0.5d

Movement speed of the camera.

.. index:: pair: variable; CameraLookSpeed
.. _doxid-class_three_d_viewer_1_1_renderer_1a86cb2812e9fc117bfd5eefb0cd2a739a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static const double CameraLookSpeed = 1d

Look (rotation) speed of the camera.

.. index:: pair: variable; AxesSize
.. _doxid-class_three_d_viewer_1_1_renderer_1a1979dd305aa79add79fd02b7bfb504bd:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static const double AxesSize = 3d

Size of the axes in the world.

Properties
----------

.. index:: pair: property; ShowFill
.. _doxid-class_three_d_viewer_1_1_renderer_1a8a2df6905d5dac19424fcbf51b0aff87:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool ShowFill

Gets or sets a value indicating whether the model should be drawn with filled triangles or not.

.. index:: pair: property; ShowWireframe
.. _doxid-class_three_d_viewer_1_1_renderer_1a796a77ae0473b9afa868710181a1a4e0:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool ShowWireframe

Gets or sets a value indicating whether the wireframe should be shown.

``true`` if the wireframe should be shown; otherwise, ``false``.

.. index:: pair: property; ShowNormals
.. _doxid-class_three_d_viewer_1_1_renderer_1aa0303512fd981fb5f438791659b12ba3:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool ShowNormals

Gets or sets a value indicating whether the normal of the faces should be shown.

.. index:: pair: property; MathematicalSize
.. _doxid-class_three_d_viewer_1_1_renderer_1a7fdbba75d4ba1c21c7b64adda075c5b5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Size MathematicalSize

Gets or sets the size of the mathematical projection area. It's the zone that will be displayed on the picture box.

.. index:: pair: property; ImageSize
.. _doxid-class_three_d_viewer_1_1_renderer_1a35aebf49f8f0f0f757872a4f5435b60f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Size ImageSize

Gets or sets the size of the image.

The size of the image.

.. index:: pair: property; OutlineColor
.. _doxid-class_three_d_viewer_1_1_renderer_1a4a4ff841f7c7ed0753607129eaf60e94:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Pen OutlineColor

Gets or sets the color of the outline of the polygons.

The color of the outline of the polygons.

.. index:: pair: property; Bitmap
.. _doxid-class_three_d_viewer_1_1_renderer_1a9eca504d1233c39272fe4e41a933a673:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Bitmap Bitmap

Gets the rendered bitmap.

.. index:: pair: property; CullingMode
.. _doxid-class_three_d_viewer_1_1_renderer_1a3932309534ae8bc9d5ac697c0cf9d1b6:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`CullingMode<doxid-namespace_three_d_viewer_1a369f532764ccd8f0b6bd085c09523a59>` CullingMode

Gets or sets the culling mode of the renderer.

.. index:: pair: property; ProjectionMode
.. _doxid-class_three_d_viewer_1_1_renderer_1aab64e5a33dfb869cb2fca73c8279f715:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`ProjectionMode<doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59>` ProjectionMode

Gets or sets the projection mode of the camera.

.. index:: pair: property; Scene
.. _doxid-class_three_d_viewer_1_1_renderer_1ac5ca1a43cb0119dd394f97a1b773596a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Scene<doxid-class_three_d_viewer_1_1_scene>` Scene

Gets or sets the scene contained in the renderer.

.. index:: pair: property; LightColor
.. _doxid-class_three_d_viewer_1_1_renderer_1a80c45e6c589385a88c45d98893da2a76:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Color LightColor

Gets or sets the color of the lighting.

Methods
-------

.. index:: pair: function; Renderer
.. _doxid-class_three_d_viewer_1_1_renderer_1a7e49b692940de6aeaf4d936420e8d7b2:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Renderer(
		Size mathematicalSize,
		Size imageSize,
		bool showFill = true,
		bool showWireframe = true
		)

Initializes a new instance of the :ref:`Renderer <doxid-class_three_d_viewer_1_1_renderer>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- mathematicalSize

		- Size of the box that contains the mathematical coordinates of the projected model.

	*
		- imageSize

		- Size of the image that will be displayed.

	*
		- showFill

		- Tells if the model should be filled or not.

	*
		- showWireframe

		- Tells if the wireframe should be displayed.

.. index:: pair: function; GoDown
.. _doxid-class_three_d_viewer_1_1_renderer_1af22bfcf2f5e7e240a47f0e6e5b2365b7:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void GoDown()

Makes the camera go down.

.. index:: pair: function; GoUp
.. _doxid-class_three_d_viewer_1_1_renderer_1afe5349e638864a2ce87159ded1a4e1f3:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void GoUp()

Makes the camera go up.

.. index:: pair: function; GoForward
.. _doxid-class_three_d_viewer_1_1_renderer_1ae3419cc3937e8ed90d5f324460877ba8:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void GoForward()

Makes the camera go forward.

.. index:: pair: function; GoBackward
.. _doxid-class_three_d_viewer_1_1_renderer_1aa5514ebcbdec3552e608f55e4618ff9b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void GoBackward()

Makes the camera go backward.

.. index:: pair: function; GoLeft
.. _doxid-class_three_d_viewer_1_1_renderer_1acfa70bfa8335b79425ad0d5e282c8f32:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void GoLeft()

Makes the camera go left.

.. index:: pair: function; GoRight
.. _doxid-class_three_d_viewer_1_1_renderer_1aea3cf51fa44c7d4bbcf8d72b3f6a0f24:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void GoRight()

Makes the camera go right.

.. index:: pair: function; LookLeft
.. _doxid-class_three_d_viewer_1_1_renderer_1ad002130d9a5e900ba7e988580954629b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void LookLeft()

Makes camera look to the left.

.. index:: pair: function; LookRight
.. _doxid-class_three_d_viewer_1_1_renderer_1a66442497c6467246f525d16f0f9fced9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void LookRight()

Makes the camera look to the right.

.. index:: pair: function; LookUp
.. _doxid-class_three_d_viewer_1_1_renderer_1aaba8aebb660b812934eaf41c728df3eb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void LookUp()

Makes the camera look up.

.. index:: pair: function; LookDown
.. _doxid-class_three_d_viewer_1_1_renderer_1ab1ed813f5225ed1969fa3642f3f03ae7:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void LookDown()

Makes the camera look down.

.. index:: pair: function; RenderNextFrame
.. _doxid-class_three_d_viewer_1_1_renderer_1a8bf88066f666f67b7be23e863e6f6aba:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void RenderNextFrame()

Renders the next frame.

