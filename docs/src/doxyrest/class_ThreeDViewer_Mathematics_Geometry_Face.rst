.. index:: pair: class; ThreeDViewer.Mathematics.Geometry.Face
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face:

class ThreeDViewer.Mathematics.Geometry.Face
============================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents one face of a 3D model. :ref:`More...<details-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class Face
	{
	public:
		// fields
	
		static const int :ref:`MinVertices<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a0197c1eb5d3984807ccdd391c680a508>` = 3;

		// properties
	
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Normal<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a848ec61cd5fc70ccb3d75e7206088080>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Center<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1ae633649972b2e1eb8453cc20027c2ae9>`;
		bool :ref:`IsClockwise<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a1b6d32665a1b427000f925cb654ffe22>`;
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] :ref:`Vertices<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a20fb274a852db6f0bde9385d4b99b50c>`;
		Color :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>`;
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` :ref:`this[int index]<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a9165bd255770c2fe704025835b77640b>`;

		// methods
	
		:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a05dddc3e0d6f2c1e339026ef4288554d>`(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] vertices);
		:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a5029f2cbe595b6d2962ca58df4e222f1>`(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] vertices, :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` color);
		:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a190244457386c4b8798d5d07baddec79>`(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex0, :ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex1, :ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex2, :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` color);
	
		:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a99b3c89adb59b35b3d47586af9d2e2b9>`(
			:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex0,
			:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex1,
			:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex2,
			:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex3,
			:ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` color
			);
	
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`[] :ref:`ToPhysicalCoords<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a5611de7ce35c9216a8ee47e2d77d3cdf>`();
		void :ref:`LightUp<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a27388af273973c49a1e8cac0687c67ae>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` lightDirection, :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` lightColor);
		static Face :ref:`operator *<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1acaa07456df755e17872a6dd7644e8d4b>` (Face f1, :ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` m2);
	};
.. _details-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents one face of a 3D model.

Fields
------

.. index:: pair: variable; MinVertices
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a0197c1eb5d3984807ccdd391c680a508:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static const int MinVertices = 3

The minimum amounts of vertices a face should have.

Properties
----------

.. index:: pair: property; Normal
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a848ec61cd5fc70ccb3d75e7206088080:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Normal

Gets the normal of the face.

.. index:: pair: property; Center
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1ae633649972b2e1eb8453cc20027c2ae9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Center

Gets the center of the face.

.. index:: pair: property; IsClockwise
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a1b6d32665a1b427000f925cb654ffe22:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	bool IsClockwise

Gets a value indicating whether the vertices in this face are clockwise.

.. index:: pair: property; Vertices
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a20fb274a852db6f0bde9385d4b99b50c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] Vertices

Gets or sets the vertices that are presents in the face.

.. index:: pair: property; Color
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Color Color

Gets or sets the color of the face.

.. index:: pair: property; this[int index]
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a9165bd255770c2fe704025835b77640b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` this[int index]

Gets or sets the vertex at this index.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- index

		- Index of the vertex.



.. rubric:: Returns:

The vertex at the index.

Methods
-------

.. index:: pair: function; Face
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a05dddc3e0d6f2c1e339026ef4288554d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Face(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] vertices)

Initializes a new instance of the :ref:`Face <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- vertices

		- The vertices that the face should contain.

	*
		- ArgumentException

		- Thrown when there are less than 3 points.

.. index:: pair: function; Face
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a5029f2cbe595b6d2962ca58df4e222f1:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Face(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`[] vertices, :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` color)

Initializes a new instance of the :ref:`Face <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- vertices

		- The vertices that the face should contain.

	*
		- color

		- The color of the face.

	*
		- ArgumentException

		- Thrown when there are less than 3 points.

.. index:: pair: function; Face
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a190244457386c4b8798d5d07baddec79:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Face(:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex0, :ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex1, :ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex2, :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` color)

Initializes a new instance of the :ref:`Face <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- vertex0

		- First point.

	*
		- vertex1

		- Second point.

	*
		- vertex2

		- Third point.

	*
		- color

		- The color of the face.

.. index:: pair: function; Face
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a99b3c89adb59b35b3d47586af9d2e2b9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Face(
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex0,
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex1,
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex2,
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` vertex3,
		:ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` color
		)

Initializes a new instance of the :ref:`Face <doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- vertex0

		- First point.

	*
		- vertex1

		- Second point.

	*
		- vertex2

		- Third point.

	*
		- vertex3

		- Fourth point.

	*
		- color

		- The color of the face.

.. index:: pair: function; ToPhysicalCoords
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a5611de7ce35c9216a8ee47e2d77d3cdf:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`[] ToPhysicalCoords()

Converts the vertices of this face to physical coordinates.



.. rubric:: Returns:

The vertices of this face as physical coordinates.

.. index:: pair: function; LightUp
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a27388af273973c49a1e8cac0687c67ae:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	void LightUp(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` lightDirection, :ref:`Color<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1a24633de8cb7532cd6af25831e1408aff>` lightColor)

Updates the color (lighting) of the face.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- lightDirection

		- The direction vector of the light.

	*
		- lightColor

		- The color the face will be at 100% lighting.

.. index:: pair: function; operator*
.. _doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face_1acaa07456df755e17872a6dd7644e8d4b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Face operator * (Face f1, :ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` m2)

Multiplies the vertices of the face by the matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- f1

		- The face to multiply.

	*
		- m2

		- The matrix to multiply the face by.



.. rubric:: Returns:

The multiplied face.

