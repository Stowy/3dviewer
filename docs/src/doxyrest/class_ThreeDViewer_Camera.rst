.. index:: pair: class; ThreeDViewer.Camera
.. _doxid-class_three_d_viewer_1_1_camera:

class ThreeDViewer.Camera
=========================

.. toctree::
	:hidden:

Overview
~~~~~~~~

A class to represent a camera in 3D Space. Inspired by this : `https://opentk.net/learn/chapter1/9-camera.html <https://opentk.net/learn/chapter1/9-camera.html>`__ and this : `https://github.com/opentk/LearnOpenTK/blob/master/Common/Camera.cs <https://github.com/opentk/LearnOpenTK/blob/master/Common/Camera.cs>`__. :ref:`More...<details-class_three_d_viewer_1_1_camera>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class Camera
	{
	public:
		// properties
	
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Position<doxid-class_three_d_viewer_1_1_camera_1a5cca1c10d0db118a69051b30b083916d>`;
		:ref:`ProjectionMode<doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59>` :ref:`ProjectionMode<doxid-class_three_d_viewer_1_1_camera_1a5c3a798160eacd1f6afe5b08c5b2a4e2>`;
		double :ref:`AspectRatio<doxid-class_three_d_viewer_1_1_camera_1af1f080aca663116691ed1ee995dc4d14>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Front<doxid-class_three_d_viewer_1_1_camera_1a9b99a918a17d446f683c72369ac4e972>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Up<doxid-class_three_d_viewer_1_1_camera_1a354cf8c195dd3998f2c098bb55b5707b>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`Right<doxid-class_three_d_viewer_1_1_camera_1a6ced811a01f7c606d7926d393b1d919a>`;
		double :ref:`Pitch<doxid-class_three_d_viewer_1_1_camera_1af75abc7de394e012d6ce40f38791d6f9>`;
		double :ref:`Yaw<doxid-class_three_d_viewer_1_1_camera_1a36ce31b0fa49704beb1666201fa243e7>`;
		double :ref:`Fov<doxid-class_three_d_viewer_1_1_camera_1a52718723419b8f82022140b7d371376e>`;
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`LineOfSight<doxid-class_three_d_viewer_1_1_camera_1a438cac842b9444574bc7563ad180af6d>`;
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` :ref:`ViewMatrix<doxid-class_three_d_viewer_1_1_camera_1a788c1cae63029f212fb4fac8076c25c7>`;
		:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` :ref:`ProjectionMatrix<doxid-class_three_d_viewer_1_1_camera_1a6c2dd3981be47e2bb71cfcd10c83b24f>`;

		// methods
	
		:ref:`Camera<doxid-class_three_d_viewer_1_1_camera_1a1cfc192b69fce57f0299efef9a994982>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` position, double aspectRatio);
	};
.. _details-class_three_d_viewer_1_1_camera:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

A class to represent a camera in 3D Space. Inspired by this : `https://opentk.net/learn/chapter1/9-camera.html <https://opentk.net/learn/chapter1/9-camera.html>`__ and this : `https://github.com/opentk/LearnOpenTK/blob/master/Common/Camera.cs <https://github.com/opentk/LearnOpenTK/blob/master/Common/Camera.cs>`__.

Properties
----------

.. index:: pair: property; Position
.. _doxid-class_three_d_viewer_1_1_camera_1a5cca1c10d0db118a69051b30b083916d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Position

Gets or sets the position of the camera.

.. index:: pair: property; ProjectionMode
.. _doxid-class_three_d_viewer_1_1_camera_1a5c3a798160eacd1f6afe5b08c5b2a4e2:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`ProjectionMode<doxid-namespace_three_d_viewer_1ac89db311615cca85e6774484af151a59>` ProjectionMode

Gets or sets the projection mode of the camera.

.. index:: pair: property; AspectRatio
.. _doxid-class_three_d_viewer_1_1_camera_1af1f080aca663116691ed1ee995dc4d14:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double AspectRatio

Sets the aspect ratio of the camera.

.. index:: pair: property; Front
.. _doxid-class_three_d_viewer_1_1_camera_1a9b99a918a17d446f683c72369ac4e972:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Front

Gets the front vector of the camera.

.. index:: pair: property; Up
.. _doxid-class_three_d_viewer_1_1_camera_1a354cf8c195dd3998f2c098bb55b5707b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Up

Gets the up vector of the camera.

.. index:: pair: property; Right
.. _doxid-class_three_d_viewer_1_1_camera_1a6ced811a01f7c606d7926d393b1d919a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` Right

Gets the right vector of the camera.

.. index:: pair: property; Pitch
.. _doxid-class_three_d_viewer_1_1_camera_1af75abc7de394e012d6ce40f38791d6f9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Pitch

Gets or sets the pitch in radians of the camera.

.. index:: pair: property; Yaw
.. _doxid-class_three_d_viewer_1_1_camera_1a36ce31b0fa49704beb1666201fa243e7:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Yaw

Gets or sets the yaw in radians of the camera.

.. index:: pair: property; Fov
.. _doxid-class_three_d_viewer_1_1_camera_1a52718723419b8f82022140b7d371376e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Fov

Gets or sets the field of view in radians of the camera.

.. index:: pair: property; LineOfSight
.. _doxid-class_three_d_viewer_1_1_camera_1a438cac842b9444574bc7563ad180af6d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` LineOfSight

Gets the line of sight of the camera.

.. index:: pair: property; ViewMatrix
.. _doxid-class_three_d_viewer_1_1_camera_1a788c1cae63029f212fb4fac8076c25c7:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` ViewMatrix

Gets the view matrix of the camera. Should be used to use to transform from world space to view space.

.. index:: pair: property; ProjectionMatrix
.. _doxid-class_three_d_viewer_1_1_camera_1a6c2dd3981be47e2bb71cfcd10c83b24f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` ProjectionMatrix

Gets the projection matrix of the camera. It's a perspective projection.

Methods
-------

.. index:: pair: function; Camera
.. _doxid-class_three_d_viewer_1_1_camera_1a1cfc192b69fce57f0299efef9a994982:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Camera(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` position, double aspectRatio)

Initializes a new instance of the :ref:`Camera <doxid-class_three_d_viewer_1_1_camera>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- position

		- Position of the camera.

	*
		- aspectRatio

		- Aspect ration of the camera.

