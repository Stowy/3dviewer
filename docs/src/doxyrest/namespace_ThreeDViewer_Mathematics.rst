.. index:: pair: namespace; ThreeDViewer.Mathematics
.. _doxid-namespace_three_d_viewer_1_1_mathematics:

namespace ThreeDViewer.Mathematics
==================================

.. toctree::
	:hidden:

	namespace_ThreeDViewer_Mathematics_Geometry.rst
	struct_ThreeDViewer_Mathematics_Axis.rst
	struct_ThreeDViewer_Mathematics_Matrix4.rst
	struct_ThreeDViewer_Mathematics_Vector3.rst
	struct_ThreeDViewer_Mathematics_Vector4.rst
	class_ThreeDViewer_Mathematics_NormalizeVectorException.rst




.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	namespace Mathematics {

	// namespaces

	namespace :ref:`ThreeDViewer.Mathematics.Geometry<doxid-namespace_three_d_viewer_1_1_mathematics_1_1_geometry>`;

	// structs

	struct :ref:`Axis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_axis>`;
	struct :ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>`;
	struct :ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`;
	struct :ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`;

	// classes

	class :ref:`NormalizeVectorException<doxid-class_three_d_viewer_1_1_mathematics_1_1_normalize_vector_exception>`;

	} // namespace Mathematics
