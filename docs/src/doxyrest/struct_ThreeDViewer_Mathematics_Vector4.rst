.. index:: pair: struct; ThreeDViewer.Mathematics.Vector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4:

struct ThreeDViewer.Mathematics.Vector4
=======================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents a 3D position vector with homogeneous coordinates. :ref:`More...<details-struct_three_d_viewer_1_1_mathematics_1_1_vector4>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	struct Vector4: IEquatable< Vector4 >
	{
		// fields
	
		static readonly int :ref:`SizeInBytes<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a5ace29ca20ad79d214f88958211e5067>` = Unsafe.SizeOf<Vector4>();
		static readonly Vector4 :ref:`Origin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a636bd7dbf2ae15e702a983898637ac52>` = new(0, 0, 0, 0);
		static readonly Vector4 :ref:`XAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a993e69f8ac54a7d95533eda7dc05705e>` = new(1, 0, 0, 0);
		static readonly Vector4 :ref:`YAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aabaa91118168198542570a84f7624344>` = new(0, 1, 0, 0);
		static readonly Vector4 :ref:`ZAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af0208df497c82cd18fa7c620cb24cb1a>` = new(0, 0, 1, 0);
		static readonly Vector4 :ref:`WAxis<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab92e462174c0137cc174b0b5041d08eb>` = new(0, 0, 0, 1);
		static readonly Vector4 :ref:`MinValue<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af12959948e2bb463a084a7d9e1925cb9>` = new(double.MinValue, double.MinValue, double.MinValue, double.MinValue);
		static readonly Vector4 :ref:`MaxValue<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1abc8cb7a334614a94f783736af267174f>` = new(double.MaxValue, double.MaxValue, double.MaxValue, double.MaxValue);
		static readonly Vector4 :ref:`Epsilon<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aa948acdfac327440dd285b6d6e94be7c>` = new(double.Epsilon, double.Epsilon, double.Epsilon, double.Epsilon);
		static readonly Vector4 :ref:`One<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab0c4c4abafafd8c4dcca6a8484e8ef5d>` = new(1, 1, 1, 1);
		static readonly Vector4 :ref:`Zero<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a508e81b543b05905a0f5c6d6dd13cbd7>` = :ref:`Origin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a636bd7dbf2ae15e702a983898637ac52>`;
		static readonly Vector4 :ref:`NaN<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a2a504eea2eef884f2542dc8bf3543318>` = new(double.NaN, double.NaN, double.NaN, double.NaN);

		// properties
	
		double :ref:`X<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a932210f2a8e23840382fb1baa0878f79>`;
		double :ref:`Y<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a39309eabd94952199c25533f1ae0e8d4>`;
		double :ref:`Z<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a2f32c1983fc47dca8a2057ff5a18060c>`;
		double :ref:`W<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1afcbb8559459e42f366dec03462f830fc>`;
		double[] :ref:`Array<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a9b64dbb808b0be139ac327409a5c9f70>`;
		double :ref:`Magnitude<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab7a6c71bd83c35d4ef81e8d5b329b57a>`;
		double :ref:`MagnitudeSquared<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a7f4bcb58a4d6f6fcd9220d43d04b84cb>`;
		Vector4 :ref:`Normalized<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a76f3c264fe3286efac87db51a217d332>`;
		double :ref:`this[int index]<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab6f760673757311cf019d4f5155b42d6>`;

		// methods
	
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ad70cd02b04b425fc0381d9fb5c40a694>`(double x, double y, double z, double w);
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a34209c37054c6c2f986c277633845c7e>`(double[] xyzw);
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab8c7ab8dafea28361a0040fff787f0db>`(Vector4 v1);
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab06d31bd3d8e56229f7e5bdba876e299>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` v1);
		:ref:`Vector4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a7eea931b276c943a8ab5bba38c0b8416>`(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` v1, double w);
		double :ref:`Dot<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aefcf62fc8a23ae44924cb3bda549327d>`(Vector4 other);
		Vector4 :ref:`SquareComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a7ce39ec20844f7945739475f1f044324>`();
		double :ref:`SumComponents<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a309d62e3eaace9ddc27ec1bd7df75c2e>`();
		Vector4 :ref:`Normalize<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a1cd2e3423c735af7c9d87f8f064bcd5b>`();
		:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` :ref:`ToPhysicalCoords<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab4863acfb7b75f4f6b0f5f17419219ce>`();
		PointF :ref:`ToPointF<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a9ca41ff85644af725fb839eef399da5c>`();
		bool :target:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a64b3104b57a6988e098987e8edd01726>`(Vector4 other);
		override bool :target:`Equals<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aea03484e2372d62c299b0930247937bc>`(object obj);
		override int :target:`GetHashCode<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab626983eab1821333fcccb174af0c9b7>`();
		static Vector4 :target:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a63e58f71dcc78b5edc5aaa1a845245fe>` (Vector4 v1, Vector4 v2);
		static Vector4 :target:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a16951fc8d030b08cdbb3c43259f471c8>` (Vector4 v1, Vector4 v2);
		static Vector4 :target:`operator -<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af815b8a231b0c026b92cd54b03d3a1d7>` (Vector4 v1);
		static Vector4 :target:`operator +<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a932dccd25756a04e8fbb5fc42d8c3779>` (Vector4 v1);
		static bool :target:`operator <<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1abc7a151f1186a13ecb6c1057ccfb7931>` (Vector4 v1, Vector4 v2);
		static bool :target:`operator <=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af9f4f245daf6e202b794853872bee36d>` (Vector4 v1, Vector4 v2);
		static bool :target:`operator ><doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af8b5dfafa62b59527841bbedc2ff818e>` (Vector4 v1, Vector4 v2);
		static bool :target:`operator >=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a1a80badc9ed8ad7150a22f9b7e7102d3>` (Vector4 v1, Vector4 v2);
		static bool :target:`operator ==<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aac4dbbb39643b32eec0aebd03bd1cab7>` (Vector4 v1, Vector4 v2);
		static bool :target:`operator !=<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1adedf1c747cdb44ff4486348efd9f0453>` (Vector4 v1, Vector4 v2);
		static Vector4 :target:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a5aec6926fa7b8c1bfb86749a0c3dd747>` (Vector4 v1, double s2);
		static Vector4 :target:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aa7f506027ed619398d3dd60c93a7151e>` (double s1, Vector4 v2);
		static Vector4 :target:`operator/<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a29d2338834658c739213f7e71be2374f>` (Vector4 v1, double s2);
		static Vector4 :ref:`operator *<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab1dbad9d389c06efb1114d62f20d612b>` (Vector4 v1, :ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` m2);
	};
.. _details-struct_three_d_viewer_1_1_mathematics_1_1_vector4:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents a 3D position vector with homogeneous coordinates.

Based on this tutorial : `https://www.codeproject.com/articles/17425/a-vector-type-for-c <https://www.codeproject.com/articles/17425/a-vector-type-for-c>`__.

Fields
------

.. index:: pair: variable; SizeInBytes
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a5ace29ca20ad79d214f88958211e5067:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly int SizeInBytes = Unsafe.SizeOf<Vector4>()

Size of the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` struct in bytes.

.. index:: pair: variable; Origin
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a636bd7dbf2ae15e702a983898637ac52:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 Origin = new(0, 0, 0, 0)

An origin vector. Is equal to Vector(0, 0, 0, 0). Same as Zero.

.. index:: pair: variable; XAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a993e69f8ac54a7d95533eda7dc05705e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 XAxis = new(1, 0, 0, 0)

Represents the X axis. Is equal to Vector(1, 0, 0, 0).

.. index:: pair: variable; YAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aabaa91118168198542570a84f7624344:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 YAxis = new(0, 1, 0, 0)

Represents the Y axis. Is equal to Vector(0, 1, 0, 0).

.. index:: pair: variable; ZAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af0208df497c82cd18fa7c620cb24cb1a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 ZAxis = new(0, 0, 1, 0)

Represents the Z axis. Is equal to Vector(0, 0, 1, 0).

.. index:: pair: variable; WAxis
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab92e462174c0137cc174b0b5041d08eb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 WAxis = new(0, 0, 0, 1)

Represents the W axis. Is equal to Vector(0, 0, 0, 1).

.. index:: pair: variable; MinValue
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1af12959948e2bb463a084a7d9e1925cb9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 MinValue = new(double.MinValue, double.MinValue, double.MinValue, double.MinValue)

A vector with every components equal to double.MinValue.

.. index:: pair: variable; MaxValue
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1abc8cb7a334614a94f783736af267174f:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 MaxValue = new(double.MaxValue, double.MaxValue, double.MaxValue, double.MaxValue)

A vector with every components equal to double.MaxValue.

.. index:: pair: variable; Epsilon
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aa948acdfac327440dd285b6d6e94be7c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 Epsilon = new(double.Epsilon, double.Epsilon, double.Epsilon, double.Epsilon)

A vector with every components equal to double.Epsilon.

.. index:: pair: variable; One
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab0c4c4abafafd8c4dcca6a8484e8ef5d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 One = new(1, 1, 1, 1)

An instance with all components set to 1.

.. index:: pair: variable; Zero
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a508e81b543b05905a0f5c6d6dd13cbd7:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 Zero = :ref:`Origin<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a636bd7dbf2ae15e702a983898637ac52>`

A zero vector. Is equal to Vector(0, 0, 0, 0). Same as Origin.

.. index:: pair: variable; NaN
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a2a504eea2eef884f2542dc8bf3543318:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static readonly Vector4 NaN = new(double.NaN, double.NaN, double.NaN, double.NaN)

A vector with every components equal to double.NaN.

Properties
----------

.. index:: pair: property; X
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a932210f2a8e23840382fb1baa0878f79:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double X

Gets the X component of the vector.

.. index:: pair: property; Y
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a39309eabd94952199c25533f1ae0e8d4:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Y

Gets the Y component of the vector.

.. index:: pair: property; Z
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a2f32c1983fc47dca8a2057ff5a18060c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Z

Gets the Z component of the vector.

.. index:: pair: property; W
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1afcbb8559459e42f366dec03462f830fc:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double W

Gets the W component of the vector.

.. index:: pair: property; Array
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a9b64dbb808b0be139ac327409a5c9f70:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double[] Array

Gets the vector as an array.

.. index:: pair: property; Magnitude
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab7a6c71bd83c35d4ef81e8d5b329b57a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Magnitude

Gets the magnitude (aka. length or absolute value) of the vector.

.. index:: pair: property; MagnitudeSquared
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a7f4bcb58a4d6f6fcd9220d43d04b84cb:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double MagnitudeSquared

Gets the squared magnitude of this vector, can be used for better performance than Magnitude.

.. index:: pair: property; Normalized
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a76f3c264fe3286efac87db51a217d332:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4 Normalized

Gets this vector but normalized.

.. index:: pair: property; this[int index]
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab6f760673757311cf019d4f5155b42d6:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double this[int index]

An index accessor for a vector, mapping index [0] -> X, [1] -> Y, [2] -> Z and [3] -> W.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- index

		- The array index referring to a component within the vector (i.e. x, y, z, w).



.. rubric:: Returns:

Returns X if 0, Y if 1, Z if 2 and W if 3.

Methods
-------

.. index:: pair: function; Vector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ad70cd02b04b425fc0381d9fb5c40a694:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4(double x, double y, double z, double w)

Initializes a new instance of the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- x

		- X component of the vector.

	*
		- y

		- Y component of the vector.

	*
		- z

		- Z component of the vector.

	*
		- w

		- W component of the vector.

.. index:: pair: function; Vector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a34209c37054c6c2f986c277633845c7e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4(double[] xyzw)

Initializes a new instance of the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- xyzw

		- Array with the X, Y, Z and W components fo the vector.

	*
		- ArgumentException

		- Thrown if the array argument does not contain exactly four components.

.. index:: pair: function; Vector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab8c7ab8dafea28361a0040fff787f0db:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4(Vector4 v1)

Initializes a new instance of the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` representing the new values for the vector.

.. index:: pair: function; Vector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab06d31bd3d8e56229f7e5bdba876e299:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` v1)

Initializes a new instance of the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` to create the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` from.

.. index:: pair: function; Vector4
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a7eea931b276c943a8ab5bba38c0b8416:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4(:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` v1, double w)

Initializes a new instance of the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` struct.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector 3 that contains the X, Y and Z components of that vector.

	*
		- w

		- The W element of the new vector.

.. index:: pair: function; Dot
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1aefcf62fc8a23ae44924cb3bda549327d:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double Dot(Vector4 other)

Determines the dot product of two vectors.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- other

		- The vector to multiply by.



.. rubric:: Returns:

Returns a scalar representing the dot product of the two vectors.

.. index:: pair: function; SquareComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a7ce39ec20844f7945739475f1f044324:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4 SquareComponents()

Squares the vector's components.



.. rubric:: Returns:

The vector with his components squared.

.. index:: pair: function; SumComponents
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a309d62e3eaace9ddc27ec1bd7df75c2e:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	double SumComponents()

Sums the components of the vector.



.. rubric:: Returns:

Returns the sum of the components of the vectors.

.. index:: pair: function; Normalize
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a1cd2e3423c735af7c9d87f8f064bcd5b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Vector4 Normalize()

Normalizes the vector.



.. rubric:: Returns:

Returns the normalized vector.

.. index:: pair: function; ToPhysicalCoords
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab4863acfb7b75f4f6b0f5f17419219ce:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Vector3<doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>` ToPhysicalCoords()

Converts the :ref:`Vector4 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4>` to physical coords in a :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`. Divides the X, Y and Z components by W. If W = 0, it just gives the X, Y and Z components in a :ref:`Vector3 <doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector3>`.



.. rubric:: Returns:

Returns the physical coordinates of this vector.

.. index:: pair: function; ToPointF
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1a9ca41ff85644af725fb839eef399da5c:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	PointF ToPointF()

Converts this vector to a PointF. Firsts gets his physical coordinates, then only keeps the X and Y components.



.. rubric:: Returns:

A PointF containing the physical X and Y coordinates of the vector.

.. index:: pair: function; operator*
.. _doxid-struct_three_d_viewer_1_1_mathematics_1_1_vector4_1ab1dbad9d389c06efb1114d62f20d612b:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static Vector4 operator * (Vector4 v1, :ref:`Matrix4<doxid-struct_three_d_viewer_1_1_mathematics_1_1_matrix4>` m2)

Multiplies a vector with a matrix.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- v1

		- The vector to multiply by the matrix.

	*
		- m2

		- The matrix to multiply the vector by.



.. rubric:: Returns:

The vector that results from the multiplication.

