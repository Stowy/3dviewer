.. index:: pair: class; ThreeDViewer.Scene
.. _doxid-class_three_d_viewer_1_1_scene:

class ThreeDViewer.Scene
========================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Represents a scene that contains entites and has a camera. :ref:`More...<details-class_three_d_viewer_1_1_scene>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class Scene
	{
	public:
		// properties
	
		:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` :ref:`Entities<doxid-class_three_d_viewer_1_1_scene_1ae0a309123677ba38b65c47cac6d0d2c9>`;
		List<:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`> :ref:`SortedAbsoluteFaces<doxid-class_three_d_viewer_1_1_scene_1a00a0c42047b7cb92992ba3324205a9e3>`;
		:ref:`Camera<doxid-class_three_d_viewer_1_1_camera>` :ref:`Camera<doxid-class_three_d_viewer_1_1_scene_1aefb6fa9afbba34f133d9d64b558acf4a>`;

		// methods
	
		:ref:`Scene<doxid-class_three_d_viewer_1_1_scene_1a360c40eb16007ccc1f93dc4b1b42f6a5>`();
		:ref:`Scene<doxid-class_three_d_viewer_1_1_scene_1adc116af8979661c7908d9e5b7e3c1d61>`(:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` entities);
		:ref:`Scene<doxid-class_three_d_viewer_1_1_scene_1aa694ef5de83a34a1f7154c08d9356663>`(:ref:`Camera<doxid-class_three_d_viewer_1_1_camera>` camera);
		:ref:`Scene<doxid-class_three_d_viewer_1_1_scene_1ad36258b989d4b00673d5f490394f2cc6>`(:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` entities, :ref:`Camera<doxid-class_three_d_viewer_1_1_camera>` camera);
	};
.. _details-class_three_d_viewer_1_1_scene:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Represents a scene that contains entites and has a camera.

Properties
----------

.. index:: pair: property; Entities
.. _doxid-class_three_d_viewer_1_1_scene_1ae0a309123677ba38b65c47cac6d0d2c9:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` Entities

Gets the collection of entities in the scene.

.. index:: pair: property; SortedAbsoluteFaces
.. _doxid-class_three_d_viewer_1_1_scene_1a00a0c42047b7cb92992ba3324205a9e3:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	List<:ref:`Face<doxid-class_three_d_viewer_1_1_mathematics_1_1_geometry_1_1_face>`> SortedAbsoluteFaces

Gets all the absolute faces sorted from the furthest to the camera to the closest.

.. index:: pair: property; Camera
.. _doxid-class_three_d_viewer_1_1_scene_1aefb6fa9afbba34f133d9d64b558acf4a:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	:ref:`Camera<doxid-class_three_d_viewer_1_1_camera>` Camera

Gets the camera of the scene.

Methods
-------

.. index:: pair: function; Scene
.. _doxid-class_three_d_viewer_1_1_scene_1a360c40eb16007ccc1f93dc4b1b42f6a5:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Scene()

Initializes a new instance of the :ref:`Scene <doxid-class_three_d_viewer_1_1_scene>` class.

.. index:: pair: function; Scene
.. _doxid-class_three_d_viewer_1_1_scene_1adc116af8979661c7908d9e5b7e3c1d61:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Scene(:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` entities)

Initializes a new instance of the :ref:`Scene <doxid-class_three_d_viewer_1_1_scene>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- entities

		- Entities contained in the scene.

.. index:: pair: function; Scene
.. _doxid-class_three_d_viewer_1_1_scene_1aa694ef5de83a34a1f7154c08d9356663:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Scene(:ref:`Camera<doxid-class_three_d_viewer_1_1_camera>` camera)

Initializes a new instance of the :ref:`Scene <doxid-class_three_d_viewer_1_1_scene>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- camera

		- :ref:`Camera <doxid-class_three_d_viewer_1_1_camera>` of the scene.

.. index:: pair: function; Scene
.. _doxid-class_three_d_viewer_1_1_scene_1ad36258b989d4b00673d5f490394f2cc6:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	Scene(:ref:`EntityCollection<doxid-class_three_d_viewer_1_1_entity_collection>` entities, :ref:`Camera<doxid-class_three_d_viewer_1_1_camera>` camera)

Initializes a new instance of the :ref:`Scene <doxid-class_three_d_viewer_1_1_scene>` class.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- entities

		- The entities contained in the scene.

	*
		- camera

		- The camera of the scene.

