.. index:: pair: class; ThreeDViewer.Utils.DoubleExtension
.. _doxid-class_three_d_viewer_1_1_utils_1_1_double_extension:

class ThreeDViewer.Utils.DoubleExtension
========================================

.. toctree::
	:hidden:

Overview
~~~~~~~~

Class that contains all the extensions for the double class. :ref:`More...<details-class_three_d_viewer_1_1_utils_1_1_double_extension>`


.. ref-code-block:: C#
	:class: doxyrest-overview-code-block

	
	class DoubleExtension
	{
	public:
		// methods
	
		static bool :ref:`AlmostEqualsWithAbsTolerance<doxid-class_three_d_viewer_1_1_utils_1_1_double_extension_1ae496ef45a9781564a96e4815e4189ffe>`(
			this double a,
			double b,
			double maxAbsoluteError
			);
	
		static double :ref:`Map<doxid-class_three_d_viewer_1_1_utils_1_1_double_extension_1a9ddef01e1d3d0812ea2a8e7f4aba1e04>`(this double s, double a1, double a2, double b1, double b2);
	};
.. _details-class_three_d_viewer_1_1_utils_1_1_double_extension:

Detailed Documentation
~~~~~~~~~~~~~~~~~~~~~~

Class that contains all the extensions for the double class.

Methods
-------

.. index:: pair: function; AlmostEqualsWithAbsTolerance
.. _doxid-class_three_d_viewer_1_1_utils_1_1_double_extension_1ae496ef45a9781564a96e4815e4189ffe:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static bool AlmostEqualsWithAbsTolerance(
		this double a,
		double b,
		double maxAbsoluteError
		)

Comparator within an absolute tolerance.



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- a

		- The double to compare to.

	*
		- b

		- The double to compare with.

	*
		- maxAbsoluteError

		- The tolerance for the comparison compared against the difference of the two doubles.



.. rubric:: Returns:

True if the doubles are equal within a tolerance.

.. index:: pair: function; Map
.. _doxid-class_three_d_viewer_1_1_utils_1_1_double_extension_1a9ddef01e1d3d0812ea2a8e7f4aba1e04:

.. ref-code-block:: C#
	:class: doxyrest-title-code-block

	static double Map(this double s, double a1, double a2, double b1, double b2)

Linearly maps the value s that's in the range [a1, a2] to the range [b1, b2].



.. rubric:: Parameters:

.. list-table::
	:widths: 20 80

	*
		- s

		- The value to map.

	*
		- a1

		- The first limit of the range that s belongs to.

	*
		- a2

		- The second limit of the range that s belongs to.

	*
		- b1

		- The first limit of the ouput range.

	*
		- b2

		- The second limit of the ouput range.



.. rubric:: Returns:

The mapped value in the [b1, b2] range.

