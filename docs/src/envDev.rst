Environnement de développement
===============================

Chocolatey
----------

Afin d'installer les différents logiciels nécessaires, j'ai utilisé un gestionnaire de paquet pour Windows nommé `Chocolatey <https://chocolatey.org/>`__.

Une fois chocolatey installé, il suffit de lancer un terminal ``Powershell`` en administrateur et de lancer cette commande pour installer les logiciels nécéssaires :

.. code-block:: powershell

   choco install dotnet-5.0-sdk pandoc python3 typora visualstudio2019community vscode-insiders git strawberryperl miktex

Documentation
-------------

Sphinx
^^^^^^

J'ai suivis `ce guide <https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html>`_ pour mettre en place la documentation pour la première fois. 

Afin de pouvoir compiler la documentation pour voir les modifications, il faut installer des dépendances ``Python`` qui se trouvent dans le fichier ``docs/requirements.txt``.
Python à déjà été installé par la commande précédente.
Pour installer les dépendances python, utilisez cette commande dans le dossier docs :

.. code-block:: bash

   pip3 install -r requirements.txt

Il est maintenant possible de compiler la documentation.

Sur bash :

.. code-block:: bash

   make html

Sur Windows :

.. code-block:: powershell

   ./make.bat html

Pour ce qui est de la page ``readthedocs``, elle se met à jour à chaque push sur le git.

.. figure:: ./img/docRtd.png
   :alt: Documentation visible sur readthedocs.org.
   :scale: 60%
   :align: center

   Documentation visible sur readthedocs.org.

Doxygen
^^^^^^^

Afin de pouvoir voir la documentation de mon code source sans avoir à l'ouvrir, j'ai décidé d'utiliser Doxygen.
Bien que ce programme permette de créer une documentation au format ``HTML``, je voulais un moyen d'y intégrer dans ma documentation existante afin de pouvoir y accéder sur ``readthedocs``.
J'ai donc utilisé un programme qui s'appelle `Doxyrest <https://github.com/vovkos/doxyrest>`_.
Il permet de prendre la documentation au format ``XML`` générée par ``Doxygen`` et de la convertir au format ``reStructuredText`` que ma documentation utilise.
Afin de rendre la compilation de tout ça, j'ai créé un script que je peux lancer quand je veux mettre à jour ma doc.

.. code-block:: batch

   @REM updt-doxy.bat

   @REM Met a jour les fichiers Doxygen
   cd ..
   doxygen .\Doxyfile 
   cd docs

   @REM Les convertis en reStructuredText
   .\doxyrest\bin\doxyrest.exe -c .\doxyrest-config.lua

J'ai également organisé les fichiers sources de ma doc afin de rendre tout cela possible.

Les documents ``rst`` que j'écris se trouvent dans un dossier ``src`` qui se trouve dans le dossier ``docs``.
Dans ce dossier ``rst``, se trouve un sous-dossier, ``doxyrest`` qui contient la documentation Doxygen.
J'ai ensuite ajouté le fichier qui contient la doc dans mon ``toctree`` qui se trouve dans le fichier ``index.rst`` comme ceci :

.. code-block:: reStructuredText

   .. toctree::
      :maxdepth: 3
      :numbered:
      :caption: Table des matières

      intro
      cdc
      organisation
      technologies
      envDev
      doxyrest/doxyrest

J'ai également changé le ``Makefile`` par défaut afin qu'il compile dans le dossier ``src`` et non dans le dossier ``docs`` en ajoutant cette ligne :

.. code-block:: batch

   set SOURCEDIR=./src

LaTeX
^^^^^

Sphinx permet d'exporter son document en LaTeX, ce qui permet ensuite de le compiler en PDF.
Ce PDF est également téléchargeable directement sur readthedocs en cliquant sur ce bouton :

.. figure:: ./img/dowloadReadthedoc.png
   :alt: Bouton du menu de readthedocs.
   :align: center

   Bouton du menu de readthedocs.

.. figure:: ./img/downloadPdfRTD.png
   :alt: Menu de readthedocs avec téléchargement en PDF.
   :align: center

   Menu de readthedocs avec téléchargement en PDF.

Il est possible de compiler ce PDF en local avec ces commandes : 

.. code-block:: bash

   # in docs/ folder
   make latex
   cd _build/latex
   make