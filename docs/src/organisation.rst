Organisation
============

Gestion de projet
-----------------

Afin de pouvoir suivre l'avancement de mon projet et de pouvoir prévoir les tâches à accomplir, je voulais un programme qui me permet d'avoir un système de ``Kanban``.
Le programme en question devrait pouvoir me laisser créer de taches qui puissent avoir une description et un moyen de les catégoriser.
Il devrait également pouvoir les mettre dans des colonnes "Backlog", "To Do", "In Progress" et "Done".

Notion
^^^^^^

Un programme qui permet de faire ça est ``Notion``.
C'est une application Web qui peut également tourner sur Windows, macOS et Linux à l'aide d'Electron.
Ce service est à la base une application pour prendre des notes, mais qui possède beaucoup d'autres fonctionnalités, notamment un ``Kanban``.

.. figure:: ./img/notionKanban.png
   :alt: Un Kanban sur Notion.
   :scale: 60%
   :align: center

   Un Kanban sur Notion.

Elle possède aussi une fonctionnalité de timeline, qui permet de placer ses tâches dans le temps et d'y visualiser facilement.

.. figure:: ./img/notionTimeline.png
   :alt: Vue Timeline sur Notion.
   :scale: 60%
   :align: center

   Vue Timeline sur Notion.

Malheureusement, après avoir placé mes tâches dans l'optique de faire un planning prévisionnel, je me suis rendu compte que je n'aimais pas tellement l'UX de ``Notion`` quand on l'utilise comme Kanban.
J'ai donc décidé d'utiliser une autre application que j'avais utilisée auparavant, ``Trello``.

Trello
^^^^^^

``Trello`` est une application Web de gestion de projet qui permet de mettre en place des Kanban facilement sur lesquels on peut collaborer.
Elle possède une interface intuitive et un très bon "look and feel".
C'est ultimement le site que j'ai décidé d'utiliser dans le cadre de mon travail de diplôme, car son interface et sa facilité d'utilisation le rend idéal dans ce contexte.

.. figure:: ./img/trelloKanban.png
   :alt: Screenshot de Trello.
   :scale: 60%
   :align: center

   Screenshot de Trello.

Format de documentation
-----------------------

J'avais pour habitude de faire mes documentations en ``LaTeX`` étant donné que l'école, en général, nous demande des documents ``PDF`` avec numéros dans les titres, numéros de figures, numéros de pages, etc.
Cela dit, M. Bonvin m'a recommandé de faire une documentation moderne en ``Markdown`` qui serait ensuite compilée en ``HTML`` et publié sur `readthedocs <https://readthedocs.org/>`_.
Je me suis donc renseigné sur la manière de publier une documentation sur ce site, et leur article recommande d'utiliser ``Sphinx`` pour générer la documentation.
Cet outil utilise un format qui s'appelle le ``reStructuredText``.
Il est également possible d'utiliser ``Markdown``, mais ``readthedocs`` et `cet article <https://www.ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/>`_ recommandent d'utiliser ``reStructuredText``. J'ai donc utilisé ``Markdown`` pour le logbook et ``reStructuredText`` pour la documentation technique.

Je trouve que cette méthode de production de documentations a beaucoup plus de sens qu'un document ``PDF`` étant donné qu'on ne lit jamais une documentation sur un ``PDF`` ou imprimé, mais plutôt sur des sites web. Aussi, il est possible de télécharger la documentation produite en format ``HTML``, ``PDF`` ou ``EPUB`` depuis ``readthedocs`` si cela est nécessaire.

Système de sauvegarde et de versionnage
----------------------------------------

Afin de sauvegarder et de gérer les différentes versions de mon code, j'ai décidé d'utiliser 3 systèmes de sauvegarde.

Le premier est ``Git``, qui permet de gérer différentes versions de mon projet, et donc de "retourner dans le temps". Ce ``Git`` est hébergé sur `GitLab <https://gitlab.com/Stowy/3dviewer>`_.
Le deuxième est mon disque sur lequel je fais périodiquement une sauvegarde.
Le troisième est mon ordinateur chez moi, sur lequel je travaille et qui a donc une copie du ``Git`` sur lui.