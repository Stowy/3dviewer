*****************************
Éléments mathématiques
*****************************

Ce projet est principalement centré sur le fait de prendre des concepts mathématiques et de les "traduire" en C#.

Les livres qui m'ont aidé sont : 


*Mathematical Elements for Computer Graphics - Second Edition* écrit par *David F. Rogers* et *J. Alan Adams*.

.. figure:: ./img/coverMathElements.jpeg
   :alt: Couverture de Mathematical Elements for Computer Graphics - Second Edition.
   :align: center
   :scale: 30%

   Couverture de *Mathematical Elements for Computer Graphics - Second Edition*.


*Inforgraphie II* écrit par *Philippe Schweizer*

.. figure:: ./img/infographie.jpg
   :alt: Couverture de Inforgraphie II.
   :align: center

   Couverture de *Inforgraphie II*.


Vecteurs
========

En math il y a deux types de grandeurs.

Tout d'abord il y a les **grandeurs scalaires**, elles sont déterminées par leur valeur et leurs unités.

Ensuite, il y a les **grandeurs vectorielles**, elles sont déterminées par leur valeur et leurs unités, mais possèdent en plus une orientation.

Le choix de l'une ou l'autre dépend de la nature de la grandeur.
Par exemple la masse est une grandeur scalaire, car il n'y a pas d'orientation à celle-ci.
Par contre, pour la vitesse, on utilisera un vecteur, car on ne va pas juste vite, on va vite dans une direction.

Cela dit, avoir juste une valeur et un angle n'est pas pratique pour faire des calculs.
C'est pour cela qu'il existe deux moyens d'écrire un vecteur.

Les **vecteurs polaires**, qui ont une valeur et un angle. Par ex : :math:`\vec{v_{1}} = (v_{1} [m/s] ; \alpha °)`

Et les **vecteurs cartésiens**, qui sont comme des coordonnées. Par ex : :math:`\vec{v_{1}} = (v_{1x}; v_{1y})`

Dans le cadre de ce projet, les vecteurs sont utilisés afin de décrire une position (vecteur position).
Nous utilisons donc des vecteurs cartésiens.

Matrices
========

Les matrices sont des tableaux de valeurs. Elles sont représentées comme ceci : 

.. math::
   :label: Exemple de matrice

    \begin{bmatrix}
    1 & 2 & 3\\
    4 & 5 & 6
    \end{bmatrix}

Les matrices ont différentes propriétés, notamment leur taille.
Par exemple la matrice :eq:`Exemple de matrice` à 2 lignes et 3 colonnes.
On note cette taille : :math:`2 \times 3`.

On précise d'abord le nombre de lignes, puis le nombre de colonnes (ou [y, x]).

Une matrice peut également avoir une seule ligne :

.. math::
   :label: Matrice-ligne
 
   \begin{bmatrix}
   1 & 2 & 3
   \end{bmatrix}

En anglais on appelle ça une "row matrix", qui peut se traduire par matrice-ligne.
S'il y a des matrices-ligne, ça veut dire qu'il y a aussi des matrices-colonnes (ou "column matrix") :

.. math::
   :label: Matrice-colonne
 
   \begin{bmatrix}
   1\\
   2\\
   3
   \end{bmatrix}

Ces matrices peuvent être utilisées pour représenter des points, qui peuvent aussi être en 2D :

.. math::
   :label: Point 2D
 
   \begin{bmatrix}
   1\\
   2
   \end{bmatrix}

Les matrices de ce genre sont souvent appelées vecteur de position.
En effet, un vecteur peut être considéré comme une matrice avec une seule ligne ou colonne.

Cela dit, il faut choisir si l'on veut travailler avec des vecteurs colonne ou des vecteurs ligne.
Dans ce projet, j'ai choisi de travailler avec des vecteurs ligne, car c'est ce que le livre *Mathematical Elements for Computer Graphics* utilise.

Un point dans un espace 3D est donc représenté par :

.. math::
   :label: Point 3D
 
   \begin{bmatrix}
   x & y & z
   \end{bmatrix}

Opérations
----------

Multiplication par un nombre réel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le résultat de la multiplication d’une matrice par un scalaire (un nombre réel) est une matrice
dont tous les éléments sont multipliés par ce scalaire.

Exemple : 

.. math::

   2 \cdot \begin{bmatrix}
   3 & 5 & 0\\
   2 & 4 & -1
   \end{bmatrix} = \begin{bmatrix}
   2 \cdot 3 & 2 \cdot 5 & 2 \cdot 0\\
   2 \cdot 2 & 2 \cdot 4 & 2 \cdot -1
   \end{bmatrix} = \begin{bmatrix}
   6 & 10 & 0\\
   4 & 8 & -2
   \end{bmatrix}

Addition et soustraction de matrices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les matrices à additionner (soustraire) doivent être de même taille (m,n). Le résultat est alors
une matrice de même taille (m,n) dont chaque élément est la somme (différence) des éléments à
la même position des deux matrices.

Autrement dit, pour l’opération de matrices :math:`A \pm B = C` l’élément de la i\ :sup:`ème` ligne et j\ :sup:`ème` colonne
de C s’obtient de la manière suivante : :math:`C_{i,j} = A_{i,j} + B_{i,j}`.

Exemples : 

.. math::
        
   \begin{bmatrix}
   6 & 4\\
   -2 & 8\\
   3 & -10
   \end{bmatrix} + \begin{bmatrix}
   1 & 4\\
   2 & 5\\
   3 & 6
   \end{bmatrix} = \begin{bmatrix}
   6 + 1 & 4 + 4\\
   -2 + 2 & 8 + 5\\
   3 + 3 & -10 + 6
   \end{bmatrix} = \begin{bmatrix}
   7 & 8\\
   0 & 13\\
   6 & -4
   \end{bmatrix}

.. math::
        
   \begin{bmatrix}
   1 & 2
   \end{bmatrix} - \begin{bmatrix}
   3 & -4
   \end{bmatrix} = \begin{bmatrix}
   1 - 3 & 2 - (-4)
   \end{bmatrix} = \begin{bmatrix}
   -2 & 6
   \end{bmatrix}

Multiplication de matrices
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Important::
   - La multiplication de matrices n'est pas commutative : :math:`AB \ne BA`
   - Pour multiplier deux matrices, il faut que le nombre de colonnes de la première soit égal au nombre de lignes de la deuxième : :math:`(m,\textbf{n}) \cdot (\textbf{n},k)`
   - La multiplication de matrices est associative : :math:`(AB)C = A(BC)`

Le résultat de la multiplication d’une matrice de taille :math:`(m,n)` par une matrice de taille :math:`(n,k)` est une matrice de taille :math:`(m,k)`.

Pour le produit :math:`AB = C` l’élément de la i\ :sup:`ème` ligne et j\ :sup:`ème` colonne de :math:`C` est la somme des 
produits des éléments de même rang dans la i\ :sup:`ème` ligne de :math:`A` et dans la j\ :sup:`ème` colonne de :math:`B`, c’est-à-dire :

.. math::

   C_{i,j} = A_{i,1} \cdot B_{1,j} + A_{i,2} \cdot B_{2,j} + \cdots + A_{i,n} \cdot B_{n,j}

Exemple :

.. math::

   \begin{bmatrix}
   0 & 3 & 4\\
   1 & 2 & 5
   \end{bmatrix} \cdot \begin{bmatrix}
   6 & 7\\
   9 & 8\\
   10 & 11
   \end{bmatrix} = \begin{bmatrix}
   a & b\\
   c & d
   \end{bmatrix} = \begin{bmatrix}
   67 & 68\\
   74 & 78   \end{bmatrix}

.. math::

   a = 0 \cdot 6 + 3 \cdot 9 + 4 \cdot 10 = 76

   b = 0 \cdot 7 + 3 \cdot 8 + 4 \cdot 11 = 68

   c = 1 \cdot 6 + 2 \cdot 9 + 5 \cdot 10 = 74

   d = 1 \cdot 7 + 2 \cdot 8 + 5 \cdot 11 = 78

Coordonnées homogènes
=====================

Dans 3D Viewer les points ne sont pas juste représentés par 3 points comme ceci :

.. math::
 
   \begin{bmatrix}
   x & y & z
   \end{bmatrix}

Mais par 4 points :

.. math::
 
   \begin{bmatrix}
   x & y & z & w
   \end{bmatrix}

Ce point utilise les coordonnées homogènes.
C'est dans le but de pouvoir inclure les translations dans les transformations possibles.

Pour obtenir les coordonnées physiques, on peut utiliser cette formule :

.. math::

   \begin{bmatrix}
   x^* & y^* & z^* & 1
   \end{bmatrix} = \begin{bmatrix}
   \frac{x}{w} & \frac{y}{w} & \frac{z}{w} & 1
   \end{bmatrix}

Transformations dans le plan
============================

Soit :math:`[x, y, z, 1]` un point dans le plan :math:`\mathbb{R} ^ 3` (avec les coordonnées homogènes, comme précisé précédemment).
On peut lui appliquer une transformation dans le plan.
Pour connaître les coordonnées du point résultant, on applique à ce point une matrice particulière,
c’est-à-dire qu’on multiplie le vecteur-ligne par la matrice correspondant à la transformation.

Pour appliquer une transformation à un segment, on applique cette transformation aux deux
points qui sont les extrémités de ce segment.
C'est le même principe pour une face, ou on applique la transformation sur les trois points de cette face.

Rotation
--------

Axe X
^^^^^

Une rotation d'un angle :math:`\theta` autour de l'axe X peut s'effectuer avec cette matrice :

.. math::
   :label: Rotation autour de l'axe X

   \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & \cos(\theta) & \sin(\theta) & 0\\
   0 & -\sin(\theta) & \cos(\theta) & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Axe Y
^^^^^

Une rotation d'un angle :math:`\phi` autour de l'axe Y peut s'effectuer avec cette matrice :

.. math::
   :label: Rotation autour de l'axe Y

   \begin{bmatrix}
   \cos(\phi) & 0 & -\sin(\phi) & 0\\
   0 & 1 & 0 & 0\\
   \sin(\phi) & 0 & \cos(\phi) & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Axe Z
^^^^^

Une rotation d'un angle :math:`\psi` autour de l'axe Z peut s'effectuer avec cette matrice :

.. math::
   :label: Rotation autour de l'axe Z

   \begin{bmatrix}
   \cos(\psi) & \sin(\psi) & 0 & 0\\
   -\sin(\psi) & \cos(\psi) &  & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Rotation autour d'un axe arbitraire
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. avec de l'aide de ce site : http://paulbourke.net/geometry/rotate/

Avec un axe arbitraire dans l'espace passant par le point :math:`P_{0} = [x_{0}, y_{0}, z_{0}]` et avec les cosinus directeurs :math:`(c_{x}, c_{y}, c_{z})`.

.. note:: Les cosinus directeurs sont une liste d'angles qui vont de leur axe respectifs a un vecteur ou un point.

   - :math:`c_{x}` : angle +x et le point/vecteur.
   - :math:`c_{y}` : angle +y et le point/vecteur.
   - :math:`c_{z}` : angle +z et le point/vecteur.

Afin d'effectuer une rotation d'un angle :math:`\delta` autour de cet axe, il faut suivre ces étapes :

#. Effectuer une translation de l'objet vers l'origine
#. Faire les rotations appropriées afin que l'axe de rotation coïncide avec l'axe Z [#pasquez]_
#. Faire une rotation sur l'axe Z par l'angle :math:`\delta`
#. Effectuer l'inverse des rotations combinées
#. Effectuer l'inverse de la translation

La transformation complète est donc :

.. math::
   :label: Transformation complète

   [M] = [T][R_{x}][R_{y}][R_{\delta}][R_{y}]^{-1}[R_{x}]^{-1}[T]^{-1}

.. note::
   Si l'axe de rotation est déjà aligné avec l'axe Z, les étapes 1 et 2 ne sont pas nécessaires.

.. [#pasquez] Le choix de l'axe Z est arbitraire.

Étape 1
"""""""

Il faut effectuer une translation de manière à ce que l'axe de rotation passe à travers l'origine.
Ceci est fait en faisant une translation par :math:`-P_{0} [-x_{0}, -y_{0}, -z_{0}]`. La matrice de translation et son inverse sont :

.. math:: 
   :label: Matrice de translation vers l'origine

   [T] = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   -x_{0} & -y_{0} & -z_{0} & 1
   \end{bmatrix}

.. math::
   :label: Inverse de la matrice de translation vers l'origine

   [T]^{-1} = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   x_{0} & y_{0} & z_{0} & 1
   \end{bmatrix}

Étape 2
"""""""

Il faut effectuer une rotation d'angle :math:`\alpha` autour de l'axe X afin que l'axe de rotation se trouve dans le plan XZ.
Afin de déterminer cet angle de rotation, il faut premièrement projeter le vecteur unitaire qui se trouve sur l'axe arbitraire sur le plan YZ.
Les composants x et y du vecteur projeté sont :math:`c_{y}` et :math:`c_{z}`, les cosinus de directions du vecteur unitaire sur le long de l'axe arbitraire.
Il est possible de calculer la distance de la projection sur le plan YZ avec :

.. math::
   :label: Distance de la projection

   d = \sqrt{c_{y}^2 + c_{z}^2}

Ensuite l'angle peut être calculé avec :

.. math::
   :label: Calcul cos alpha

   \cos \alpha = \frac{c_{z}}{d}

.. math::
   :label: Calcul sin alpha

   \sin \alpha = \frac{c_{y}}{d}

Après avoir effectué la rotation autour de l'axe X sur le plan XZ, le composant z du vecteur est d et le composant x est :math:`c_{x}`,
le cosinus directeur de la direction x.
La longueur du vecteur unitaire est évidemment 1.
Il est donc maintenant possible de calculer l'angle de rotation :math:`\beta` autour de l'axe y afin de rendre l'axe arbitraire coïncidant avec l'axe Z est :

.. math::
   :label: Calcul cos beta

   \cos \beta = d

.. math::
   :label: Calcul sin beta

   \sin \beta = c_{x}

La matrice de rotation autour de l'axe X est 

.. math::
   :label: Rotation X

   [R_{x}] = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & \cos \alpha & \sin \alpha & 0\\
   0 & -\sin \alpha & \cos \alpha & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix} = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & c_{z} / d & c_{y} / d & 0\\
   0 & -c_{y} / d & c_{z} / d & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Et autour de l'axe Y

.. math::
   :label: Rotation Y

   [R_{y}] = \begin{bmatrix}
   \cos(-\beta) & 0 & -\sin(-\beta) & 0\\
   0 & 1 & 0 & 0\\
   \sin(-\beta) & 0 & \cos(-\beta) & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix} = \begin{bmatrix}
   d & 0 & c_{x} & 0\\
   0 & 1 & 0 & 0\\
   -c_{x} & 0 & d & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Étape 3
"""""""

Puis finalement, la rotation autour de l'axe arbitraire est donnée par une matrice de rotation sur l'axe Z

.. math::
   :label: Rotation Z

   [R_{\delta}] = \begin{bmatrix}
   \cos(\delta) & \sin(\delta) & 0 & 0\\
   -\sin(\delta) & \cos(\delta) &  & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Étapes 4 et 5
"""""""""""""

Il faut maintenant calculer l'inverse des matrices de translation et de rotation.

Sans cosinus directeurs
"""""""""""""""""""""""

Si on ne connait pas les cosinus directeurs de l'axe arbitraire, on peut les obtenir à l'aide d'un autre point sur l'axe :math:`(x_{1}, y_{1}, z_{1})` en normalisant le vecteur du premier au second point.
Le vecteur sur l'axe allant de :math:`(x_{0}, y_{0}, z_{0})` à :math:`(x_{1}, y_{1}, z_{1})` est

.. math::
   :label: Calculer vecteur

   [V] = \begin{bmatrix} (x_{1} - x_{0}) & (y_{1} - y_{0}) & (z_{1} - z_{0}) \end{bmatrix}

Normalisé, ça donne les cosinus directeurs

.. math::
   :label: Calcul des cosinus directeurs

   \begin{bmatrix} c_{x} & c_{y} & c_{z} \end{bmatrix} = 
   \frac{\begin{bmatrix}(x_{1} - x_{0}) & (y_{1} - y_{0}) & (z_{1} - z_{0})\end{bmatrix}}
      {[(x_{1} - x_{0})^2 + (y_{1} - y_{0})^2 + (z_{1} - z_{0})^2]^\frac{1}{2}}

Homothétie
----------

Une mise à l'échelle globale est obtenue en utilisant le quatrième élément diagonal :

.. math::
   :label: Scale

   \begin{bmatrix}X\end{bmatrix} \begin{bmatrix}T\end{bmatrix} = \begin{bmatrix}
   x & y & z & 1
   \end{bmatrix} \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & s
   \end{bmatrix} = \begin{bmatrix}
   x' & y' & z' & s
   \end{bmatrix}

Les coordonnées physiques peuvent être obtenues comme ceci :

.. math::
   :label: Scale physique

   \begin{bmatrix}
   x^* & y^* & z^* & 1
   \end{bmatrix} = \begin{bmatrix}
   \frac{x'}{s} & \frac{y'}{s} & \frac{z'}{s} & 1
   \end{bmatrix}

Translation
-----------

La matrice de translation est :

.. math::
   :label: Matrice de translation

   [T] = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   l & m & n & s
   \end{bmatrix}

Les coordonnées homogènes translatées sont obtenues par :

.. math::

   \begin{bmatrix} x' & y' & z' & w \end{bmatrix} = \begin{bmatrix} x & y & z & 1 \end{bmatrix} \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   l & m & n & s
   \end{bmatrix} = \begin{bmatrix} (x + l) & (y + m) & (z + n) & 1 \end{bmatrix}

Ce qui veut dire que les coordonnées physiques transformées sont :

.. math:: 

   x^* = x + l\\
   y^* = y + m\\
   z^* = z + n

Plusieurs transformations
-------------------------

Plusieurs transformations peuvent être combinées dans une seule matrice :math:`4 \times 4` qui donne le même résultat.
Cela dit, la multiplication de matrices n'est pas commutative, il faut faire attention à l'ordre d'application.
L'ordre est déterminé pas la position de la matrice de rotation par rapport au vecteur-position.
La matrice la plus proche du vecteur générera la première transformation individuelle et la plus éloignée fera la dernière.
Mathématiquement, ceci est exprimé comme :

.. math::

   [X][T] = [X][T_{1}][T_{2}][T_{3}][T_{4}] \cdots

Comme les transformations perspectives distordent les objets géométriques et les transformations projectives font perdre des informations,
si ces matrices sont incluses, elles doivent être mises à la fin.


Projections
===========

Les projections permettent de passer des points dans un espace 3D à un espace 2D (l'écran dans notre cas).

Orthographique
--------------
Une projection orthographique est une projection qui garde la vraie taille de l'objet.
Les objets qui se trouvent loin ne sont donc pas plus petits.
Cette projection a pour avantage d'être simple à implémenter, car elle équivaut au fait de juste garder deux des trois coordonnées de chaque point.
Il est donc possible d'effectuer cette projection sur 3 axes : 

- X = 0, on garde Y et Z
- Y = 0, on garde X et Z
- Z = 0, on garde X et Y

.. figure:: ./img/orthographic-projection.jpg
   :alt: Exemple de projection orthographique.
   :scale: 60%
   :align: center

   Exemple de projection orthographique.

Les matrices pour ces projections sont :

.. math::

   [P_{x}] = \begin{bmatrix}
   0 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

.. math::

   [P_{y}] = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 0 & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

.. math::

   [P_{z}] = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 0 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Perspective
-----------
Une projection ou transformation perspective est une projection ou des objets tridimensionnels apparaissent plus petits quand ils sont éloignés de la caméra.

Les projections perspectives ne suivent pas les règles de la géométrie affine (classique).
Par exemple, deux lignes parallèles ne le seront plus et la longueur d'un segment sera réduite en fonction de sa distance de l'observateur.

Les trois premiers éléments de la dernière ligne d'une matrice 4x4 correspondent à la perspective.
C'est pour ça qu'on utilise des coordonnées homogènes.

Si la direction de la vision est alignée avec un axe principal, la création de la matrice perspective est simple.
Afin de pouvoir y effectuer sur n'importe quel axe, on pourra utiliser un changement de repère afin d'aligner la vision à un axe principal.

Perspective selon un axe principal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Une manière d'imaginer la perspective est d'avoir un centre de projection (observateur) qui est face à un plan de projection ou les objets se trouveront.
Le repère se trouve sur le plan de projection avec Y en haut, X à droite et Z en positif en direction du centre de projection.
La distance entre le plan et le centre de projection se note :math:`L`.

.. figure:: ./img/planProjOrigine.jpg
   :alt: Schema de la projection perspective tiré du livre Infographie II de Philipe Schweizer à la page 573.
   :scale: 30%
   :align: center

   Schema de la projection perspective tiré du livre *Infographie II* de *Philipe Schweizer* à la page 573.

Sachant que :

  * Centre de projection = :math:`\begin{bmatrix} 0 & 0 & L & 1 \end{bmatrix}`
  * Direction de vision = :math:`\begin{bmatrix} 0 & 0 & -1 & 0 \end{bmatrix}`
  * Plan de projection : :math:`Z = 0`
  * Point : :math:`\begin{bmatrix} X & Y & Z & 1 \end{bmatrix}`
  * Projection : :math:`\begin{bmatrix} X' & Y' & 0 & 1 \end{bmatrix}`

On peut donc calculer les coordonnées projetées avec :

.. math::

   X' = X \cdot \frac{-L}{Z - L}

.. math::

   Y' = Y \cdot \frac{-L}{Z - L}

.. math::

   Z' = 0

En divisant le numérateur et dénominateur par :math:`-L`, les équations de perspective peuvent aussi s'écrire comme suit :

.. math::

   X' = X \cdot \frac{1}{Z / -L + 1}

.. math::

   Y' = Y \cdot \frac{1}{Z / -L + 1}

On peut écrire ces équations matriciellement comme :

.. math::

   P' = M_{TransfPersp.z} \cdot P

.. math::

   M_{TransfPersp.z} = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & r & 1
   \end{bmatrix}

.. math::

   r = \frac{1}{-L}

Cette matrice correspond à une transformation perspective et non une projection perspective.
Afin d'obtenir une projection, il faut la multiplier par la matrice de projection orthographique Z.

.. math::

   M_{ProjPersp.z} = M_{Ortho.z} \cdot M_{TransfPersp.z} = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 0 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix} \cdot \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & r & 1
   \end{bmatrix}

.. math::

   M_{ProjPersp.z} = \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 0 & 0\\
   0 & 0 & r & 1
   \end{bmatrix}

On peut ensuite projeter un point en le multipliant par cette matrice :

.. math::

   P' = M_{ProjPersp.z} \cdot P

.. math::

   \begin{bmatrix}
   X' & Y' & Z' & W'
   \end{bmatrix} = \begin{bmatrix}
   X & Y & Z & W
   \end{bmatrix} \cdot \begin{bmatrix}
   1 & 0 & 0 & 0\\
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & -1/L & 1
   \end{bmatrix}

.. math::

   \begin{bmatrix}
   X' & Y' & Z' & W'
   \end{bmatrix} = \begin{bmatrix}
   X & Y & Z & -1 / L \cdot Z + W
   \end{bmatrix}

Projection perspective générale
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour que la perspective soit utile, il faut pouvoir créer une matrice qui marche dans un cas quelconque.
Il est possible de faire ça en faisant un changement de repère puis d'appliquer la projection perspective sur l'axe Z.
Le repère est déterminé par la position de l'observateur O et un point visé V.

Le changement de repère peut aussi se dire qu'on passe du world space au view space.
Ce passage se fait par une succession de transformations élémentaires, les mêmes que pour la rotation autour d'un axe quelconque.

Les étapes pour ce changement sont :

#. :math:`T^{-1}` Translation à l'observateur
#. :math:`R_{Z}^{-1}` Rotation d'axe Z
#. :math:`R_{Y}^{-1}` Rotation d'axe Y
#. :math:`E^{-1}` Échange des coordonnées (dans le cas ou axes ne se trouvent pas au même endroit entre le world space et le view space)

La première étape est de translater le repère actuel à l'observateur, la matrice pour cette transformation est :

.. math::

   T^{-1} = \begin{bmatrix}
   1 & 0 & 0 & -T_{x}\\
   0 & 1 & 0 & -T_{y}\\
   0 & 0 & 1 & -T_{z}\\
   0 & 0 & 0 & 1
   \end{bmatrix}

La rotation Z d'angle :math:`\varphi` est :

.. math::

   R_{Z}^{-1} = \begin{bmatrix}
   \cos \varphi & \sin \varphi & 0 & 0\\
   -\sin \varphi & \cos \varphi & 0 & 0\\
   0 & 0 & 1 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Finalement la matrice de rotation autour de l'axe Y d'un angle :math:`\theta` est :

.. math::

   R_{Y}^{-1} = \begin{bmatrix}
   \cos \theta & 0 & \sin \theta & 0\\
   0 & 1 & 0 & 0\\
   -\sin \theta & \cos \theta & 0 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Si maintenant on veut échanger les axes (ce que je n'ai pas fait dans mon code), on peut le faire avec une matrice de permutation.
Par exemple, pour une permutation ou

* X = Y
* Y = Z
* Z = X

La matrice pour effectuer ce changement est : 

.. math::

   E^{-1} = \begin{bmatrix}
   0 & 1 & 0 & 0\\
   0 & 0 & 1 & 0\\
   1 & 0 & 0 & 0\\
   0 & 0 & 0 & 1
   \end{bmatrix}

Une fois que nous sommes dans le repaire de l'observateur, on peut effectuer la perspective à l'aide de la matrice de transformation perspective.

Les sinus et cosinus des angles :math:`\varphi` et :math:`\theta` grâce au vecteur direction de vision :math:`\vec{D}`.
Ce vecteur peut être calculé avec :math:`\vec{D} = \vec{O} - \vec{E}`.
Il faut également calculer la projection de celui-ci (:math:`\vec{D}_{proj}`).
Les sinus et cosinus recherchés sont :

.. math::

   \cos \varphi = D_{x} / |D_{proj}|

.. math::

   \sin \varphi = D_{y} / |D_{proj}|

.. math::

   \cos \theta = |D_{proj}| / |D|

.. math::

   \sin \theta = D_{z} / |D|

Dans mon code pour faire ce système, j'ai créé deux méthodes statiques dans mon struct ``Matrix4`` : ``LookAt`` et ``CreatePerspectiveFieldOfView``.

La méthode ``Matrix4 LookAt(Vector3 eye, Vector3 target, Vector3 up)`` s'occupe de créer la matrice de changement de repère globale.
Puis, la méthode ``Matrix4 CreatePerspectiveFieldOfView(double fovy, double aspect, double depthNear, double depthFar)``, va créer la matrice de projection perspective.

Caméra virtuelle
----------------
Afin de pouvoir projeter de n'importe où, il faut intégrer un système de caméra virtuelle.
Le principe de base est de transformer les objets du world space au view space (donc face à la caméra), puis d'y projeter avec une matrice de projection.

Pour faire ceci, dans mon code j'ai créé une classe ``Camera``.
Elle possède une position qui permet de savoir où elle se trouve dans l'espace.
Elle a également deux variables (propriété) d'angles ``Pitch`` et ``Yaw`` qui permettent de savoir où elle regarde dans l'espace.
Finalement, elle possède trois vecteurs : ``Front``, ``Up`` et ``Right`` qui déterminent les axes du view space.
Quand les propriétés ``Pitch`` ou ``Yaw`` sont mises à jour, une méthode nommée ``UpdateVectors()`` est appelée.
Celle-ci s'occupe de mettre à jour les axes du view space grâce à de la trigonométrie.

Une fois que l'on veut projeter à l'aide de la caméra, il faut appeler les propriétés ``ViewMatrix`` et ``ProjectionMatrix``
qui respectivement appellent les méthodes ``Matrix4.LookAt`` et ``Matrix4.CreatePerspectiveFieldOfView``.

La direction de vision de la caméra est déterminée par le vecteur ``Front``.