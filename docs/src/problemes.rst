**********************
Problèmes rencontrés
**********************

Projection sur l'axe Z
======================
Lors du POC avant le début du travail de diplôme, j'avais déjà réussi à afficher un modèle avec du backface culling, mais pas lorsque je projetais sur l'axe Z.

.. figure:: ./img/logbook/goodBackface.png
   :alt: Backface culling qui ne marche pas sur l'axe Z.
   :align: center
   :scale: 70%

   Backface culling qui ne marche pas sur l'axe Z.

Malheureusement par la suite (le 21/04/2021), je me suis concentré à abstraire l'affichage dans une autre classe, ce qui fait que je projetais uniquement sur l'axe X.
Ce n'est que quand M. Bonvin m'a demandé de projeter avec l'axe Y en haut, Z en face et X à droite que j'ai vu que ce problème était toujours là (le 04/05/2021).

.. figure:: ./img/logbook/badZ.png
   :alt: Projection qui ne marche pas sur l'axe Z avec le backface culling.
   :align: center
   :scale: 70%

   Projection qui ne marche pas sur l'axe Z avec le backface culling.

Je me suis ensuite dit que cette façon de projeter que j'ai, n'est que temporaire et qu'il fallait que je passe à la suite.
Je me suis donc mis à la création d'une classe ``Camera`` qui permet de faire une projection perspective depuis n'importe où dans la scène.
Malgré cette classe, j'ai toujours le problème de projection.

.. figure:: ./img/badColors.png
   :alt: Projection qui marche mal.
   :align: center
   :scale: 70%

   Projection qui marche mal.

J'ai ensuite réussi à régler ce problème.

.. figure:: ./img/logbook/niceProjection.png
   :alt: Projection qui marche bien.
   :align: center
   :scale: 70%

   Projection qui marche bien.

Mon problème c'est que je faisais les calculs de normales sur les faces projetées, ce qui fait que j'avais n'importe quoi lors de l'éclairage ou du backface culling.

Tableaux à deux dimensions sur Doxygen
=======================================

En C#, il y a une syntaxe particulière pour créer des tableaux à deux dimensions :

.. code::C#

   double[,] doubles;

Sauf que quand je générais ma documentation Doxygen avec des tableaux comme ça dans mon code, j'avais ce résultat :

.. figure:: ./img/logbook/badDoxygen.png
   :alt: Tableaux à deux dimensions qui ne marchent pas dans Doxygen.
   :align: center
   :scale: 70%

   Tableaux à deux dimensions qui ne marchent pas dans Doxygen.

Ne trouvant pas la réponse à mon problème, j'ai posé une question sur `StackOverflow <https://stackoverflow.com/questions/67197013/doxygen-doesnt-work-with-2d-arrays-in-c-sharp/67197320#67197320>`_.
Il fallait mettre à jour Doxygen vers la version ``1.9.1``, car la version ``1.8.17`` ne marchait pas avec cette syntaxe.

Affichage des normales des faces
================================

Déjà durant le POC j'avais essayé de dessiner les normales, malheureusement je n'avais pas réussi, car mes normales faisaient n'importe quoi.
J'ai donc re-essayé durant le TD et j'ai eu le même problème :

.. figure:: ./img/logbook/changeSizeNormal.png
   :alt: Normales qui ne s'affichent pas correctement.
   :align: center
   :scale: 70%

   Normales qui ne s'affichent pas correctement.

Plus tard, en lisant le `wiki de OpenGL <https://www.khronos.org/opengl/wiki/Calculating_a_Surface_Normal>`_, je me suis rendu compte que je calculais les normales avec le mauvais ordre :

.. code:: c#

   Vector3 physicalTwo = this.vertices[2].ToPhysicalCoords();
   Vector3 v0 = this.vertices[0].ToPhysicalCoords() - physicalTwo;
   Vector3 v1 = this.vertices[1].ToPhysicalCoords() - physicalTwo;
   normal = v0.Cross(v1).NormalizeOrDefault();

Alors que j'aurais du faire : 

.. code:: c#

   Vector3 physicalZero = this.vertices[0].ToPhysicalCoords();
   Vector3 v0 = this.vertices[1].ToPhysicalCoords() - physicalZero;
   Vector3 v1 = this.vertices[2].ToPhysicalCoords() - physicalZero;
   normal = v0.Cross(v1).NormalizeOrDefault();

Après ça les normales se sont bien affichées. Cela dit, je n'ai malheureusement pas pris de screenshot.

Sauf qu'ensuite, je devais essayer de projeter sur l'axe Z et là, ça n'a plus marché.
C'est d'ailleurs surement ce problème qui cause le bug d'affichage.

Voici à quoi ressemblent les normales le 10/05/2021 :

.. figure:: ./img/badNormalsAgain.png
   :alt: Normales qui ne s'affichent (toujours) pas correctement.
   :align: center
   :scale: 70%

   Normales qui ne s'affichent (toujours) pas correctement.

Et parfois elles vont même se coller à la face opposée

.. figure:: ./img/stickyNormals.png
   :alt: Normales qui vont se coller à la face opposée.
   :align: center
   :scale: 70%

   Normales qui vont se coller à la face opposée.

Au final, j'ai réglé ce problème.

.. figure:: ./img/logbook/goodNormalsNiceGoodNotBad.png
   :alt: Normales bien désinnée.
   :align: center
   :scale: 70%

   Normales bien déssinée.

Le problème venait du fait que je dessinais les normales depuis la face projetée et non depuis la face transformée (donc avant la projection).