# 3D Viewer

(English below)

3D Viewer est un moteur 3D codé en C# qui permet d'afficher des modèles et d'organiser des objets dans une scène.

Documentation disponible sur Read the Docs : https://3dviewer.readthedocs.io

## Ce que j'ai appris

- Rasterisation
- Algèbre linéaire
- Création de stuctures mathématiques (par ex: Vector3)

## English

3D viewer is a 3D engine (rasteriser) made in C# (windows form, no graphics library) that allows to show models and organize them in a scene graph.

The online documentation is available on Read the Docs (FR only) : https://3dviewer.readthedocs.io

## What did I learn ?

- Rasterisation
- Linear algebra
- Creation of mathematical structures (e.g. `Vector3`)
