﻿/**
* @file EntityCollection.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the EntityCollection class.
* @version 1.0
* @date 21.05.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace ThreeDViewer
{
    using System.Collections;
    using System.Collections.Generic;
    using ThreeDViewer.Mathematics.Geometry;

    /// <summary>
    /// Class containing a list of entities and a reference to its parents.
    /// </summary>
    public class EntityCollection
        : IEnumerable<Entity>
    {
        private readonly Entity parent;
        private readonly List<Entity> entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityCollection"/> class.
        /// </summary>
        /// <param name="parent">Parent of this collection.</param>
        public EntityCollection(Entity parent)
        {
            this.parent = parent;
            entities = new();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityCollection"/> class.
        /// </summary>
        /// <param name="parent">Parent of this collection.</param>
        /// <param name="entities">Entities stored in this collection.</param>
        public EntityCollection(Entity parent, List<Entity> entities)
        {
            this.parent = parent;
            this.entities = new(entities);
        }

        /// <summary>
        /// Gets the number of entities contained in the collection.
        /// </summary>
        public int Count => entities.Count;

        /// <summary>
        /// Gets all the absolute faces in the collection (this includes the children of the entities).
        /// </summary>
        public List<Face> AbsoluteFaces
        {
            get
            {
                List<Face> faces = new();

                foreach (Entity entity in entities)
                {
                    faces.AddRange(entity.AbsoluteModel.Faces);
                    faces.AddRange(entity.Children.AbsoluteFaces);
                }

                return faces;
            }
        }

        /// <summary>
        /// Gets the entity in the specified index.
        /// </summary>
        /// <param name="index">Index of the entity.</param>
        /// <returns>The specified entity.</returns>
        public Entity this[int index] => entities[index];

        /// <summary>
        /// Adds an entity to the list.
        /// Checks if the entity is in the list first and updates it's parent when added.
        /// </summary>
        /// <param name="entity">Entity to add in the list.</param>
        public void Add(Entity entity)
        {
            if (!entities.Contains(entity))
            {
                entities.Add(entity);
                entity.Parent = parent;
            }
        }

        /// <summary>
        /// Removes an entity in the list.
        /// Sets the parent of the entity as null.
        /// </summary>
        /// <param name="entity">Entity to remove from the list.</param>
        public void Remove(Entity entity)
        {
            if (entities.Contains(entity))
            {
                entities.Remove(entity);
                entity.Parent = null;
            }
        }

        /// <summary>
        /// Removes all the elements from the collection.
        /// </summary>
        public void Clear() => entities.Clear();

        /// <inheritdoc/>
        public IEnumerator<Entity> GetEnumerator() => entities.GetEnumerator();

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
