﻿namespace ThreeDViewer
{
    /// <summary>
    /// Defines the way that backface will be culled.
    /// </summary>
    public enum CullingMode
    {
        /// <summary>
        /// No backface culling.
        /// </summary>
        None = 0,

        /// <summary>
        /// Cull with the normals of the face.
        /// </summary>
        Normals = 1,

        /// <summary>
        /// Cull by checking if the faces are in clockwise order or not once projected.
        /// </summary>
        Clockwise = 2,
    }
}
