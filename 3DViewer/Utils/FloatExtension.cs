﻿/**
 * @file FloatExtension.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the FloatExtension class.
 * @version 1.0
 * @date 21.04.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Utils
{
    /// <summary>
    /// Class that contains all the extensions for the <see cref="float"/> class.
    /// </summary>
    public static class FloatExtension
    {
        /// <summary>
        /// Linearly maps the value s that's in the range [a1, a2] to the range [b1, b2].
        /// </summary>
        /// <param name="s">The value to map.</param>
        /// <param name="a1">The first limit of the range that s belongs to.</param>
        /// <param name="a2">The second limit of the range that s belongs to.</param>
        /// <param name="b1">The first limit of the ouput range.</param>
        /// <param name="b2">The second limit of the ouput range.</param>
        /// <returns>The mapped value in the [b1, b2] range.</returns>
        public static float Map(this float s, float a1, float a2, float b1, float b2) => b1 + ((s - a1) * (b2 - b1) / (a2 - a1));
    }
}
