﻿/**
 * @file PointFExtension.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PointFExtension class.
 * @version 1.0
 * @date 11.05.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Utils
{
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// Class that contains all the extensions for the <see cref="PointF"/> class.
    /// </summary>
    public static class PointFExtension
    {
        /// <summary>
        /// Maps the point from it's mathematical size to pixels on the screen.
        /// </summary>
        /// <param name="point">Point to map.</param>
        /// <param name="mathematicalSize">Size of the mathematical space that will be projected.</param>
        /// <param name="imageSize">Size of the image that the point will be projected on.</param>
        /// <returns>The mapped point.</returns>
        public static PointF MapToPixel(this PointF point, Size mathematicalSize, Size imageSize)
        {
            float mathMaxX = mathematicalSize.Width / 2;
            float mathMinX = -mathMaxX;

            float mathMaxY = mathematicalSize.Height / 2;
            float mathMinY = -mathMaxY;

            float mappedPointX = point.X.Map(mathMinX, mathMaxX, 0, imageSize.Width);
            float mappedPointY = point.Y.Map(mathMinY, mathMaxY, imageSize.Height, 0);
            return new(mappedPointX, mappedPointY);
        }

        /// <summary>
        /// Maps the list of points from their mathematical size to pixels on the screen.
        /// </summary>
        /// <param name="points">Points to map.</param>
        /// <param name="mathematicalSize">Size of the mathematical space that will be projected.</param>
        /// <param name="imageSize">Size of the image that the point will be projected on.</param>
        /// <returns>The mapped points.</returns>
        public static IList<PointF> MapToPixel(this IList<PointF> points, Size mathematicalSize, Size imageSize)
        {
            for (int i = 0; i < points.Count; i++)
            {
                points[i] = points[i].MapToPixel(mathematicalSize, imageSize);
            }

            return points;
        }
    }
}
