﻿/**
 * @file MathHelper.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the MathHelper class.
 * @version 1.0
 * @date 11.05.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Utils
{
    using System;

    /// <summary>
    /// Class that contains methods to help with mathematical things.
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// PI divided by two.
        /// </summary>
        public static readonly double PiOver2 = Math.PI / 2;

        /// <summary>
        /// Converts radians to degree.
        /// </summary>
        /// <param name="radians">Radians to convert.</param>
        /// <returns>The angle in degree.</returns>
        public static double RadToDeg(double radians) => (180 / Math.PI) * radians;

        /// <summary>
        /// Converts degrees to radians.
        /// </summary>
        /// <param name="degrees">Degress to convert.</param>
        /// <returns>The angle in radians.</returns>
        public static double DegToRad(double degrees) => (Math.PI / 180) * degrees;
    }
}
