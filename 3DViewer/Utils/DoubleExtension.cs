﻿/**
 * @file DoubleExtension.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the DoubleExtension class.
 * @version 1.1
 * @date 21.04.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Utils
{
    using System;

    /// <summary>
    /// Class that contains all the extensions for the <see cref="double"/> class.
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>
        /// Comparator within an absolute tolerance.
        /// </summary>
        /// <param name="a">The double to compare to.</param>
        /// <param name="b">The double to compare with.</param>
        /// <param name="maxAbsoluteError">The tolerance for the comparison compared against the difference of the two doubles.</param>
        /// <returns>True if the doubles are equal within a tolerance.</returns>
        public static bool AlmostEqualsWithAbsTolerance(this double a, double b, double maxAbsoluteError)
        {
            double diff = Math.Abs(a - b);

            // Shortcut, handles infinities
            return a.Equals(b) || diff <= maxAbsoluteError;
        }

        /// <summary>
        /// Linearly maps the value s that's in the range [a1, a2] to the range [b1, b2].
        /// </summary>
        /// <param name="s">The value to map.</param>
        /// <param name="a1">The first limit of the range that s belongs to.</param>
        /// <param name="a2">The second limit of the range that s belongs to.</param>
        /// <param name="b1">The first limit of the ouput range.</param>
        /// <param name="b2">The second limit of the ouput range.</param>
        /// <returns>The mapped value in the [b1, b2] range.</returns>
        public static double Map(this double s, double a1, double a2, double b1, double b2) => b1 + ((s - a1) * (b2 - b1) / (a2 - a1));
    }
}
