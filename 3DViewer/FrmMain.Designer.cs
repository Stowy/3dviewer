﻿
namespace ThreeDViewer
{
	partial class FrmMain
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            fpsTimer = new System.Windows.Forms.Timer(components);
            button1 = new System.Windows.Forms.Button();
            chbFill = new System.Windows.Forms.CheckBox();
            chbTimer = new System.Windows.Forms.CheckBox();
            trbScale = new System.Windows.Forms.TrackBar();
            toolStrip1 = new System.Windows.Forms.ToolStrip();
            saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            chbWire = new System.Windows.Forms.CheckBox();
            btnColorDialog = new System.Windows.Forms.Button();
            pbxRender = new System.Windows.Forms.PictureBox();
            renderTimer = new System.Windows.Forms.Timer(components);
            chbShowNormals = new System.Windows.Forms.CheckBox();
            cbxCullingMode = new System.Windows.Forms.ComboBox();
            cbxProjectionMode = new System.Windows.Forms.ComboBox();
            rdbModelDemo = new System.Windows.Forms.RadioButton();
            rdbBackhoeDemo = new System.Windows.Forms.RadioButton();
            radioButton2 = new System.Windows.Forms.RadioButton();
            btnLoadModel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)trbScale).BeginInit();
            toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pbxRender).BeginInit();
            SuspendLayout();
            // 
            // fpsTimer
            // 
            fpsTimer.Enabled = true;
            fpsTimer.Interval = 16;
            fpsTimer.Tick += Timer1_Tick;
            // 
            // button1
            // 
            button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            button1.Location = new System.Drawing.Point(12, 726);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(84, 23);
            button1.TabIndex = 1;
            button1.Text = "Next Frame";
            button1.UseVisualStyleBackColor = true;
            button1.Click += Button1_Click;
            // 
            // chbFill
            // 
            chbFill.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            chbFill.AutoSize = true;
            chbFill.Location = new System.Drawing.Point(105, 729);
            chbFill.Name = "chbFill";
            chbFill.Size = new System.Drawing.Size(39, 19);
            chbFill.TabIndex = 2;
            chbFill.Text = "fill";
            chbFill.UseVisualStyleBackColor = true;
            chbFill.CheckedChanged += ChbFill_CheckedChanged;
            // 
            // chbTimer
            // 
            chbTimer.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            chbTimer.AutoSize = true;
            chbTimer.Checked = true;
            chbTimer.CheckState = System.Windows.Forms.CheckState.Checked;
            chbTimer.Location = new System.Drawing.Point(204, 730);
            chbTimer.Name = "chbTimer";
            chbTimer.Size = new System.Drawing.Size(54, 19);
            chbTimer.TabIndex = 3;
            chbTimer.Text = "timer";
            chbTimer.UseVisualStyleBackColor = true;
            chbTimer.CheckedChanged += ChbTimer_CheckedChanged;
            // 
            // trbScale
            // 
            trbScale.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            trbScale.Location = new System.Drawing.Point(12, 754);
            trbScale.Maximum = 200;
            trbScale.Minimum = 1;
            trbScale.Name = "trbScale";
            trbScale.Size = new System.Drawing.Size(500, 45);
            trbScale.TabIndex = 5;
            trbScale.Value = 100;
            trbScale.Scroll += TrbScale_Scroll;
            // 
            // toolStrip1
            // 
            toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { saveToolStripButton });
            toolStrip1.Location = new System.Drawing.Point(0, 0);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            toolStrip1.Size = new System.Drawing.Size(804, 25);
            toolStrip1.TabIndex = 6;
            toolStrip1.Text = "toolStrip1";
            // 
            // saveToolStripButton
            // 
            saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            saveToolStripButton.Image = (System.Drawing.Image)resources.GetObject("saveToolStripButton.Image");
            saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            saveToolStripButton.Name = "saveToolStripButton";
            saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            saveToolStripButton.Tag = "save";
            saveToolStripButton.Text = "&Save";
            saveToolStripButton.Click += SaveToolStripButton_Click;
            // 
            // chbWire
            // 
            chbWire.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            chbWire.AutoSize = true;
            chbWire.Checked = true;
            chbWire.CheckState = System.Windows.Forms.CheckState.Checked;
            chbWire.Location = new System.Drawing.Point(150, 730);
            chbWire.Name = "chbWire";
            chbWire.Size = new System.Drawing.Size(48, 19);
            chbWire.TabIndex = 7;
            chbWire.Text = "wire";
            chbWire.UseVisualStyleBackColor = true;
            chbWire.CheckedChanged += ChbWire_CheckedChanged;
            // 
            // btnColorDialog
            // 
            btnColorDialog.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            btnColorDialog.Location = new System.Drawing.Point(12, 697);
            btnColorDialog.Name = "btnColorDialog";
            btnColorDialog.Size = new System.Drawing.Size(84, 23);
            btnColorDialog.TabIndex = 8;
            btnColorDialog.Text = "Color";
            btnColorDialog.UseVisualStyleBackColor = true;
            btnColorDialog.Click += BtnColorDialog_Click;
            // 
            // pbxRender
            // 
            pbxRender.Location = new System.Drawing.Point(12, 41);
            pbxRender.Name = "pbxRender";
            pbxRender.Size = new System.Drawing.Size(650, 650);
            pbxRender.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            pbxRender.TabIndex = 9;
            pbxRender.TabStop = false;
            // 
            // renderTimer
            // 
            renderTimer.Enabled = true;
            renderTimer.Tick += RenderTimer_Tick;
            // 
            // chbShowNormals
            // 
            chbShowNormals.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            chbShowNormals.AutoSize = true;
            chbShowNormals.Location = new System.Drawing.Point(105, 705);
            chbShowNormals.Name = "chbShowNormals";
            chbShowNormals.Size = new System.Drawing.Size(101, 19);
            chbShowNormals.TabIndex = 10;
            chbShowNormals.Text = "Show normals";
            chbShowNormals.UseVisualStyleBackColor = true;
            chbShowNormals.CheckedChanged += ChbShowNormals_CheckedChanged;
            // 
            // cbxCullingMode
            // 
            cbxCullingMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            cbxCullingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cbxCullingMode.FormattingEnabled = true;
            cbxCullingMode.Items.AddRange(new object[] { "None", "Normals", "Clockwise" });
            cbxCullingMode.Location = new System.Drawing.Point(264, 713);
            cbxCullingMode.Name = "cbxCullingMode";
            cbxCullingMode.Size = new System.Drawing.Size(121, 23);
            cbxCullingMode.TabIndex = 11;
            cbxCullingMode.SelectedIndexChanged += CbxCullingMode_SelectedIndexChanged;
            // 
            // cbxProjectionMode
            // 
            cbxProjectionMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            cbxProjectionMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cbxProjectionMode.FormattingEnabled = true;
            cbxProjectionMode.Items.AddRange(new object[] { "Perspective", "Orthographic" });
            cbxProjectionMode.Location = new System.Drawing.Point(391, 713);
            cbxProjectionMode.Name = "cbxProjectionMode";
            cbxProjectionMode.Size = new System.Drawing.Size(121, 23);
            cbxProjectionMode.TabIndex = 15;
            cbxProjectionMode.SelectedIndexChanged += CbxProjectionMode_SelectedIndexChanged;
            // 
            // rdbModelDemo
            // 
            rdbModelDemo.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            rdbModelDemo.AutoSize = true;
            rdbModelDemo.Checked = true;
            rdbModelDemo.Location = new System.Drawing.Point(675, 41);
            rdbModelDemo.Name = "rdbModelDemo";
            rdbModelDemo.Size = new System.Drawing.Size(94, 19);
            rdbModelDemo.TabIndex = 19;
            rdbModelDemo.TabStop = true;
            rdbModelDemo.Tag = "0";
            rdbModelDemo.Text = "Model Demo";
            rdbModelDemo.UseVisualStyleBackColor = true;
            rdbModelDemo.CheckedChanged += RdbDemo_CheckedChanged;
            // 
            // rdbBackhoeDemo
            // 
            rdbBackhoeDemo.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            rdbBackhoeDemo.AutoSize = true;
            rdbBackhoeDemo.Location = new System.Drawing.Point(675, 66);
            rdbBackhoeDemo.Name = "rdbBackhoeDemo";
            rdbBackhoeDemo.Size = new System.Drawing.Size(105, 19);
            rdbBackhoeDemo.TabIndex = 20;
            rdbBackhoeDemo.Tag = "1";
            rdbBackhoeDemo.Text = "Backhoe Demo";
            rdbBackhoeDemo.UseVisualStyleBackColor = true;
            rdbBackhoeDemo.CheckedChanged += RdbDemo_CheckedChanged;
            // 
            // radioButton2
            // 
            radioButton2.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            radioButton2.AutoSize = true;
            radioButton2.Location = new System.Drawing.Point(675, 91);
            radioButton2.Name = "radioButton2";
            radioButton2.Size = new System.Drawing.Size(89, 19);
            radioButton2.TabIndex = 21;
            radioButton2.Tag = "2";
            radioButton2.Text = "Hand Demo";
            radioButton2.UseVisualStyleBackColor = true;
            radioButton2.CheckedChanged += RdbDemo_CheckedChanged;
            // 
            // btnLoadModel
            // 
            btnLoadModel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnLoadModel.Location = new System.Drawing.Point(675, 116);
            btnLoadModel.Name = "btnLoadModel";
            btnLoadModel.Size = new System.Drawing.Size(105, 23);
            btnLoadModel.TabIndex = 22;
            btnLoadModel.Text = "Load Model";
            btnLoadModel.UseVisualStyleBackColor = true;
            btnLoadModel.Click += BtnLoadModel_Click;
            // 
            // FrmMain
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(804, 811);
            Controls.Add(btnLoadModel);
            Controls.Add(radioButton2);
            Controls.Add(rdbBackhoeDemo);
            Controls.Add(rdbModelDemo);
            Controls.Add(cbxProjectionMode);
            Controls.Add(cbxCullingMode);
            Controls.Add(chbShowNormals);
            Controls.Add(pbxRender);
            Controls.Add(btnColorDialog);
            Controls.Add(chbWire);
            Controls.Add(toolStrip1);
            Controls.Add(trbScale);
            Controls.Add(chbTimer);
            Controls.Add(chbFill);
            Controls.Add(button1);
            Name = "FrmMain";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "3D Viewer";
            Load += FrmMain_Load;
            Paint += FrmMain_Paint;
            KeyDown += FrmMain_KeyDown;
            ((System.ComponentModel.ISupportInitialize)trbScale).EndInit();
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pbxRender).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.Timer fpsTimer;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.CheckBox chbFill;
		private System.Windows.Forms.CheckBox chbTimer;
		private System.Windows.Forms.TrackBar trbScale;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton saveToolStripButton;
		private System.Windows.Forms.CheckBox chbWire;
		private System.Windows.Forms.Button btnColorDialog;
		private System.Windows.Forms.PictureBox pbxRender;
		private System.Windows.Forms.Timer renderTimer;
		private System.Windows.Forms.CheckBox chbShowNormals;
		private System.Windows.Forms.ComboBox cbxCullingMode;
		private System.Windows.Forms.ComboBox cbxProjectionMode;
        private System.Windows.Forms.RadioButton rdbModelDemo;
        private System.Windows.Forms.RadioButton rdbBackhoeDemo;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Button btnLoadModel;
    }
}

