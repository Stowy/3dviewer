﻿/**
 * @file FrmMain.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the FrmMain class.
 * @version 1.0
 * @date 27.04.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Windows.Forms;
    using ThreeDViewer.Mathematics.Geometry;
    using ThreeDViewer.Utils;

    /// <summary>
    /// Main form of the app.
    /// </summary>
    public partial class FrmMain : Form
    {
        private const int Fps = 60;
        private const string DefaultModelFile = "./ply/cube2.ply";

        private readonly Renderer renderer;
        private readonly SaveFileDialog imageSaver;

        private DemoType demoType;

        private Scene demoModelScene;
        private Scene demoBackhoeScene;
        private Scene demoHandScene;

        private bool openArm = false;
        private bool openHandOne = false;
        private bool openHandTwo = false;
        private bool openHandThree = false;
        private double falcoAngle = 0d;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmMain"/> class.
        /// </summary>
        public FrmMain()
        {
            InitializeComponent();

            CreateScenes();

            renderer = new(new(3, 3), pbxRender.Size);
            renderer.ShowWireframe = chbWire.Checked;
            renderer.ShowFill = chbFill.Checked;
            renderer.CullingMode = CullingMode.None;
            renderer.ShowNormals = chbShowNormals.Checked;
            renderer.LightColor = Color.White;
            renderer.Scene = demoModelScene;

            DoubleBuffered = true;
            renderTimer.Interval = 1000 / Fps;
            renderTimer.Enabled = chbTimer.Checked;
            fpsTimer.Interval = 1000 / Fps;
            fpsTimer.Enabled = chbTimer.Checked;

            KeyPreview = true;

            cbxCullingMode.SelectedIndex = 0;
            cbxProjectionMode.SelectedIndex = 0;
            demoType = DemoType.Model;

            imageSaver = new SaveFileDialog
            {
                Filter = "PNG Image|*.png|Bitmap Image|*.bmp",
            };
        }

        private enum DemoType
        {
            Model = 0,
            Backhoe = 1,
            Hand = 2,
        }

        private void CreateScenes()
        {
            Model cube = Model.FromPly(DefaultModelFile);

            double aspect = pbxRender.Height / (double)pbxRender.Width;
            Camera camera = new(new(0, 0, 6), aspect);

            // Create model scene
            demoModelScene = new Scene(camera);
            Entity entity = new(cube);
            demoModelScene.Entities.Add(entity);

            // Create backhoe scene
            demoBackhoeScene = new(camera);
            Model armModel = Model.FromPly(@"./ply/backhoe/arm.ply");

            Entity wheels = new(Model.FromPly(@"./ply/backhoe/wheels.ply"));
            wheels.Scale = 1 / 0.5d;
            Entity body = new(Model.FromPly(@"./ply/backhoe/body.ply"), wheels);
            body.Position = new(0, 2.3d, 0);
            Entity armOne = new(armModel, body);
            armOne.Position = new(3, 1, 0);
            armOne.Rotation = new(0, 0, MathHelper.DegToRad(45));
            Entity armTwo = new(armModel, armOne);
            armTwo.Position = new(6, 0, 0);
            armTwo.Rotation = new(0, 0, MathHelper.DegToRad(-90));

            demoBackhoeScene.Entities.Add(wheels);

            // Create hand scene
            demoHandScene = new(camera);

            Model proximamalPhalanxModel = Model.CreateRectangularCuboidOffCenter(0.8, 1.7, 0.9);
            Model middlePhalanxModel = Model.CreateRectangularCuboidOffCenter(0.8, 1.5, 0.8);

            Entity palm = new(Model.CreateRectangularCuboidCentered(1, 3, 3));
            palm.Rotation = new(0, 0, MathHelper.DegToRad(-20));
            palm.Position = new(2, 0, 0);

            Entity proximamalPhalanxOne = new(proximamalPhalanxModel, palm);
            proximamalPhalanxOne.Position = new(-1.5, -0.5, 1.5);
            proximamalPhalanxOne.Rotation = new(MathHelper.DegToRad(-5), MathHelper.DegToRad(185), 0);

            Entity proximamalPhalanxTwo = new(proximamalPhalanxModel, palm);
            proximamalPhalanxTwo.Position = new(-1.5, -0.5, 0.5);
            proximamalPhalanxTwo.Rotation = new(0, MathHelper.DegToRad(180), 0);

            Entity proximamalPhalanxThree = new(proximamalPhalanxModel, palm);
            proximamalPhalanxThree.Position = new(-1.5, -0.5, -0.5);
            proximamalPhalanxThree.Rotation = new(MathHelper.DegToRad(5), MathHelper.DegToRad(175), 0);

            Entity middlePhalanxOne = new(middlePhalanxModel, proximamalPhalanxOne);
            middlePhalanxOne.Position = new(1.7, 0, 0.05);

            Entity middlePhalanxTwo = new(middlePhalanxModel, proximamalPhalanxTwo);
            middlePhalanxTwo.Position = new(1.7, 0, 0.05);

            Entity middlePhalanxThree = new(middlePhalanxModel, proximamalPhalanxThree);
            middlePhalanxThree.Position = new(1.7, 0, 0.05);

            Entity falco = new(Model.FromPly(@"./ply/falco.ply"));
            falco.Position = new(0, 0, 3);

            demoHandScene.Entities.Add(palm);
            demoHandScene.Entities.Add(falco);
        }

        private void Timer1_Tick(object sender, EventArgs e) => Refresh();

        private void ChbFill_CheckedChanged(object sender, EventArgs e) => renderer.ShowFill = chbFill.Checked;

        private void ChbTimer_CheckedChanged(object sender, EventArgs e)
        {
            fpsTimer.Enabled = chbTimer.Checked;
            renderTimer.Enabled = chbTimer.Checked;
        }

        private void ChbWire_CheckedChanged(object sender, EventArgs e) => renderer.ShowWireframe = chbWire.Checked;

        private void BtnColorDialog_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new();
            colorDialog.AllowFullOpen = true;
            colorDialog.ShowHelp = true;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                renderer.LightColor = colorDialog.Color;
            }
        }

        private void FrmMain_Paint(object sender, PaintEventArgs e) => pbxRender.Image = renderer.Bitmap;

        private void RenderTimer_Tick(object sender, EventArgs e)
        {
            renderer.RenderNextFrame();

            switch (demoType)
            {
                case DemoType.Model:
                    MoveDemoModel();
                    break;
                case DemoType.Backhoe:
                    MoveDemoBackhoe();
                    break;
                case DemoType.Hand:
                    MoveDemoHand();
                    break;
                default:
                    break;
            }
        }

        private void MoveDemoModel()
        {
            Entity entity = renderer.Scene.Entities[0];
            entity.Rotation = new(0, entity.Rotation.Y + MathHelper.DegToRad(1), 0);
        }

        private void MoveDemoBackhoe()
        {
            Entity body = renderer.Scene.Entities[0].Children[0];
            body.Rotation = new(0, body.Rotation.Y + MathHelper.DegToRad(0.8), 0);

            Entity armOne = body.Children[0];
            Entity armTwo = armOne.Children[0];

            double movementOne = MathHelper.DegToRad(1);
            double movementTwo = -MathHelper.DegToRad(2);

            if (!openArm)
            {
                movementOne = -movementOne;
                movementTwo = -movementTwo;
            }

            armOne.Rotation = new(0, 0, armOne.Rotation.Z + movementOne);
            armTwo.Rotation = new(0, 0, armTwo.Rotation.Z + movementTwo);

            if (armOne.Rotation.Z >= MathHelper.DegToRad(90) || armOne.Rotation.Z <= MathHelper.DegToRad(0))
            {
                openArm = !openArm;
            }
        }

        private void MoveDemoHand()
        {
            // Get the references to the part of the hand
            Entity palm = renderer.Scene.Entities[0];
            Entity proximamalPhalanxOne = palm.Children[0];
            Entity proximamalPhalanxTwo = palm.Children[1];
            Entity proximamalPhalanxThree = palm.Children[2];
            Entity middlePhalanxOne = proximamalPhalanxOne.Children[0];
            Entity middlePhalanxTwo = proximamalPhalanxTwo.Children[0];
            Entity middlePhalanxThree = proximamalPhalanxThree.Children[0];

            // Move the fingers
            double rotationOne = openHandOne ? MathHelper.DegToRad(-1.2) : MathHelper.DegToRad(1.2);
            double rotationTwo = openHandTwo ? MathHelper.DegToRad(-1) : MathHelper.DegToRad(1);
            double rotationThree = openHandThree ? MathHelper.DegToRad(-0.9) : MathHelper.DegToRad(0.9);
            double max = MathHelper.DegToRad(90);
            double min = MathHelper.DegToRad(0);

            proximamalPhalanxOne.Rotation = new(proximamalPhalanxOne.Rotation.X, proximamalPhalanxOne.Rotation.Y, proximamalPhalanxOne.Rotation.Z + rotationOne);
            proximamalPhalanxTwo.Rotation = new(proximamalPhalanxTwo.Rotation.X, proximamalPhalanxTwo.Rotation.Y, proximamalPhalanxTwo.Rotation.Z + rotationTwo);
            proximamalPhalanxThree.Rotation = new(proximamalPhalanxThree.Rotation.X, proximamalPhalanxThree.Rotation.Y, proximamalPhalanxThree.Rotation.Z + rotationThree);

            middlePhalanxOne.Rotation = new(middlePhalanxOne.Rotation.X, middlePhalanxOne.Rotation.Y, middlePhalanxOne.Rotation.Z - rotationOne);
            middlePhalanxTwo.Rotation = new(middlePhalanxTwo.Rotation.X, middlePhalanxTwo.Rotation.Y, middlePhalanxTwo.Rotation.Z - rotationTwo);
            middlePhalanxThree.Rotation = new(middlePhalanxThree.Rotation.X, middlePhalanxThree.Rotation.Y, middlePhalanxThree.Rotation.Z - rotationThree);

            if (proximamalPhalanxOne.Rotation.Z > max || proximamalPhalanxOne.Rotation.Z < min)
            {
                openHandOne = !openHandOne;
            }

            if (proximamalPhalanxTwo.Rotation.Z > max || proximamalPhalanxTwo.Rotation.Z < min)
            {
                openHandTwo = !openHandTwo;
            }

            if (proximamalPhalanxThree.Rotation.Z > max || proximamalPhalanxThree.Rotation.Z < min)
            {
                openHandThree = !openHandThree;
            }

            // Move falco
            Entity falco = renderer.Scene.Entities[1];
            const double falcoDisance = 3d;
            falco.Position = new(Math.Sin(falcoAngle) * falcoDisance, 0, Math.Cos(falcoAngle) * falcoDisance);
            falcoAngle += MathHelper.DegToRad(1);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            RenderTimer_Tick(sender, e);
            Refresh();
        }

        private void FrmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
            {
                renderer.GoDown();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Space)
            {
                renderer.GoUp();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.W)
            {
                renderer.GoForward();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.A)
            {
                renderer.GoLeft();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.S)
            {
                renderer.GoBackward();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.D)
            {
                renderer.GoRight();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.K)
            {
                renderer.LookUp();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.J)
            {
                renderer.LookDown();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.H)
            {
                renderer.LookLeft();
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.L)
            {
                renderer.LookRight();
                e.Handled = true;
            }
        }

        private void ChbShowNormals_CheckedChanged(object sender, EventArgs e) => renderer.ShowNormals = chbShowNormals.Checked;

        private void CbxCullingMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            renderer.CullingMode = (CullingMode)cbxCullingMode.SelectedIndex;
            Refresh();
        }

        private void CbxProjectionMode_SelectedIndexChanged(object sender, EventArgs e) => renderer.ProjectionMode = (ProjectionMode)cbxProjectionMode.SelectedIndex;

        private void TrbScale_Scroll(object sender, EventArgs e) => renderer.Scene.Entities[0].Scale = 1d / (trbScale.Value / 100d);

        private void RdbDemo_CheckedChanged(object sender, EventArgs e)
        {
            string tag = (string)(sender as Control).Tag;
            demoType = (DemoType)int.Parse(tag);

            switch (demoType)
            {
                case DemoType.Model:
                    btnLoadModel.Visible = true;
                    renderer.Scene = demoModelScene;
                    break;
                case DemoType.Backhoe:
                    btnLoadModel.Visible = false;
                    renderer.Scene = demoBackhoeScene;
                    break;
                case DemoType.Hand:
                    btnLoadModel.Visible = false;
                    renderer.Scene = demoHandScene;
                    break;
                default:
                    break;
            }
        }

        private void BtnLoadModel_Click(object sender, EventArgs e)
        {
            using OpenFileDialog fileDialog = new();
            fileDialog.Filter = "ply files (*.ply)|*.ply";
            fileDialog.RestoreDirectory = true;

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = fileDialog.FileName;

                Model demoModel = Model.FromPly(path);

                demoModelScene.Entities.Clear();
                demoModelScene.Entities.Add(new(demoModel, null));
            }
        }

        private void SaveToolStripButton_Click(object sender, EventArgs e)
        {
            if (renderer.Bitmap == null)
            {
                return;
            }

            if (imageSaver.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(imageSaver.FileName))
                {
                    File.Delete(imageSaver.FileName);
                }

                if (imageSaver.FilterIndex == 0)
                {
                    renderer.Bitmap.Save(imageSaver.FileName, ImageFormat.Png);
                }
                else if (imageSaver.FilterIndex == 1)
                {
                    renderer.Bitmap.Save(imageSaver.FileName, ImageFormat.Bmp);
                }
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }
    }
}
