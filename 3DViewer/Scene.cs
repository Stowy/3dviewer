﻿/**
* @file Scene.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Scene class.
* @version 1.0
* @date 27.05.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace ThreeDViewer
{
    using System.Collections.Generic;
    using ThreeDViewer.Mathematics.Geometry;

    /// <summary>
    /// Represents a scene that contains entites and has a camera.
    /// </summary>
    public class Scene
    {
        private readonly EntityCollection entities;
        private readonly Camera camera;

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class.
        /// </summary>
        public Scene()
            : this(new EntityCollection(null))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class.
        /// </summary>
        /// <param name="entities">Entities contained in the scene.</param>
        public Scene(EntityCollection entities)
            : this(entities, new(new(0, 0, 3), 1))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class.
        /// </summary>
        /// <param name="camera">Camera of the scene.</param>
        public Scene(Camera camera)
            : this(new EntityCollection(null), camera)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class.
        /// </summary>
        /// <param name="entities">The entities contained in the scene.</param>
        /// <param name="camera">The camera of the scene.</param>
        public Scene(EntityCollection entities, Camera camera)
        {
            this.entities = entities;
            this.camera = camera;
        }

        /// <summary>
        /// Gets the collection of entities in the scene.
        /// </summary>
        public EntityCollection Entities => entities;

        /// <summary>
        /// Gets all the absolute faces sorted from the furthest to the camera to the closest.
        /// </summary>
        public List<Face> SortedAbsoluteFaces
        {
            get
            {
                List<Face> faces = entities.AbsoluteFaces;

                faces.Sort((l, r) => r.Center.Distance(camera.Position).CompareTo(l.Center.Distance(camera.Position)));

                return faces;
            }
        }

        /// <summary>
        /// Gets the camera of the scene.
        /// </summary>
        public Camera Camera => camera;
    }
}
