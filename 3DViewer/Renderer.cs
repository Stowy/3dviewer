﻿/**
* @file Renderer.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Renderer class.
* @version 1.0
* @date 27.04.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace ThreeDViewer
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using ThreeDViewer.Mathematics;
    using ThreeDViewer.Mathematics.Geometry;
    using ThreeDViewer.Utils;

    /// <summary>
    /// The class that renders a 3D model on a <see cref="System.Drawing.Bitmap"/>.
    /// </summary>
    public class Renderer
    {
        /// <summary>
        /// Movement speed of the camera.
        /// </summary>
        public const double CameraMoveSpeed = 0.5d;

        /// <summary>
        /// Look (rotation) speed of the camera.
        /// </summary>
        public const double CameraLookSpeed = 1d;

        /// <summary>
        /// Size of the axes in the world.
        /// </summary>
        public const double AxesSize = 3d;

        #region Fields
        private readonly Graphics g;
        private Scene scene;

        private Bitmap bitmap;

        private Matrix4 projection;

        private CullingMode cullingMode;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// </summary>
        /// <param name="mathematicalSize">Size of the box that contains the mathematical coordinates of the projected model.</param>
        /// <param name="imageSize">Size of the image that will be displayed.</param>
        /// <param name="showFill">Tells if the model should be filled or not.</param>
        /// <param name="showWireframe">Tells if the wireframe should be displayed.</param>
        public Renderer(
            Size mathematicalSize,
            Size imageSize,
            bool showFill = true,
            bool showWireframe = true)
        {
            // Set the passed values
            ShowFill = showFill;
            ShowWireframe = showWireframe;
            MathematicalSize = mathematicalSize;
            ImageSize = imageSize;

            OutlineColor = Pens.LightGreen;

            Bitmap = new Bitmap(ImageSize.Width, ImageSize.Height);
            g = Graphics.FromImage(Bitmap);

            double aspect = Bitmap.Height / (double)Bitmap.Width;
            scene = new Scene(new Camera(new(0, 0, 3), aspect));
            projection = scene.Camera.ViewMatrix * scene.Camera.ProjectionMatrix;
        }

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether the model should be drawn with filled triangles or not.
        /// </summary>
        public bool ShowFill { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the wireframe should be shown.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the wireframe should be shown; otherwise, <c>false</c>.
        /// </value>
        public bool ShowWireframe { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the normal of the faces should be shown.
        /// </summary>
        public bool ShowNormals { get; set; }

        /// <summary>
        /// Gets or sets the size of the mathematical projection area. It's the zone that will be displayed on the picture box.
        /// </summary>
        public Size MathematicalSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the image.
        /// </summary>
        /// <value>
        /// The size of the image.
        /// </value>
        public Size ImageSize { get; set; }

        /// <summary>
        /// Gets or sets the color of the outline of the polygons.
        /// </summary>
        /// <value>
        /// The color of the outline of the polygons.
        /// </value>
        public Pen OutlineColor { get; set; }

        /// <summary>
        /// Gets the rendered bitmap.
        /// </summary>
        public Bitmap Bitmap { get => bitmap; private set => bitmap = value; }

        /// <summary>
        /// Gets or sets the culling mode of the renderer.
        /// </summary>
        public CullingMode CullingMode { get => cullingMode; set => cullingMode = value; }

        /// <summary>
        /// Gets or sets the projection mode of the camera.
        /// </summary>
        public ProjectionMode ProjectionMode { get => scene.Camera.ProjectionMode; set => scene.Camera.ProjectionMode = value; }

        /// <summary>
        /// Gets or sets the scene contained in the renderer.
        /// </summary>
        public Scene Scene
        {
            get => scene;
            set => scene = value;
        }

        /// <summary>
        /// Gets or sets the color of the lighting.
        /// </summary>
        public Color LightColor { get; set; }

        #endregion

        #region Methods

        #region Movements

        /// <summary>
        /// Makes the camera go down.
        /// </summary>
        public void GoDown() => scene.Camera.Position -= Vector3.YAxis * CameraMoveSpeed;

        /// <summary>
        /// Makes the camera go up.
        /// </summary>
        public void GoUp() => scene.Camera.Position += Vector3.YAxis * CameraMoveSpeed;

        /// <summary>
        /// Makes the camera go forward.
        /// </summary>
        public void GoForward() => scene.Camera.Position += -Vector3.ZAxis * CameraMoveSpeed;

        /// <summary>
        /// Makes the camera go backward.
        /// </summary>
        public void GoBackward() => scene.Camera.Position -= -Vector3.ZAxis * CameraMoveSpeed;

        /// <summary>
        /// Makes the camera go left.
        /// </summary>
        public void GoLeft() => scene.Camera.Position -= Vector3.XAxis * CameraMoveSpeed;

        /// <summary>
        /// Makes the camera go right.
        /// </summary>
        public void GoRight() => scene.Camera.Position += Vector3.XAxis * CameraMoveSpeed;

        /// <summary>
        /// Makes camera look to the left.
        /// </summary>
        public void LookLeft() => scene.Camera.Yaw -= MathHelper.DegToRad(CameraLookSpeed);

        /// <summary>
        /// Makes the camera look to the right.
        /// </summary>
        public void LookRight() => scene.Camera.Yaw += MathHelper.DegToRad(CameraLookSpeed);

        /// <summary>
        /// Makes the camera look up.
        /// </summary>
        public void LookUp() => scene.Camera.Pitch += MathHelper.DegToRad(CameraLookSpeed);

        /// <summary>
        /// Makes the camera look down.
        /// </summary>
        public void LookDown() => scene.Camera.Pitch -= MathHelper.DegToRad(CameraLookSpeed);

        #endregion

        /// <summary>
        /// Renders the next frame.
        /// </summary>
        public void RenderNextFrame()
        {
            // Clear the image in black
            g.Clear(Color.Black);

            // Update the projection matrix
            projection = scene.Camera.ViewMatrix * scene.Camera.ProjectionMatrix;

            // Draw the X, Y and Z axes on the scene
            DrawAxes();

            // Draw the faces
            foreach (Face face in scene.SortedAbsoluteFaces)
            {
                // Don't draw the faces if we cull with the normals of the faces
                if (CullingMode == CullingMode.Normals)
                {
                    // The face is not facing the camera, we continue
                    if (face.Normal.IsBackFace(scene.Camera.LineOfSight))
                    {
                        continue;
                    }
                }

                // Light the face
                if (ShowFill)
                {
                    face.LightUp(scene.Camera.LineOfSight, LightColor);
                }

                // Project the face
                Face projectedFace = face * projection;

                // Draw the face
                DrawFace(projectedFace, face);
            }

            // Draw camera informations
            g.DrawString($"Camera pos : {scene.Camera.Position}", new Font("Cascadia Code", 13), Brushes.White, new PointF(0, 0));
            double pitch = Math.Round(MathHelper.RadToDeg(scene.Camera.Pitch));
            double yaw = Math.Round(MathHelper.RadToDeg(scene.Camera.Yaw));
            g.DrawString(
                $"Camera angle (pitch, yaw): {pitch}, {yaw}",
                new Font("Cascadia Code", 13),
                Brushes.White,
                new PointF(0, 20));
        }

        ////private void DrawEntities(EntityCollection entities)
        ////{
        ////    // Display each model
        ////    foreach (Entity entity in entities)
        ////    {
        ////        DrawModel(entity.AbsoluteModel);

        ////        DrawEntities(entity.Children);
        ////    }
        ////}

        private void DrawAxes()
        {
            // Set the coordinates of the axes
            Vector4 plusZ = new(Vector3.ZAxis * AxesSize, 1);
            Vector4 minusZ = new(-Vector3.ZAxis * AxesSize, 1);
            Vector4 plusX = new(Vector3.XAxis * AxesSize, 1);
            Vector4 minusX = new(-Vector3.XAxis * AxesSize, 1);
            Vector4 plusY = new(Vector3.YAxis * AxesSize, 1);
            Vector4 minusY = new(-Vector3.YAxis * AxesSize, 1);

            Vector4[] points = new Vector4[] { plusX, minusX, plusY, minusY, plusZ, minusZ };

            for (int i = 0; i < points.Length; i++)
            {
                points[i] *= projection;
            }

            PointF[] mappedPoints = new PointF[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
                mappedPoints[i] = points[i].ToPointF().MapToPixel(MathematicalSize, ImageSize);
            }

            // Draw the axes
            g.DrawLine(Pens.Red, mappedPoints[0], mappedPoints[1]);    // X
            g.DrawLine(Pens.Green, mappedPoints[2], mappedPoints[3]);  // Y
            g.DrawLine(Pens.Blue, mappedPoints[4], mappedPoints[5]);   // Z
        }

        private void DrawFace(Face projectedFace, Face transformedFace)
        {
            // Skip if we have backface culling
            if (CullingMode == CullingMode.Clockwise && projectedFace.IsClockwise)
            {
                return;
            }

            Vector3[] points = projectedFace.ToPhysicalCoords();

            // Map the points to pixels on the screen
            float mathMinX = 0 - (MathematicalSize.Width / 2);
            float mathMaxX = MathematicalSize.Width / 2;

            float mathMinY = 0 - (MathematicalSize.Height / 2);
            float mathMaxY = MathematicalSize.Height / 2;

            List<PointF> pointsF = new();
            for (int i = 0; i < points.Length; i++)
            {
                PointF pointf = points[i].ToPointF();

                // Check if the point is in the image
                if (pointf.X < mathMinX || pointf.X > mathMaxX || pointf.Y < mathMinY || pointf.Y > mathMaxY)
                {
                    continue;
                }

                // Map the mathematical coordinates to pixels on the screen and add them to the list
                pointsF.Add(pointf.MapToPixel(MathematicalSize, ImageSize));
            }

            // Check if we have a face
            if (pointsF.Count < Face.MinVertices)
            {
                return;
            }

            // Draw the filled polygon
            if (ShowFill)
            {
                g.FillPolygon(new SolidBrush(projectedFace.Color), pointsF.ToArray());
            }

            // Draw the wireframe
            if (ShowWireframe)
            {
                g.DrawPolygon(OutlineColor, pointsF.ToArray());
            }

            // Draw the normal
            if (ShowNormals)
            {
                Vector3 offsetedNormal = transformedFace.Normal + transformedFace.Center;
                Vector4 projectedNormal = offsetedNormal.ToVector4() * projection;
                Vector3 projectedCenter = (transformedFace.Center.ToVector4() * projection).ToPhysicalCoords();

                PointF mappedCenter = projectedCenter.ToPointF().MapToPixel(MathematicalSize, ImageSize);

                PointF mappedNormal = projectedNormal.ToPointF().MapToPixel(MathematicalSize, ImageSize);

                g.DrawLine(Pens.Gold, mappedNormal, mappedCenter);
            }
        }
        #endregion
    }
}
