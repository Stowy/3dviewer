﻿namespace ThreeDViewer
{
    /// <summary>
    /// Defines the projection matrix that will be used.
    /// </summary>
    public enum ProjectionMode
    {
        /// <summary>
        /// Project with a perspective projection matrix.
        /// </summary>
        Perspective = 0,

        /// <summary>
        /// Project with an orthographic projection matrix.
        /// </summary>
        Orthographic = 1,
    }
}
