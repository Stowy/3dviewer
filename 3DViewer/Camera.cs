﻿/**
 * @file Camera.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Camera class.
 * @version 1.0
 * @date 11.05.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer
{
    using System;
    using ThreeDViewer.Mathematics;
    using ThreeDViewer.Utils;

    /// <summary>
    /// A class to represent a camera in 3D Space.
    /// Inspired by this : https://opentk.net/learn/chapter1/9-camera.html
    /// and this : https://github.com/opentk/LearnOpenTK/blob/master/Common/Camera.cs .
    /// </summary>
    public class Camera
    {
        private const double DepthNear = 0.01d;
        private const double DepthFar = 100d;

        private Vector3 front;
        private Vector3 up;
        private Vector3 right;

        private double pitch;
        private double yaw;
        private double fov;

        /// <summary>
        /// Initializes a new instance of the <see cref="Camera"/> class.
        /// </summary>
        /// <param name="position">Position of the camera.</param>
        /// <param name="aspectRatio">Aspect ration of the camera.</param>
        public Camera(Vector3 position, double aspectRatio)
        {
            Position = position;
            AspectRatio = aspectRatio;

            front = -Vector3.ZAxis;
            up = Vector3.YAxis;
            right = Vector3.XAxis;

            pitch = 0;
            yaw = -MathHelper.PiOver2;
            fov = MathHelper.PiOver2;
        }

        /// <summary>
        /// Gets or sets the position of the camera.
        /// </summary>
        public Vector3 Position { get; set; }

        /// <summary>
        /// Gets or sets the projection mode of the camera.
        /// </summary>
        public ProjectionMode ProjectionMode { get; set; }

        /// <summary>
        /// Sets the aspect ratio of the camera.
        /// </summary>
        public double AspectRatio { private get; set; }

        /// <summary>
        /// Gets the front vector of the camera.
        /// </summary>
        public Vector3 Front => front;

        /// <summary>
        /// Gets the up vector of the camera.
        /// </summary>
        public Vector3 Up => up;

        /// <summary>
        /// Gets the right vector of the camera.
        /// </summary>
        public Vector3 Right => right;

        /// <summary>
        /// Gets or sets the pitch in radians of the camera.
        /// </summary>
        public double Pitch
        {
            get => pitch;
            set
            {
                // Clamped to avoid gimball lock
                double angle = Math.Clamp(value, MathHelper.DegToRad(-89d), MathHelper.DegToRad(89d));
                pitch = angle;
                UpdateVectors();
            }
        }

        /// <summary>
        /// Gets or sets the yaw in radians of the camera.
        /// </summary>
        public double Yaw
        {
            get => yaw;
            set
            {
                yaw = value;
                UpdateVectors();
            }
        }

        /// <summary>
        /// Gets or sets the field of view in radians of the camera.
        /// </summary>
        public double Fov
        {
            get => fov;
            set
            {
                double angle = Math.Clamp(value, MathHelper.DegToRad(1d), MathHelper.DegToRad(45d));
                fov = angle;
            }
        }

        /// <summary>
        /// Gets the line of sight of the camera.
        /// </summary>
        public Vector3 LineOfSight => (Position + Front).NormalizeOrDefault();

        /// <summary>
        /// Gets the view matrix of the camera.
        /// Should be used to use to transform from world space to view space.
        /// </summary>
        public Matrix4 ViewMatrix => Matrix4.LookAt(Position, Position + Front, Up);

        /// <summary>
        /// Gets the projection matrix of the camera.
        /// It's a perspective projection.
        /// </summary>
        public Matrix4 ProjectionMatrix
        {
            get
            {
                return ProjectionMode switch
                {
                    ProjectionMode.Perspective => Matrix4.CreatePerspectiveFieldOfView(Fov, AspectRatio, DepthNear, DepthFar),
                    _ => Matrix4.Identity,
                };
            }
        }

        private void UpdateVectors()
        {
            // First the front vector is calculated using some trigonometry
            front = new Vector3(
                Math.Cos(pitch) * Math.Cos(yaw),
                Math.Sin(pitch),
                Math.Cos(pitch) * Math.Sin(yaw));

            // We need to make sure the vectors are all normalized to not get some funky results
            front = front.Normalize();

            // Calculate both the right and the up vector using cross product
            // Note that we are calculating the right from the global up, this behaviour is best suited for an FPS camera
            up = Vector3.YAxis;
            right = Vector3.Normalize(front.Cross(up));
        }
    }
}
