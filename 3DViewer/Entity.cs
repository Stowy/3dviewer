﻿/**
* @file Entity.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Entity class.
* @version 1.0
* @date 21.05.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace ThreeDViewer
{
    using ThreeDViewer.Mathematics;
    using ThreeDViewer.Mathematics.Geometry;

    /// <summary>
    /// Represents an entity that can be show with the renderer and that has children.
    /// </summary>
    public class Entity
    {
        private readonly EntityCollection children;
        private Entity parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        public Entity() => children = new(this);

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="model">3D model of this entity.</param>
        /// <param name="parent">Parent of this entity.</param>
        public Entity(Model model, Entity parent)
        {
            Model = model;

            children = new(this);
            Parent = parent;
            parent?.Children.Add(this);
            Scale = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entity"/> class.
        /// </summary>
        /// <param name="model">3D model of this entity.</param>
        public Entity(Model model)
            : this(model, null)
        {
        }

        /// <summary>
        /// Gets or sets the 3D model of this entity.
        /// </summary>
        public Model Model { get; set; }

        /// <summary>
        /// Gets or sets the position of this entity.
        /// </summary>
        public Vector3 Position { get; set; }

        /// <summary>
        /// Gets or sets the rotation in radian of this entity.
        /// </summary>
        public Vector3 Rotation { get; set; }

        /// <summary>
        /// Gets or sets the scale of this entity.
        /// </summary>
        public double Scale { get; set; }

        /// <summary>
        /// Gets the children of this entity.
        /// </summary>
        public EntityCollection Children => children;

        /// <summary>
        /// Gets or sets the parent of this entity.
        /// </summary>
        public Entity Parent
        {
            get => parent;
            set
            {
                Entity oldParent = parent;
                parent = value;

                if (oldParent != null)
                {
                    oldParent.children.Remove(this);
                }

                if (value != null)
                {
                    value.Children.Add(this);
                }
            }
        }

        /// <summary>
        ///  Gets a value indicating whether this entity is root or not.
        /// </summary>
        public bool IsRoot => Parent == null;

        /// <summary>
        /// Gets a value indicating whether this entity is a leaf or not.
        /// </summary>
        public bool IsLeaf => Children.Count == 0;

        /// <summary>
        /// Gets the relative transformation matrix of this entity.
        /// Contains the scale, rotation and translation of this entity.
        /// </summary>
        public Matrix4 Transformation
        {
            get
            {
                Matrix4 transf = Matrix4.CreateScale(Scale);
                transf *= Matrix4.CreateRotation(Rotation);
                transf *= Matrix4.CreateTranslation(Position);

                return transf;
            }
        }

        /// <summary>
        ///  Gets the absolute transformation matrix of this entity.
        ///  Contains the scale, rotation and translation of this entity multiplied by his parent's.
        ///  Gets <see cref="Transformation"/> if <see cref="IsRoot"/> is false.
        /// </summary>
        public Matrix4 AbsoluteTransformation
        {
            get
            {
                Matrix4 transf = Transformation;

                if (!IsRoot)
                {
                    transf *= Parent.AbsoluteTransformation;
                }

                return transf;
            }
        }

        /// <summary>
        /// Gets the absolute model of this entity.
        /// Use this model to project and draw the entity.
        /// </summary>
        public Model AbsoluteModel => Model * AbsoluteTransformation;
    }
}
