﻿/**
 * @file Matrix4.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Matrix4 struct.
 * @version 1.0
 * @date 25.01.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Mathematics
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using ThreeDViewer.Utils;

    /// <summary>
    /// <para>Represents a matrix of size 4x4.</para>
    /// <para>Every coordinates are in [y, x] format (or [row, column]).</para>
    /// </summary>
    [ImmutableObject(true)]
    public struct Matrix4
        : IEquatable<Matrix4>
    {
        #region Constants

        /// <summary>
        /// Size of the matrix.
        /// </summary>
        public const int Size = 4;

        /// <summary>
        /// The identity matrix.
        /// </summary>
        public static readonly Matrix4 Identity = new(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>
        /// Matrix for projection onto the x = 0 plane.
        /// </summary>
        public static readonly Matrix4 ProjectionOrthoX = new(
            0, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>
        /// Matrix for projection onto the y = 0 plane.
        /// </summary>
        public static readonly Matrix4 ProjectionOrthoY = new(
            1, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>
        /// Matrix for projection onto the z = 0 plane.
        /// </summary>
        public static readonly Matrix4 ProjectionOrthoZ = new(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 1);

        /// <summary>
        /// The zero matrix.
        /// </summary>
        public static readonly Matrix4 Zero = new(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        #endregion

        private readonly double[,] values;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="values">Values of the matrix. Should be 4x4.</param>
        public Matrix4(double[,] values)
        {
            this.values = new double[Size, Size]
            {
                   { values[0, 0], values[0, 1], values[0, 2], values[0, 3] },
                   { values[1, 0], values[1, 1], values[1, 2], values[1, 3] },
                   { values[2, 0], values[2, 1], values[2, 2], values[2, 3] },
                   { values[3, 0], values[3, 1], values[3, 2], values[3, 3] },
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="value00">Value of the matrix in the 0, 0 position.</param>
        /// <param name="value01">Value of the matrix in the 0, 1 position.</param>
        /// <param name="value02">Value of the matrix in the 0, 2 position.</param>
        /// <param name="value03">Value of the matrix in the 0, 3 position.</param>
        /// <param name="value10">Value of the matrix in the 1, 0 position.</param>
        /// <param name="value11">Value of the matrix in the 1, 1 position.</param>
        /// <param name="value12">Value of the matrix in the 1, 2 position.</param>
        /// <param name="value13">Value of the matrix in the 1, 3 position.</param>
        /// <param name="value20">Value of the matrix in the 2, 0 position.</param>
        /// <param name="value21">Value of the matrix in the 2, 1 position.</param>
        /// <param name="value22">Value of the matrix in the 2, 2 position.</param>
        /// <param name="value23">Value of the matrix in the 2, 3 position.</param>
        /// <param name="value30">Value of the matrix in the 3, 0 position.</param>
        /// <param name="value31">Value of the matrix in the 3, 1 position.</param>
        /// <param name="value32">Value of the matrix in the 3, 2 position.</param>
        /// <param name="value33">Value of the matrix in the 3, 3 position.</param>
        [SuppressMessage(
            "StyleCop.CSharp.ReadabilityRules",
            "SA1117:Parameters should be on same line or separate lines",
            Justification = "For better readability of Matrix struct.")]
        public Matrix4(
            double value00, double value01, double value02, double value03,
            double value10, double value11, double value12, double value13,
            double value20, double value21, double value22, double value23,
            double value30, double value31, double value32, double value33)
             : this(new double[Size, Size]
            {
                { value00, value01, value02, value03 },
                { value10, value11, value12, value13 },
                { value20, value21, value22, value23 },
                { value30, value31, value32, value33 },
            })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="matrix">Matrix struct to take the values from.</param>
        public Matrix4(Matrix4 matrix)
            : this(matrix.values)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="values">Values of the Matrix.</param>
        /// /// <param name="isColums">Tells if the vector is a column vector or a row vector.</param>
        public Matrix4(Vector4[] values, bool isColums = true)
        {
            if (values.Length != Size)
            {
                throw new ArgumentException($"There should be exactly {Size} vectors.", nameof(values));
            }

            this.values = new double[Size, Size];

            if (isColums)
            {
                this.values[0, 0] = values[0].X;
                this.values[1, 0] = values[0].Y;
                this.values[2, 0] = values[0].Z;
                this.values[3, 0] = values[0].W;

                this.values[0, 1] = values[1].X;
                this.values[1, 1] = values[1].Y;
                this.values[2, 1] = values[1].Z;
                this.values[3, 1] = values[1].W;

                this.values[0, 2] = values[2].X;
                this.values[1, 2] = values[2].Y;
                this.values[2, 2] = values[2].Z;
                this.values[3, 2] = values[2].W;

                this.values[0, 3] = values[3].X;
                this.values[1, 3] = values[3].Y;
                this.values[2, 3] = values[3].Z;
                this.values[3, 3] = values[3].W;
            }
            else
            {
                this.values[0, 0] = values[0].X;
                this.values[0, 1] = values[0].Y;
                this.values[0, 2] = values[0].Z;
                this.values[0, 3] = values[0].W;

                this.values[1, 0] = values[1].X;
                this.values[1, 1] = values[1].Y;
                this.values[1, 2] = values[1].Z;
                this.values[1, 3] = values[1].W;

                this.values[2, 0] = values[2].X;
                this.values[2, 1] = values[2].Y;
                this.values[2, 2] = values[2].Z;
                this.values[2, 3] = values[2].W;

                this.values[3, 0] = values[3].X;
                this.values[3, 1] = values[3].Y;
                this.values[3, 2] = values[3].Z;
                this.values[3, 3] = values[3].W;
            }
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the values of the matrix.
        /// </summary>
        public double[,] Values
        {
            get
            {
                return new double[Size, Size]
                {
                    { values[0, 0], values[0, 1], values[0, 2], values[0, 3] },
                    { values[1, 0], values[1, 1], values[1, 2], values[1, 3] },
                    { values[2, 0], values[2, 1], values[2, 2], values[2, 3] },
                    { values[3, 0], values[3, 1], values[3, 2], values[3, 3] },
                };
            }
        }

        /// <summary>
        /// Gets the value at 0, 0 on the matrix.
        /// </summary>
        public double V00 => values[0, 0];

        /// <summary>
        /// Gets the value at 0, 1 on the matrix.
        /// </summary>
        public double V01 => values[0, 1];

        /// <summary>
        /// Gets the value at 0, 2 on the matrix.
        /// </summary>
        public double V02 => values[0, 2];

        /// <summary>
        /// Gets the value at 0, 3 on the matrix.
        /// </summary>
        public double V03 => values[0, 3];

        /// <summary>
        /// Gets the value at 1, 0 on the matrix.
        /// </summary>
        public double V10 => values[1, 0];

        /// <summary>
        /// Gets the value at 1, 1 on the matrix.
        /// </summary>
        public double V11 => values[1, 1];

        /// <summary>
        /// Gets the value at 1, 2 on the matrix.
        /// </summary>
        public double V12 => values[1, 2];

        /// <summary>
        /// Gets the value at 1, 3 on the matrix.
        /// </summary>
        public double V13 => values[1, 3];

        /// <summary>
        /// Gets the value at 2, 0 on the matrix.
        /// </summary>
        public double V20 => values[2, 0];

        /// <summary>
        /// Gets the value at 2, 1 on the matrix.
        /// </summary>
        public double V21 => values[2, 1];

        /// <summary>
        /// Gets the value at 2, 2 on the matrix.
        /// </summary>
        public double V22 => values[2, 2];

        /// <summary>
        /// Gets the value at 2, 3 on the matrix.
        /// </summary>
        public double V23 => values[2, 3];

        /// <summary>
        /// Gets the value at 3, 0 on the matrix.
        /// </summary>
        public double V30 => values[3, 0];

        /// <summary>
        /// Gets the value at 3, 1 on the matrix.
        /// </summary>
        public double V31 => values[3, 1];

        /// <summary>
        /// Gets the value at 3, 2 on the matrix.
        /// </summary>
        public double V32 => values[3, 2];

        /// <summary>
        /// Gets the value at 3, 3 on the matrix.
        /// </summary>
        public double V33 => values[3, 3];

        /// <summary>
        /// Gets the determinant of the matrix.
        /// </summary>
        public double Determinant
        {
            get
            {
                return (values[0, 0] * values[1, 1] * values[2, 2] * values[3, 3]) -
                    (values[0, 0] * values[1, 1] * values[2, 3] * values[3, 2]) +
                    (values[0, 0] * values[1, 2] * values[2, 3] * values[3, 1]) -
                    (values[0, 0] * values[1, 2] * values[2, 1] * values[3, 3]) +
                    (values[0, 0] * values[1, 3] * values[2, 1] * values[3, 2]) -
                    (values[0, 0] * values[1, 3] * values[2, 2] * values[3, 1]) -
                    (values[0, 1] * values[1, 2] * values[2, 3] * values[3, 0]) +
                    (values[0, 1] * values[1, 2] * values[2, 0] * values[3, 3]) -
                    (values[0, 1] * values[1, 3] * values[2, 0] * values[3, 2]) +
                    (values[0, 1] * values[1, 3] * values[2, 2] * values[3, 0]) -
                    (values[0, 1] * values[1, 0] * values[2, 2] * values[3, 3]) +
                    (values[0, 1] * values[1, 0] * values[2, 3] * values[3, 2]) +

                    (values[0, 2] * values[1, 3] * values[2, 0] * values[3, 1]) -
                    (values[0, 2] * values[1, 3] * values[2, 1] * values[3, 0]) +
                    (values[0, 2] * values[1, 0] * values[2, 1] * values[3, 3]) -
                    (values[0, 2] * values[1, 0] * values[2, 3] * values[3, 1]) +
                    (values[0, 2] * values[1, 1] * values[2, 3] * values[3, 0]) +
                    (values[0, 2] * values[1, 1] * values[2, 0] * values[3, 3]) -
                    (values[0, 3] * values[1, 0] * values[2, 1] * values[3, 2]) +
                    (values[0, 3] * values[1, 0] * values[2, 2] * values[3, 1]) -
                    (values[0, 3] * values[1, 1] * values[2, 2] * values[3, 0]) +
                    (values[0, 3] * values[1, 1] * values[2, 0] * values[3, 2]) -
                    (values[0, 3] * values[1, 2] * values[2, 0] * values[3, 1]) +
                    (values[0, 3] * values[1, 2] * values[2, 1] * values[3, 0]);
            }
        }

        /// <summary>
        /// Gets the normalized matrix.
        /// </summary>
        public Matrix4 Normalized => Normalize(this);

        /// <summary>
        /// Gets the trace of the matrix, the sum of the values along the diagonal axis.
        /// </summary>
        public double Trace => values[0, 0] + values[1, 1] + values[2, 2] + values[3, 3];

        #endregion Properties

        #region Operators

        /// <summary>
        /// Gets the value at the specified row and column.
        /// </summary>
        /// <param name="rowIndex">The index of the row.</param>
        /// <param name="columnIndex">The index of the column.</param>
        /// <returns>The element at the given row and column index.</returns>
        public double this[int rowIndex, int columnIndex] => values[rowIndex, columnIndex];

        /// <summary>
        /// Adds a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to add to.</param>
        /// <param name="scalar">Scalar to add to the matrix.</param>
        /// <returns>The added matrix.</returns>
        public static Matrix4 operator +(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size]
            {
                { mat[0, 0] + scalar, mat[0, 1] + scalar, mat[0, 2] + scalar, mat[0, 3] + scalar },
                { mat[1, 0] + scalar, mat[1, 1] + scalar, mat[1, 2] + scalar, mat[1, 3] + scalar },
                { mat[2, 0] + scalar, mat[2, 1] + scalar, mat[2, 2] + scalar, mat[2, 3] + scalar },
                { mat[3, 0] + scalar, mat[3, 1] + scalar, mat[3, 2] + scalar, mat[3, 3] + scalar },
            };

            return new(newMat);
        }

        /// <summary>
        /// Adds a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to add to.</param>
        /// <param name="scalar">Scalar to add to the matrix.</param>
        /// <returns>The added matrix.</returns>
        public static Matrix4 operator +(double scalar, Matrix4 mat) => mat + scalar;

        /// <summary>
        /// Substracts a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to substract to.</param>
        /// <param name="scalar">Scalar to substract to the matrix.</param>
        /// <returns>The substracted matrix.</returns>
        public static Matrix4 operator -(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size]
            {
                { mat[0, 0] - scalar, mat[0, 1] - scalar, mat[0, 2] - scalar, mat[0, 3] - scalar },
                { mat[1, 0] - scalar, mat[1, 1] - scalar, mat[1, 2] - scalar, mat[1, 3] - scalar },
                { mat[2, 0] - scalar, mat[2, 1] - scalar, mat[2, 2] - scalar, mat[2, 3] - scalar },
                { mat[3, 0] - scalar, mat[3, 1] - scalar, mat[3, 2] - scalar, mat[3, 3] - scalar },
            };

            return new(newMat);
        }

        /// <summary>
        /// Substracts a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to substract to.</param>
        /// <param name="scalar">Scalar to substract to the matrix.</param>
        /// <returns>The substracted matrix.</returns>
        public static Matrix4 operator -(double scalar, Matrix4 mat) => mat - scalar;

        /// <summary>
        /// Multiplies a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to multiply.</param>
        /// <param name="scalar">Scalar to multiply with the matrix.</param>
        /// <returns>The multiplied matrix.</returns>
        public static Matrix4 operator *(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size]
            {
                { mat[0, 0] * scalar, mat[0, 1] * scalar, mat[0, 2] * scalar, mat[0, 3] * scalar },
                { mat[1, 0] * scalar, mat[1, 1] * scalar, mat[1, 2] * scalar, mat[1, 3] * scalar },
                { mat[2, 0] * scalar, mat[2, 1] * scalar, mat[2, 2] * scalar, mat[2, 3] * scalar },
                { mat[3, 0] * scalar, mat[3, 1] * scalar, mat[3, 2] * scalar, mat[3, 3] * scalar },
            };

            return new(newMat);
        }

        /// <summary>
        /// Multiplies a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to multiply.</param>
        /// <param name="scalar">Scalar to multiply with the matrix.</param>
        /// <returns>The multiplied matrix.</returns>
        public static Matrix4 operator *(double scalar, Matrix4 mat) => mat * scalar;

        /// <summary>
        /// Divides a matrix by a scalar.
        /// </summary>
        /// <param name="mat">Matrix to divide.</param>
        /// <param name="scalar">Scalar to divide the matrix with.</param>
        /// <returns>The divided matrix.</returns>
        public static Matrix4 operator /(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size]
            {
                { mat[0, 0] / scalar, mat[0, 1] / scalar, mat[0, 2] / scalar, mat[0, 3] / scalar },
                { mat[1, 0] / scalar, mat[1, 1] / scalar, mat[1, 2] / scalar, mat[1, 3] / scalar },
                { mat[2, 0] / scalar, mat[2, 1] / scalar, mat[2, 2] / scalar, mat[2, 3] / scalar },
                { mat[3, 0] / scalar, mat[3, 1] / scalar, mat[3, 2] / scalar, mat[3, 3] / scalar },
            };

            return new(newMat);
        }

        /// <summary>
        /// Inverses the sign of a matrix.
        /// </summary>
        /// <param name="mat">Matrix to inverse the sign of.</param>
        /// <returns>The sign inverted matrix.</returns>
        public static Matrix4 operator -(Matrix4 mat)
        {
            double[,] newMat = new double[Size, Size]
            {
                { -mat.values[0, 0], -mat.values[0, 1], -mat.values[0, 2], -mat.values[0, 3] },
                { -mat.values[1, 0], -mat.values[1, 1], -mat.values[1, 2], -mat.values[1, 3] },
                { -mat.values[2, 0], -mat.values[2, 1], -mat.values[2, 2], -mat.values[2, 3] },
                { -mat.values[3, 0], -mat.values[3, 1], -mat.values[3, 2], -mat.values[3, 3] },
            };

            return new(newMat);
        }

        /// <summary>
        /// Adds two matrices.
        /// </summary>
        /// <param name="left">The matrix on the left.</param>
        /// <param name="right">The matrix on the right.</param>
        /// <returns>The added matrix.</returns>
        public static Matrix4 operator +(Matrix4 left, Matrix4 right)
        {
            double[,] newMat = new double[Size, Size]
            {
                { left[0, 0] + right[0, 0], left[0, 1] + right[0, 0], left[0, 2] + right[0, 0], left[0, 3] + right[0, 0] },
                { left[1, 0] + right[1, 0], left[1, 1] + right[1, 0], left[1, 2] + right[1, 0], left[1, 3] + right[1, 0] },
                { left[2, 0] + right[2, 0], left[2, 1] + right[2, 0], left[2, 2] + right[2, 0], left[2, 3] + right[2, 0] },
                { left[3, 0] + right[3, 0], left[3, 1] + right[3, 0], left[3, 2] + right[3, 0], left[3, 3] + right[3, 0] },
            };

            return new(newMat);
        }

        /// <summary>
        /// Substracts two matrices.
        /// </summary>
        /// <param name="left">The matrix on the left.</param>
        /// <param name="right">The matrix on the right.</param>
        /// <returns>The substracted matrix.</returns>
        public static Matrix4 operator -(Matrix4 left, Matrix4 right)
        {
            double[,] newMat = new double[Size, Size]
            {
                { left[0, 0] - right[0, 0], left[0, 1] - right[0, 0], left[0, 2] - right[0, 0], left[0, 3] - right[0, 0] },
                { left[1, 0] - right[1, 0], left[1, 1] - right[1, 0], left[1, 2] - right[1, 0], left[1, 3] - right[1, 0] },
                { left[2, 0] - right[2, 0], left[2, 1] - right[2, 0], left[2, 2] - right[2, 0], left[2, 3] - right[2, 0] },
                { left[3, 0] - right[3, 0], left[3, 1] - right[3, 0], left[3, 2] - right[3, 0], left[3, 3] - right[3, 0] },
            };

            return new(newMat);
        }

        /// <summary>
        /// Multiplies two matrices.
        /// </summary>
        /// <param name="left">The left matrix.</param>
        /// <param name="right">The right matrix.</param>
        /// <returns>The dot product of the matrices.</returns>
        public static Matrix4 operator *(Matrix4 left, Matrix4 right)
        {
            double[,] newMat = new double[Size, Size];

            // First row
            newMat[0, 0] = (left.values[0, 0] * right.values[0, 0]) +
                (left.values[0, 1] * right.values[1, 0]) +
                (left.values[0, 2] * right.values[2, 0]) +
                (left.values[0, 3] * right.values[3, 0]);

            newMat[0, 1] = (left.values[0, 0] * right.values[0, 1]) +
                (left.values[0, 1] * right.values[1, 1]) +
                (left.values[0, 2] * right.values[2, 1]) +
                (left.values[0, 3] * right.values[3, 1]);

            newMat[0, 2] = (left.values[0, 0] * right.values[0, 2]) +
                (left.values[0, 1] * right.values[1, 2]) +
                (left.values[0, 2] * right.values[2, 2]) +
                (left.values[0, 3] * right.values[3, 2]);

            newMat[0, 3] = (left.values[0, 0] * right.values[0, 3]) +
                (left.values[0, 1] * right.values[1, 3]) +
                (left.values[0, 2] * right.values[2, 3]) +
                (left.values[0, 3] * right.values[3, 3]);

            // Second row
            newMat[1, 0] = (left.values[1, 0] * right.values[0, 0]) +
                (left.values[1, 1] * right.values[1, 0]) +
                (left.values[1, 2] * right.values[2, 0]) +
                (left.values[1, 3] * right.values[3, 0]);

            newMat[1, 1] = (left.values[1, 0] * right.values[0, 1]) +
                (left.values[1, 1] * right.values[1, 1]) +
                (left.values[1, 2] * right.values[2, 1]) +
                (left.values[1, 3] * right.values[3, 1]);

            newMat[1, 2] = (left.values[1, 0] * right.values[0, 2]) +
                (left.values[1, 1] * right.values[1, 2]) +
                (left.values[1, 2] * right.values[2, 2]) +
                (left.values[1, 3] * right.values[3, 2]);

            newMat[1, 3] = (left.values[1, 0] * right.values[0, 3]) +
                (left.values[1, 1] * right.values[1, 3]) +
                (left.values[1, 2] * right.values[2, 3]) +
                (left.values[1, 3] * right.values[3, 3]);

            // Third row
            newMat[2, 0] = (left.values[2, 0] * right.values[0, 0]) +
                (left.values[2, 1] * right.values[1, 0]) +
                (left.values[2, 2] * right.values[2, 0]) +
                (left.values[2, 3] * right.values[3, 0]);

            newMat[2, 1] = (left.values[2, 0] * right.values[0, 1]) +
                (left.values[2, 1] * right.values[1, 1]) +
                (left.values[2, 2] * right.values[2, 1]) +
                (left.values[2, 3] * right.values[3, 1]);

            newMat[2, 2] = (left.values[2, 0] * right.values[0, 2]) +
                (left.values[2, 1] * right.values[1, 2]) +
                (left.values[2, 2] * right.values[2, 2]) +
                (left.values[2, 3] * right.values[3, 2]);

            newMat[2, 3] = (left.values[2, 0] * right.values[0, 3]) +
                (left.values[2, 1] * right.values[1, 3]) +
                (left.values[2, 2] * right.values[2, 3]) +
                (left.values[2, 3] * right.values[3, 3]);

            // Fourth row
            newMat[3, 0] = (left.values[3, 0] * right.values[0, 0]) +
                (left.values[3, 1] * right.values[1, 0]) +
                (left.values[3, 2] * right.values[2, 0]) +
                (left.values[3, 3] * right.values[3, 0]);

            newMat[3, 1] = (left.values[3, 0] * right.values[0, 1]) +
                (left.values[3, 1] * right.values[1, 1]) +
                (left.values[3, 2] * right.values[2, 1]) +
                (left.values[3, 3] * right.values[3, 1]);

            newMat[3, 2] = (left.values[3, 0] * right.values[0, 2]) +
                (left.values[3, 1] * right.values[1, 2]) +
                (left.values[3, 2] * right.values[2, 2]) +
                (left.values[3, 3] * right.values[3, 2]);

            newMat[3, 3] = (left.values[3, 0] * right.values[0, 3]) +
                (left.values[3, 1] * right.values[1, 3]) +
                (left.values[3, 2] * right.values[2, 3]) +
                (left.values[3, 3] * right.values[3, 3]);

            return new(newMat);
        }

        /// <summary>
        /// Check if two matrices are equal.
        /// </summary>
        /// <param name="left">The left matrix.</param>
        /// <param name="right">The right matrix.</param>
        /// <returns>True if the matrices are equal.</returns>
        public static bool operator ==(Matrix4 left, Matrix4 right) => left.Equals(right);

        /// <summary>
        /// Check if two matrices are not equal.
        /// </summary>
        /// <param name="left">The left matrix.</param>
        /// <param name="right">The right matrix.</param>
        /// <returns>True if the matrices are not equal.</returns>
        public static bool operator !=(Matrix4 left, Matrix4 right) => !(left == right);

        #endregion Operators

        #region Methods

        /// <summary>
        /// Builds a rotation matrix for a rotation around x-axis.
        /// </summary>
        /// <param name="angle">The counter clockwise angle in radians.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotationX(double angle)
        {
            return new(new double[,]
            {
                { 1, 0, 0, 0 },
                { 0, Math.Cos(angle), Math.Sin(angle), 0 },
                { 0, -Math.Sin(angle), Math.Cos(angle), 0 },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Builds a rotation matrix for a rotation around y-axis.
        /// </summary>
        /// <param name="angle">The counter clockwise angle in radians.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotationY(double angle)
        {
            return new Matrix4(new double[,]
            {
                { Math.Cos(angle), 0, -Math.Sin(angle), 0 },
                { 0, 1, 0, 0 },
                { Math.Sin(angle), 0, Math.Cos(angle), 0 },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Builds a rotation matrix for a rotation around z-axis.
        /// </summary>
        /// <param name="angle">The counter clockwise angle in radians.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotationZ(double angle)
        {
            return new Matrix4(new double[,]
            {
                { Math.Cos(angle), Math.Sin(angle), 0, 0 },
                { -Math.Sin(angle), Math.Cos(angle), 0, 0 },
                { 0, 0, 1, 0 },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Builds a combined rotation matrix around the X, Y and Z axes.
        /// Angles in radians.
        /// </summary>
        /// <param name="rotation">Rotation that will be applied in the matrix.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotation(Vector3 rotation)
        {
            double x = rotation.X;
            double y = rotation.Y;
            double z = rotation.Z;

            return new Matrix4(new double[,]
            {
                {
                    Math.Cos(y) * Math.Cos(z),
                    Math.Cos(y) * Math.Sin(z),
                    -Math.Sin(y),
                    0,
                },
                {
                    (Math.Sin(x) * Math.Sin(y) * Math.Cos(z)) - (Math.Cos(x) * Math.Sin(z)),
                    (Math.Sin(x) * Math.Sin(y) * Math.Sin(z)) + (Math.Cos(x) * Math.Cos(z)),
                    Math.Sin(x) * Math.Cos(y),
                    0,
                },
                {
                    (Math.Cos(x) * Math.Sin(y) * Math.Cos(z)) + (Math.Sin(x) * Math.Sin(z)),
                    (Math.Cos(x) * Math.Sin(y) * Math.Sin(z)) - (Math.Sin(x) * Math.Cos(z)),
                    Math.Cos(x) * Math.Cos(y),
                    0,
                },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Builds a rotation matrix around an arbitrary axis.
        /// </summary>
        /// <param name="rotationAxis">The arbitrary axis to rotate about.</param>
        /// <param name="angle">The angle of the rotation.</param>
        /// <returns>The constructed rotation matrix.</returns>
        public static Matrix4 CreateRotationAbitraryAxis(Axis rotationAxis, double angle)
        {
            Vector3 v = rotationAxis.FromOrigin;
            Vector3 dirCosines = v / Math.Pow((v * v).SumComponents(), 1d / 2d);
            Matrix4 translation = new(new double[Size, Size]
            {
                { 1, 0, 0, 0 },
                { 0, 1, 0, 0 },
                { 0, 0, 1, 0 },
                { -rotationAxis.Point0.X, -rotationAxis.Point0.Y, -rotationAxis.Point0.Z, 1 },
            });

            Matrix4 translationInverse = new(new double[Size, Size]
            {
                { 1, 0, 0, 0 },
                { 0, 1, 0, 0 },
                { 0, 0, 1, 0 },
                { rotationAxis.Point0.X, rotationAxis.Point0.Y, rotationAxis.Point0.Z, 1 },
            });

            double d = Math.Sqrt((dirCosines.Y * dirCosines.Y) + (dirCosines.Z * dirCosines.Z));

            Matrix4 rotationX = new(new double[Size, Size]
            {
                { 1, 0, 0, 0 },
                { 0, dirCosines.Z / d, dirCosines.Y / d, 0 },
                { 0, -dirCosines.Y / d, dirCosines.Z / d, 0 },
                { 0, 0, 0, 1 },
            });

            Matrix4 rotationXInverse = new(new double[Size, Size]
            {
                { 1, 0, 0, 0 },
                { 0, dirCosines.Z / d, -dirCosines.Y / d, 0 },
                { 0, dirCosines.Y / d, dirCosines.Z / d, 0 },
                { 0, 0, 0, 1 },
            });

            Matrix4 rotationY = new(new double[Size, Size]
            {
                { d, 0, dirCosines.X, 0 },
                { 0,  1, 0, 0 },
                { -dirCosines.X, 0, d, 0 },
                { 0, 0, 0, 1 },
            });

            Matrix4 rotationYInverse = new(new double[Size, Size]
            {
                { d, 0, -dirCosines.X, 0 },
                { 0,  1, 0, 0 },
                { dirCosines.X, 0, d, 0 },
                { 0, 0, 0, 1 },
            });

            Matrix4 rotationZ = CreateRotationZ(angle);

            Matrix4 mat = translation * rotationX * rotationY * rotationZ * rotationYInverse * rotationXInverse * translationInverse;

            return mat;
        }

        /// <summary>
        /// Builds a scale matrix.
        /// </summary>
        /// <param name="scaleFactor">The scaling factor for this matrix.</param>
        /// <returns>The scale matrix.</returns>
        public static Matrix4 CreateScale(double scaleFactor)
        {
            return new Matrix4(new double[,]
            {
                { 1, 0, 0, 0 },
                { 0, 1, 0, 0 },
                { 0, 0, 1, 0 },
                { 0, 0, 0, scaleFactor },
            });
        }

        /// <summary>
        /// Creates an perspective projection matrix.
        /// </summary>
        /// <param name="left">Left edge of the view frustum.</param>
        /// <param name="right">Right edge of the view frustum.</param>
        /// <param name="bottom">Bottom edge of the view frustum.</param>
        /// <param name="top">Top edge of the view frustum.</param>
        /// <param name="depthNear">Distance to the near clip plane.</param>
        /// <param name="depthFar">Distance to the far clip plane.</param>
        /// <returns>A perspective projection matrix.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown under the following conditions:
        ///  <list type="bullet">
        ///  <item>depthNear is negative or zero</item>
        ///  <item>depthFar is negative or zero</item>
        ///  <item>depthNear is larger than depthFar</item>
        ///  </list>
        /// </exception>
        /// <remarks>Taken from here : https://github.com/opentk/opentk/blob/082c8d228d0def042b11424ac002776432f44f47/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1024. </remarks>
        public static Matrix4 CreatePerspectiveOffCenter(
            double left,
            double right,
            double bottom,
            double top,
            double depthNear,
            double depthFar)
        {
            if (depthNear <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthNear));
            }

            if (depthFar <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthFar));
            }

            if (depthNear >= depthFar)
            {
                throw new ArgumentOutOfRangeException(nameof(depthNear));
            }

            double x = 2.0f * depthNear / (right - left);
            double y = 2.0f * depthNear / (top - bottom);
            double a = (right + left) / (right - left);
            double b = (top + bottom) / (top - bottom);
            double c = -(depthFar + depthNear) / (depthFar - depthNear);
            double d = -(2.0f * depthFar * depthNear) / (depthFar - depthNear);

#pragma warning disable SA1117 // Parameters should be on same line or separate lines
            return new Matrix4(
                x, 0, 0, 0,
                0, y, 0, 0,
                a, b, c, -1,
                0, 0, d, 0);
#pragma warning restore SA1117 // Parameters should be on same line or separate lines
        }

        /// <summary>
        /// Creates a perspective projection matrix.
        /// </summary>
        /// <param name="fovy">Angle of the field of view in the y direction (in radians).</param>
        /// <param name="aspect">Aspect ratio of the view (width / height).</param>
        /// <param name="depthNear">Distance to the near clip plane.</param>
        /// <param name="depthFar">Distance to the far clip plane.</param>
        /// <returns>A perspective projection matrix.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown under the following conditions:
        ///  <list type="bullet">
        ///  <item>fovy is zero, less than zero or larger than Math.PI</item>
        ///  <item>aspect is negative or zero</item>
        ///  <item>depthNear is negative or zero</item>
        ///  <item>depthFar is negative or zero</item>
        ///  <item>depthNear is larger than depthFar</item>
        ///  </list>
        /// </exception>
        public static Matrix4 CreatePerspectiveFieldOfView(double fovy, double aspect, double depthNear, double depthFar)
        {
            if (fovy <= 0 || fovy > Math.PI)
            {
                throw new ArgumentOutOfRangeException(nameof(fovy));
            }

            if (aspect <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(aspect));
            }

            if (depthNear <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthNear));
            }

            if (depthFar <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthFar));
            }

            double maxY = depthNear * Math.Tan(0.5d * fovy);
            double minY = -maxY;
            double minX = minY * aspect;
            double maxX = maxY * aspect;

            return CreatePerspectiveOffCenter(minX, maxX, minY, maxY, depthNear, depthFar);
        }

        /// <summary>
        /// Build a translation matrix.
        /// </summary>
        /// <param name="x">X translation.</param>
        /// <param name="y">Y translation.</param>
        /// <param name="z">Z translation.</param>
        /// <returns>The resulting <see cref="Matrix4"/> instance.</returns>
        public static Matrix4 CreateTranslation(double x, double y, double z)
        {
            return new(new double[,]
            {
                { 1, 0, 0, 0 },
                { 0, 1, 0, 0 },
                { 0, 0, 1, 0 },
                { x, y, z, 1 },
            });
        }

        /// <summary>
        /// Builds a translation matrix from a vector.
        /// </summary>
        /// <param name="vector">The translation vector.</param>
        /// <returns>The resulting <see cref="Matrix4"/> instance.</returns>
        public static Matrix4 CreateTranslation(Vector3 vector) => CreateTranslation(vector.X, vector.Y, vector.Z);

        /// <summary>
        /// Builds a world space to view space matrix.
        /// </summary>
        /// <param name="eye">Eye (camera) position in world space.</param>
        /// <param name="target">Target position in world space.</param>
        /// <param name="up">Up vector in world space (should not be parallel to the camera direction, that is target - eye).</param>
        /// <returns>A Matrix that transforms world space to camera (view) space.</returns>
        public static Matrix4 LookAt(Vector3 eye, Vector3 target, Vector3 up)
        {
            Vector3 z = Vector3.Normalize(eye - target);
            Vector3 x = Vector3.Normalize(up.Cross(z));
            Vector3 y = Vector3.Normalize(z.Cross(x));

            Matrix4 result = new(new double[,]
            {
                { x.X, y.X, z.X, 0d },
                { x.Y, y.Y, z.Y, 0d },
                { x.Z, y.Z, z.Z, 0d },
                {
                    -((x.X * eye.X) + (x.Y * eye.Y) + (x.Z * eye.Z)),
                    -((y.X * eye.X) + (y.Y * eye.Y) + (y.Z * eye.Z)),
                    -((z.X * eye.X) + (z.Y * eye.Y) + (z.Z * eye.Z)),
                    1,
                },
            });

            return result;
        }

        /// <summary>
        /// Computes the transpose of the given matrix.
        /// </summary>
        /// <param name="mat">Matrix to compute the transpose of.</param>
        /// <returns>The transpose of the given matrix.</returns>
        public static Matrix4 Transpose(Matrix4 mat)
        {
            double[,] newMat = new double[Size, Size];

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[j, i] = mat[i, j];
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Divides each element in the Matrix by the <see cref="Determinant"/>.
        /// </summary>
        /// <param name="mat">Matrix to normalize.</param>
        /// <returns>The normalized matrix.</returns>
        public static Matrix4 Normalize(Matrix4 mat)
        {
            double determinant = mat.Determinant;
            return mat / determinant;
        }

        /// <summary>
        /// Computes the transpose of this matrix.
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public Matrix4 Transpose() => Transpose(this);

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            // Check if the other object is a Matrix4
            if (other is Matrix4 mat)
            {
                return mat.Equals(this);
            }

            return false;
        }

        /// <inheritdoc/>
        public bool Equals(Matrix4 other)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (values[i, j] != other.values[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Comparator within a tolerance.
        /// </summary>
        /// <param name="other">Matrix to compare with.</param>
        /// <param name="tolerance">Tolerance to apply in the comparison.</param>
        /// <returns>True if two matrices are equal withing a tolerance.</returns>
        public bool Equals(object other, double tolerance)
        {
            // Check if the other object is a Matrix4
            if (other is Matrix4 mat)
            {
                return mat.Equals(this, tolerance);
            }

            return false;
        }

        /// <summary>
        /// Comparator within a tolerance.
        /// </summary>
        /// <param name="other">Matrix to compare with.</param>
        /// <param name="tolerance">Tolerance to apply in the comparison.</param>
        /// <returns>True if two matrices are equal withing a tolerance.</returns>
        public bool Equals(Matrix4 other, double tolerance)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (values[i, j].AlmostEqualsWithAbsTolerance(other.values[i, j], tolerance))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(values);

        /// <summary>
        /// Inverts this instance.
        /// </summary>
        /// <returns>The inverted matrix.</returns>
        /// <exception cref="InvalidOperationException">Thrown when the matrix is singular and cannot be inverted.</exception>
        /// <remarks> Taken from here : https://github.com/opentk/opentk/blob/master/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1582 .</remarks>
        public Matrix4 Invert()
        {
#pragma warning disable SA1407 // Arithmetic expressions should declare precedence
            double a = V00, b = V10, c = V20, d = V30;
            double e = V01, f = V11, g = V21, h = V31;
            double i = V02, j = V12, k = V22, l = V32;
            double m = V03, n = V13, o = V23, p = V33;

            double kp_lo = k * p - l * o;
            double jp_ln = j * p - l * n;
            double jo_kn = j * o - k * n;
            double ip_lm = i * p - l * m;
            double io_km = i * o - k * m;
            double in_jm = i * n - j * m;

            double a11 = +(f * kp_lo - g * jp_ln + h * jo_kn);
            double a12 = -(e * kp_lo - g * ip_lm + h * io_km);
            double a13 = +(e * jp_ln - f * ip_lm + h * in_jm);
            double a14 = -(e * jo_kn - f * io_km + g * in_jm);

            double det = a * a11 + b * a12 + c * a13 + d * a14;

            if (Math.Abs(det) < double.Epsilon)
            {
                throw new InvalidOperationException("Matrix is singular and cannot be inverted.");
            }

            double invDet = 1.0d / det;

            Vector4 row0 = new Vector4(a11, a12, a13, a14) * invDet;

            Vector4 row1 = new Vector4(
                -(b * kp_lo - c * jp_ln + d * jo_kn),
                +(a * kp_lo - c * ip_lm + d * io_km),
                -(a * jp_ln - b * ip_lm + d * in_jm),
                +(a * jo_kn - b * io_km + c * in_jm)) * invDet;

            double gp_ho = g * p - h * o;
            double fp_hn = f * p - h * n;
            double fo_gn = f * o - g * n;
            double ep_hm = e * p - h * m;
            double eo_gm = e * o - g * m;
            double en_fm = e * n - f * m;

            Vector4 row2 = new Vector4(
                +(b * gp_ho - c * fp_hn + d * fo_gn),
                -(a * gp_ho - c * ep_hm + d * eo_gm),
                +(a * fp_hn - b * ep_hm + d * en_fm),
                -(a * fo_gn - b * eo_gm + c * en_fm)) * invDet;

            double gl_hk = g * l - h * k;
            double fl_hj = f * l - h * j;
            double fk_gj = f * k - g * j;
            double el_hi = e * l - h * i;
            double ek_gi = e * k - g * i;
            double ej_fi = e * j - f * i;

            Vector4 row3 = new Vector4(
                -(b * gl_hk - c * fl_hj + d * fk_gj),
                +(a * gl_hk - c * el_hi + d * ek_gi),
                -(a * fl_hj - b * el_hi + d * ej_fi),
                +(a * fk_gj - b * ek_gi + c * ej_fi)) * invDet;

            Vector4[] vectors = { row0, row1, row2, row3 };
            return new(vectors, false);
#pragma warning restore SA1407 // Arithmetic expressions should declare precedence
        }

        /// <inheritdoc/>
        public override string ToString() => $"[{V00}, {V01}, {V02}, {V03}]{Environment.NewLine}" +
            $"[{V10}, {V11}, {V12}, {V13}]{Environment.NewLine}" +
            $"[{V20}, {V21}, {V22}, {V23}]{Environment.NewLine}" +
            $"[{V30}, {V31}, {V32}, {V33}]{Environment.NewLine}";

        #endregion
    }
}