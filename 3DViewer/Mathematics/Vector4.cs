﻿/**
 * @file Vector4.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Vector4 struct.
 * @version 1.0
 * @date 30.03.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Mathematics
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Represents a 3D position vector with homogeneous coordinates.
    /// </summary>
    /// <remarks>
    /// Based on this tutorial : https://www.codeproject.com/articles/17425/a-vector-type-for-c.
    /// </remarks>
    [ImmutableObject(true)]
    public struct Vector4
        : IEquatable<Vector4>
    {
        #region Constants

        /// <summary>
        /// Size of the Vector4 struct in bytes.
        /// </summary>
        public static readonly int SizeInBytes = Unsafe.SizeOf<Vector4>();

        /// <summary>
        /// An origin vector. Is equal to Vector(0, 0, 0, 0). Same as Zero.
        /// </summary>
        public static readonly Vector4 Origin = new(0, 0, 0, 0);

        /// <summary>
        /// Represents the X axis. Is equal to Vector(1, 0, 0, 0).
        /// </summary>
        public static readonly Vector4 XAxis = new(1, 0, 0, 0);

        /// <summary>
        /// Represents the Y axis. Is equal to Vector(0, 1, 0, 0).
        /// </summary>
        public static readonly Vector4 YAxis = new(0, 1, 0, 0);

        /// <summary>
        /// Represents the Z axis. Is equal to Vector(0, 0, 1, 0).
        /// </summary>
        public static readonly Vector4 ZAxis = new(0, 0, 1, 0);

        /// <summary>
        /// Represents the W axis. Is equal to Vector(0, 0, 0, 1).
        /// </summary>
        public static readonly Vector4 WAxis = new(0, 0, 0, 1);

        /// <summary>
        /// A vector with every components equal to double.MinValue.
        /// </summary>
        public static readonly Vector4 MinValue = new(double.MinValue, double.MinValue, double.MinValue, double.MinValue);

        /// <summary>
        /// A vector with every components equal to double.MaxValue.
        /// </summary>
        public static readonly Vector4 MaxValue = new(double.MaxValue, double.MaxValue, double.MaxValue, double.MaxValue);

        /// <summary>
        /// A vector with every components equal to double.Epsilon.
        /// </summary>
        public static readonly Vector4 Epsilon = new(double.Epsilon, double.Epsilon, double.Epsilon, double.Epsilon);

        /// <summary>
        /// An instance with all components set to 1.
        /// </summary>
        public static readonly Vector4 One = new(1, 1, 1, 1);

        /// <summary>
        /// A zero vector. Is equal to Vector(0, 0, 0, 0). Same as Origin.
        /// </summary>
        public static readonly Vector4 Zero = Origin;

        /// <summary>
        /// A vector with every components equal to double.NaN.
        /// </summary>
        public static readonly Vector4 NaN = new(double.NaN, double.NaN, double.NaN, double.NaN);

        private const string FourComponentsException = "Array must contain exactly four components, [x, y, z, w]";

        #endregion Constants

        /// <summary>
        /// The X component of the vector.
        /// </summary>
        private readonly double x;

        /// <summary>
        /// The Y component of the vector.
        /// </summary>
        private readonly double y;

        /// <summary>
        /// The Z component of the vector.
        /// </summary>
        private readonly double z;

        /// <summary>
        /// The W component of the vector.
        /// </summary>
        private readonly double w;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4"/> struct.
        /// </summary>
        /// <param name="x">X component of the vector.</param>
        /// <param name="y">Y component of the vector.</param>
        /// <param name="z">Z component of the vector.</param>
        /// <param name="w">W component of the vector.</param>
        public Vector4(double x, double y, double z, double w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4"/> struct.
        /// </summary>
        /// <param name="xyzw">Array with the X, Y, Z and W components fo the vector.</param>
        /// <exception cref="ArgumentException">
        /// Thrown if the array argument does not contain exactly four components.
        /// </exception>
        public Vector4(double[] xyzw)
        {
            if (xyzw.Length == 4)
            {
                x = xyzw[0];
                y = xyzw[1];
                z = xyzw[2];
                w = xyzw[3];
            }
            else
            {
                throw new ArgumentException(FourComponentsException);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4"/> struct.
        /// </summary>
        /// <param name="v1">Vector4 representing the new values for the vector.</param>
        public Vector4(Vector4 v1)
            : this(v1.X, v1.Y, v1.Z, v1.W)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4"/> struct.
        /// </summary>
        /// <param name="v1"><see cref="Vector3"/> to create the <see cref="Vector4"/> from.</param>
        public Vector4(Vector3 v1)
            : this(v1, 0d)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4"/> struct.
        /// </summary>
        /// <param name="v1">The vector 3 that contains the X, Y and Z components of that vector.</param>
        /// <param name="w">The W element of the new vector.</param>
        public Vector4(Vector3 v1, double w)
            : this(v1.X, v1.Y, v1.Z, w)
        {
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the X component of the vector.
        /// </summary>
        public double X => x;

        /// <summary>
        /// Gets the Y component of the vector.
        /// </summary>
        public double Y => y;

        /// <summary>
        /// Gets the Z component of the vector.
        /// </summary>
        public double Z => z;

        /// <summary>
        /// Gets the W component of the vector.
        /// </summary>
        public double W => w;

        /// <summary>
        /// Gets the vector as an array.
        /// </summary>
        public double[] Array => new double[] { X, Y, Z, W };

        /// <summary>
        /// Gets the magnitude (aka. length or absolute value) of the vector.
        /// </summary>
        public double Magnitude => Math.Sqrt(MagnitudeSquared);

        /// <summary>
        /// Gets the squared magnitude of this vector, can be used for better performance than Magnitude.
        /// </summary>
        public double MagnitudeSquared => SquareComponents().SumComponents();

        /// <summary>
        /// Gets this vector but normalized.
        /// </summary>
        public Vector4 Normalized => Normalize();

        #endregion Properties

        #region Operators

        /// <summary>
        /// An index accessor for a vector, mapping index [0] -> X, [1] -> Y, [2] -> Z and [3] -> W.
        /// </summary>
        /// <param name="index">The array index referring to a component within the vector (i.e. x, y, z, w).</param>
        /// <returns>Returns X if 0, Y if 1, Z if 2 and W if 3.</returns>
        public double this[int index] => index switch
        {
            0 => X,
            1 => Y,
            2 => Z,
            3 => W,
            _ => throw new ArgumentException(FourComponentsException),
        };

        public static Vector4 operator +(Vector4 v1, Vector4 v2) => new(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, v1.W + v2.W);

        public static Vector4 operator -(Vector4 v1, Vector4 v2) => new(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, v1.W - v2.W);

        public static Vector4 operator -(Vector4 v1) => new(-v1.X, -v1.Y, -v1.Z, -v1.W);

        public static Vector4 operator +(Vector4 v1) => new(+v1.X, +v1.Y, +v1.Z, +v1.W);

        public static bool operator <(Vector4 v1, Vector4 v2) => v1.MagnitudeSquared < v2.MagnitudeSquared;

        public static bool operator <=(Vector4 v1, Vector4 v2) => v1.MagnitudeSquared <= v2.MagnitudeSquared;

        public static bool operator >(Vector4 v1, Vector4 v2) => v1.MagnitudeSquared > v2.MagnitudeSquared;

        public static bool operator >=(Vector4 v1, Vector4 v2) => v1.MagnitudeSquared >= v2.MagnitudeSquared;

        public static bool operator ==(Vector4 v1, Vector4 v2) => v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z && v1.W == v2.W;

        public static bool operator !=(Vector4 v1, Vector4 v2) => !(v1 == v2);

        public static Vector4 operator *(Vector4 v1, double s2) => new(v1.X * s2, v1.Y * s2, v1.Z * s2, v1.W * s2);

        public static Vector4 operator *(double s1, Vector4 v2) => v2 * s1;

        public static Vector4 operator /(Vector4 v1, double s2) => new(v1.X / s2, v1.Y / s2, v1.Z / s2, v1.W / s2);

        /// <summary>
        /// Multiplies a vector with a matrix.
        /// </summary>
        /// <param name="v1">The vector to multiply by the matrix.</param>
        /// <param name="m2">The matrix to multiply the vector by.</param>
        /// <returns>The vector that results from the multiplication.</returns>
        public static Vector4 operator *(Vector4 v1, Matrix4 m2)
        {
            return new(
                (v1.X * m2[0, 0]) + (v1.Y * m2[1, 0]) + (v1.Z * m2[2, 0]) + (v1.W * m2[3, 0]),
                (v1.X * m2[0, 1]) + (v1.Y * m2[1, 1]) + (v1.Z * m2[2, 1]) + (v1.W * m2[3, 1]),
                (v1.X * m2[0, 2]) + (v1.Y * m2[1, 2]) + (v1.Z * m2[2, 2]) + (v1.W * m2[3, 2]),
                (v1.X * m2[0, 3]) + (v1.Y * m2[1, 3]) + (v1.Z * m2[2, 3]) + (v1.W * m2[3, 3]));
        }

        #endregion Operators

        /// <summary>
        /// Determines the dot product of two vectors.
        /// </summary>
        /// <param name="other">The vector to multiply by.</param>
        /// <returns>Returns a scalar representing the dot product of the two vectors.</returns>
        public double Dot(Vector4 other) => (X * other.X) + (Y * other.Y) + (Z * other.Z) + (W * other.W);

        /// <summary>
        /// Squares the vector's components.
        /// </summary>
        /// <returns>The vector with his components squared.</returns>
        public Vector4 SquareComponents() => new(X * X, Y * Y, Z * Z, W * W);

        /// <summary>
        /// Sums the components of the vector.
        /// </summary>
        /// <returns>Returns the sum of the components of the vectors.</returns>
        public double SumComponents() => X + Y + Z + W;

        /// <summary>
        /// Normalizes the vector.
        /// </summary>
        /// <returns>Returns the normalized vector.</returns>
        public Vector4 Normalize()
        {
            double scale = 1d / Magnitude;
            return new(X * scale, Y * scale, Z * scale, W * scale);
        }

        /// <summary>
        /// Converts the Vector4 to physical coords in a Vector3.
        /// Divides the X, Y and Z components by W.
        /// If W = 0, it just gives the X, Y and Z components in a Vector3.
        /// </summary>
        /// <returns>Returns the physical coordinates of this vector.</returns>
        public Vector3 ToPhysicalCoords() => W != 0 ? new(X / W, Y / W, Z / W) : new(X, Y, Z);

        /// <summary>
        /// Converts this vector to a <see cref="PointF"/>.
        /// Firsts gets his physical coordinates, then only keeps the X and Y components.
        /// </summary>
        /// <returns>A <see cref="PointF"/> containing the physical X and Y coordinates of the vector.</returns>
        public PointF ToPointF() => ToPhysicalCoords().ToPointF();

        /// <inheritdoc/>
        public bool Equals(Vector4 other) => this == other;

        /// <inheritdoc/>
        public override bool Equals(object obj) => obj is Vector4 vector && Equals(vector);

        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(X, Y, Z, W);
    }
}