﻿/**
 * @file Vector3.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Vector3 struct.
 * @version 1.0
 * @date 04.01.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Mathematics
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Text;
    using ThreeDViewer.Utils;

    /// <summary>
    /// Represents a 3D vector.
    /// </summary>
    /// <remarks>
    /// Based on this tutorial : https://www.codeproject.com/articles/17425/a-vector-type-for-c.
    /// </remarks>
    [ImmutableObject(true)]
    public struct Vector3
        : IComparable, IComparable<Vector3>, IEquatable<Vector3>
    {
        #region Constants

        /// <summary>
        /// An origin vector. Is equal to Vector(0, 0, 0). Same as Zero.
        /// </summary>
        public static readonly Vector3 Origin = new(0, 0, 0);

        /// <summary>
        /// Represents the X axis. Is equal to Vector(1, 0, 0).
        /// </summary>
        public static readonly Vector3 XAxis = new(1, 0, 0);

        /// <summary>
        /// Represents the Y axis. Is equal to Vector(0, 1, 0).
        /// </summary>
        public static readonly Vector3 YAxis = new(0, 1, 0);

        /// <summary>
        /// Represents the Z axis. Is equal to Vector(0, 0, 1).
        /// </summary>
        public static readonly Vector3 ZAxis = new(0, 0, 1);

        /// <summary>
        /// A vector with every components equal to double.MinValue.
        /// </summary>
        public static readonly Vector3 MinValue = new(double.MinValue, double.MinValue, double.MinValue);

        /// <summary>
        /// A vector with every components equal to double.MaxValue.
        /// </summary>
        public static readonly Vector3 MaxValue = new(double.MaxValue, double.MaxValue, double.MaxValue);

        /// <summary>
        /// A vector with every components equal to double.Epsilon.
        /// </summary>
        public static readonly Vector3 Epsilon = new(double.Epsilon, double.Epsilon, double.Epsilon);

        /// <summary>
        /// A zero vector. Is equal to Vector(0, 0, 0). Same as Origin.
        /// </summary>
        public static readonly Vector3 Zero = Origin;

        /// <summary>
        /// A vector with every components equal to double.NaN.
        /// </summary>
        public static readonly Vector3 NaN = new(double.NaN, double.NaN, double.NaN);

        private const string ThreeComponentsException = "Array must contain exactly three components, [x, y, z]";
        private const string NonVectorComparaison = "Cannot compare a Vector3 to a non-Vector3\nThe argument provided is a type of {0}";
        private const string NormalizeZero = "Cannot normalize a vector when it's magnitude is zero";
        private const string NormalizeNaN = "Cannot normalize a vector when it's magnitude is NaN";
        private const string NormalizeInf = "Cannot normalize a vector when it's magnitude is infinite";
        private const string NegativeMagnitude = "The magnitude of a Vector3 must be a positive value, (i.e. greater than 0)";
        private const string OriginVectorMagnitude = "Cannot change the magnitude of Vector3(0, 0, 0)";
        private const string ArgumentValue = "The argument provided has a value of ";
        private const string InterpolationRange = "Control parameter must be a value between 0 & 1";
        #endregion

        /// <summary>
        /// The X component of the vector.
        /// </summary>
        private readonly double x;

        /// <summary>
        /// The Y component of the vector.
        /// </summary>
        private readonly double y;

        /// <summary>
        /// The Z component of the vector.
        /// </summary>
        private readonly double z;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3"/> struct.
        /// </summary>
        /// <param name="x">X component of the vector.</param>
        /// <param name="y">Y component of the vector.</param>
        /// <param name="z">Z component of the vector.</param>
        public Vector3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3"/> struct.
        /// </summary>
        /// <param name="xyz">Array with the X, Y and Z components fo the vector.</param>
        /// <exception cref="System.ArgumentException">
        /// Thrown if the array argument does not contain exactly three components.
        /// </exception>
        public Vector3(double[] xyz)
        {
            if (xyz.Length == 3)
            {
                x = xyz[0];
                y = xyz[1];
                z = xyz[2];
            }
            else
            {
                throw new ArgumentException(ThreeComponentsException);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3"/> struct.
        /// </summary>
        /// <param name="v1">Vector3 representing the new values for the vector.</param>
        public Vector3(Vector3 v1)
        {
            x = v1.X;
            y = v1.Y;
            z = v1.Z;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the X component of the vector.
        /// </summary>
        public double X => x;

        /// <summary>
        /// Gets the Y component of the vector.
        /// </summary>
        public double Y => y;

        /// <summary>
        /// Gets the Z component of the vector.
        /// </summary>
        public double Z => z;

        /// <summary>
        /// Gets the vector as an array.
        /// </summary>
        public double[] Array => new double[] { X, Y, Z };

        /// <summary>
        /// Gets the magnitude (aka. length or absolute value) of the vector.
        /// </summary>
        public double Magnitude => Math.Sqrt(SqrMagnitude);

        /// <summary>
        /// Gets the squared magnitude of this vector, can be used for better performance than Magnitude.
        /// </summary>
        public double SqrMagnitude => SqrComponents().SumComponents();

        /// <summary>
        /// Gets this vector but normalized.
        /// </summary>
        public Vector3 Normalized => Normalize(this);
        #endregion

        #region Operators

        /// <summary>
        /// An index accessor for a vector, mapping index [0] -> X, [1] -> Y and  [2] -> Z.
        /// </summary>
        /// <param name="index">The array index referring to a component within the vector (i.e. x, y, z).</param>
        /// <returns>Returns X if 0, Y if 1 and Z if 2.</returns>
        public double this[int index] => index switch
        {
            0 => X,
            1 => Y,
            2 => Z,
            _ => throw new ArgumentException(ThreeComponentsException),
        };

        public static Vector3 operator +(Vector3 v1, Vector3 v2) => new(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);

        public static Vector3 operator -(Vector3 v1, Vector3 v2) => new(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);

        public static Vector3 operator -(Vector3 v1) => new(-v1.X, -v1.Y, -v1.Z);

        public static Vector3 operator +(Vector3 v1) => new(+v1.X, +v1.Y, +v1.Z);

        public static bool operator <(Vector3 v1, Vector3 v2) => v1.SqrMagnitude < v2.SqrMagnitude;

        public static bool operator <=(Vector3 v1, Vector3 v2) => v1.SqrMagnitude <= v2.SqrMagnitude;

        public static bool operator >(Vector3 v1, Vector3 v2) => v1.SqrMagnitude > v2.SqrMagnitude;

        public static bool operator >=(Vector3 v1, Vector3 v2) => v1.SqrMagnitude >= v2.SqrMagnitude;

        public static bool operator ==(Vector3 v1, Vector3 v2) => v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z;

        public static bool operator !=(Vector3 v1, Vector3 v2) => !(v1 == v2);

        public static Vector3 operator *(Vector3 v1, double s2) => new(v1.X * s2, v1.Y * s2, v1.Z * s2);

        public static Vector3 operator *(double s1, Vector3 v2) => v2 * s1;

        public static Vector3 operator /(Vector3 v1, double s2) => new(v1.X / s2, v1.Y / s2, v1.Z / s2);

        public static Vector3 operator *(Vector3 v1, Vector3 v2) => new(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
        #endregion

        #region Static Methods

        /// <summary>
        /// Determines the dot product of two vectors.
        /// </summary>
        /// <param name="v1">The vector to multiply.</param>
        /// <param name="v2">The vector to multiply by.</param>
        /// <returns>Returns a scalar representing the dot product of the two vectors.</returns>
        public static double Dot(Vector3 v1, Vector3 v2) => (v1.X * v2.X) + (v1.Y * v2.Y) + (v1.Z * v2.Z);

        /// <summary>
        /// Determine the cross product of two Vectors.
        /// Determine the vector product.
        /// Determine the normal vector (Vector3 90° to the plane).
        /// </summary>
        /// <param name="v1">The vector to multiply.</param>
        /// <param name="v2">The vector to multiply by.</param>
        /// <returns>Vector3 representig the cross product of the two vectors.</returns>
        public static Vector3 Cross(Vector3 v1, Vector3 v2) => new((v1.Y * v2.Z) - (v1.Z * v2.Y), (v1.Z * v2.X) - (v1.X * v2.Z), (v1.X * v2.Z) - (v1.Y * v2.X));

        /// <summary>
        /// Checks if the vector is a unit vector.
        /// Checks if the vector has be normalized.
        /// Checks if the vector has a magnitude of 1.
        /// </summary>
        /// <param name="v1">The vector to be checked for normalization.</param>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public static bool IsUnitVector(Vector3 v1) => v1.Magnitude == 1;

        /// <summary>
        /// Checks if the vector is a unit vector within a tolerance.
        /// Checks if the vector has been normalized within a tolerance.
        /// Checks if the vector has a magnitude of 1 within a tolerance.
        /// </summary>
        /// <param name="v1">The vector to be checked for normalization.</param>
        /// <param name="tolerance">The tolerance to use when comparing the magnitude.</param>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public static bool IsUnitVector(Vector3 v1, double tolerance) => v1.Magnitude.AlmostEqualsWithAbsTolerance(1, tolerance);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <param name="v1">The vector to be normalized.</param>
        /// <returns>Returns the normalized vector.</returns>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a zero magnitude vector is attempted.
        /// </exception>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a NaN magnitude vector is attempted.
        /// </exception>
        /// <remarks>
        /// Exceptions will be thrown if the vector being normalized has a magnitude of 0 or of NaN.
        /// </remarks>
        public static Vector3 Normalize(Vector3 v1)
        {
            double magnitude = v1.Magnitude;

            if (double.IsInfinity(magnitude))
            {
                v1 = NormalizeSpecialCasesOrOrigional(v1);

                if (v1.IsNaN())
                {
                    // If this wasn't a special case, throw an exception
                    throw new NormalizeVectorException(NormalizeInf);
                }
            }

            // Check that we are not trying to normalize a vector of magnitude 0
            if (magnitude == 0)
            {
                throw new NormalizeVectorException(NormalizeZero);
            }

            // Check that we are not trying to normalize a vector of magnitude NaN
            if (double.IsNaN(magnitude))
            {
                throw new NormalizeVectorException(NormalizeNaN);
            }

            return NormalizeOrNaN(v1);
        }

        /// <summary>
        /// Checks if any component of a vector is Not A Number (NaN).
        /// </summary>
        /// <param name="v1">The vector checked for NaN components.</param>
        /// <returns>Returns true if the vector has NaN components.</returns>
        public static bool IsNaN(Vector3 v1) => double.IsNaN(v1.X) || double.IsNaN(v1.Y) || double.IsNaN(v1.Z);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <param name="v1">The vector to be normalized.</param>
        /// <returns>Returns Vector (0,0,0) if the magnitude is zero, Vector (NaN, NaN, NaN) if magnitude is NaN, or normalized vector.</returns>
        public static Vector3 NormalizeOrDefault(Vector3 v1)
        {
            // Special cases
            v1 = NormalizeSpecialCasesOrOrigional(v1);

            // Check that we are not trying to normalize with a vector of magnitude 0. If yes, we return v(0, 0, 0).
            if (v1.Magnitude == 0)
            {
                return Origin;
            }

            // Check that we are not trying to normalize a vector with a NaN component. If yes, we return v(NaN, NaN, NaN).
            if (v1.IsNaN())
            {
                return NaN;
            }

            return NormalizeOrNaN(v1);
        }

        /// <summary>
        /// Computes the angle between two vectors.
        /// </summary>
        /// <param name="v1">The vector to discern the angle from.</param>
        /// <param name="v2">The vector to discern the angle to.</param>
        /// <returns>Returns the angle between the two vectors.</returns>
        public static double Angle(Vector3 v1, Vector3 v2)
        {
            if (v1 == v2)
            {
                return 0;
            }

            return Math.Acos(Math.Min(1.0f, NormalizeOrDefault(v1).Dot(NormalizeOrDefault(v2))));
        }

        /// <summary>
        /// Finds the absolute value of a vector.
        /// Finds the magnitude of a vector.
        /// </summary>
        /// <param name="v1">The vector to get the magnitude from.</param>
        /// <returns>Returns a double representig the magnitude of the vector.</returns>
        public static double Abs(Vector3 v1) => v1.Magnitude;

        /// <summary>
        /// Computes the distance between two vectors.
        /// </summary>
        /// <param name="v1">The vector to find the distance from.</param>
        /// <param name="v2">The vector to find the distance to.</param>
        /// <returns>Returns the distance between two vectors.</returns>
        public static double Distance(Vector3 v1, Vector3 v2) => Math.Sqrt(
            ((v1.X - v2.X) * (v1.X - v2.X)) +
            ((v1.Y - v2.Y) * (v1.Y - v2.Y)) +
            ((v1.Z - v2.Z) * (v1.Z - v2.Z)));

        /// <summary>
        /// Sums the components of the vector.
        /// </summary>
        /// <param name="v1">The vector whose scalar components to sum.</param>
        /// <returns>The sums of the vector's X, Y and Z components.</returns>
        public static double SumComponents(Vector3 v1) => v1.X + v1.Y + v1.Z;

        /// <summary>
        /// The individual multiplication to a power of the vectors's components.
        /// </summary>
        /// <param name="v1">The vector whose scalar components to multiply by a power.</param>
        /// <param name="power">The power by which to multiply the components.</param>
        /// <returns>The multiplied vector.</returns>
        public static Vector3 PowComponents(Vector3 v1, double power) => new(Math.Pow(v1.X, power), Math.Pow(v1.Y, power), Math.Pow(v1.Z, power));

        /// <summary>
        /// The individual square root of a vectors's components.
        /// </summary>
        /// <param name="v1">The vector whose scalar components to square root.</param>
        /// <returns>The rooted vector.</returns>
        public static Vector3 SqrtComponents(Vector3 v1) => new(Math.Sqrt(v1.X), Math.Sqrt(v1.Y), Math.Sqrt(v1.Z));

        /// <summary>
        /// The vectors's components squared.
        /// </summary>
        /// <param name="v1">The vector whose scalar components are to square.</param>
        /// <returns>The squared vectors.</returns>
        public static Vector3 SqrComponents(Vector3 v1) => new(v1.X * v1.X, v1.Y * v1.Y, v1.Z * v1.Z);

        /// <summary>
        /// Checks if a face normal vector represents back face.
        /// Checks if a face is visible, given the line of sight.
        /// </summary>
        /// <param name="normal">The vector representing the face normal Vector3.</param>
        /// <param name="lineOfSight">The unit vector representing the direction of sight from a virtual camera.</param>
        /// <returns>True if the vector (as a normal) represents a back-face.</returns>
        public static bool IsBackFace(Vector3 normal, Vector3 lineOfSight) => normal.Dot(lineOfSight) < 0;
        #endregion

        #region Methods

        /// <summary>
        /// Determines the dot product of two vectors.
        /// </summary>
        /// <param name="other">The vector to multiply by.</param>
        /// <returns>Returns a scalar representing the dot product of the two vectors.</returns>
        public double Dot(Vector3 other) => Dot(this, other);

        /// <summary>
        /// Determine the cross product of two Vectors.
        /// Determine the vector product.
        /// Determine the normal vector (Vector3 90° to the plane).
        /// </summary>
        /// <param name="other">The vector to multiply by.</param>
        /// <returns>Vector3 representig the cross product of the two vectors.</returns>
        public Vector3 Cross(Vector3 other) => Cross(this, other);

        /// <summary>
        /// Checks if the vector is a unit vector.
        /// Checks if the vector has be normalized.
        /// Checks if the vector has a magnitude of 1.
        /// </summary>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public bool IsUnitVector() => IsUnitVector(this);

        /// <summary>
        /// Checks if the vector is a unit vector within a tolerance.
        /// Checks if the vector has been normalized within a tolerance.
        /// Checks if the vector has a magnitude of 1 within a tolerance.
        /// </summary>
        /// <param name="tolerance">The tolerance to use when comparing the magnitude.</param>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public bool IsUnitVector(double tolerance) => IsUnitVector(this, tolerance);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <returns>Returns the normalized vector.</returns>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a zero magnitude vector is attempted.
        /// </exception>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a NaN magnitude vector is attempted.
        /// </exception>
        /// <remarks>
        /// Exceptions will be thrown if the vector being normalized has a magnitude of 0 or of NaN.
        /// </remarks>
        public Vector3 Normalize() => Normalize(this);

        /// <summary>
        /// Checks if any component of a vector is Not A Number (NaN).
        /// </summary>
        /// <returns>Returns true if the vector has NaN components.</returns>
        public bool IsNaN() => IsNaN(this);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <returns>Returns Vector (0, 0, 0) if the magnitude is zero, Vector (NaN, NaN, NaN) if magnitude is NaN, or normalized vector.</returns>
        public Vector3 NormalizeOrDefault() => NormalizeOrDefault(this);

        /// <summary>
        /// Computes the distance between two vectors.
        /// </summary>
        /// <param name="other">The vector to find the distance to.</param>
        /// <returns>Returns the distance between two vectors.</returns>
        public double Distance(Vector3 other) => Distance(this, other);

        /// <summary>
        /// Sums the components of the vector.
        /// </summary>
        /// <returns>The sums of the vector's X, Y and Z components.</returns>
        public double SumComponents() => SumComponents(this);

        /// <summary>
        /// The vectors's components squared.
        /// </summary>
        /// <returns>The squared vectors.</returns>
        public Vector3 SqrComponents() => SqrComponents(this);

        /// <inheritdoc/>
        public override string ToString() => $"X: {x}, Y: {y}, Z: {z}, ";

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            // Check if the other object is a Vector3
            if (other is Vector3 vector)
            {
                return vector.Equals(this);
            }

            return false;
        }

        /// <inheritdoc/>
        public bool Equals(Vector3 other) => X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);

        /// <summary>
        /// Comparator within a tolerance.
        /// </summary>
        /// <param name="other">The other object to compare to.</param>
        /// <param name="tolerance">The tolerance to use when comparing the vector components.</param>
        /// <returns>True if two objects are Vector3s and are equal within a tolerance.</returns>
        public bool Equals(object other, double tolerance)
        {
            if (other is Vector3 vector)
            {
                return Equals(vector, tolerance);
            }

            return false;
        }

        /// <summary>
        /// Comparator within a tolerance.
        /// </summary>
        /// <param name="other">The other vector to compare to.</param>
        /// <param name="tolerance">The tolerance to use when comparing the vector components.</param>
        /// <returns>True if two vectors are equal within a tolerance.</returns>
        public bool Equals(Vector3 other, double tolerance) => X.AlmostEqualsWithAbsTolerance(other.X, tolerance) &&
            Y.AlmostEqualsWithAbsTolerance(other.Y, tolerance) &&
            Z.AlmostEqualsWithAbsTolerance(other.Z, tolerance);

        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(X, Y, Z);

        /// <inheritdoc/>
        public int CompareTo(object other)
        {
            if (other is Vector3 vector)
            {
                return CompareTo(vector);
            }

            throw new ArgumentException(string.Format(NonVectorComparaison, other.GetType()), nameof(other));
        }

        /// <inheritdoc/>
        public int CompareTo(Vector3 other)
        {
            if (this < other)
            {
                return -1;
            }
            else if (this > other)
            {
                return 1;
            }

            return 0;
        }

        /// <summary>
        /// Compares the magnitude of this instance against the magnitude of the supplied vector.
        /// </summary>
        /// <param name="other">The vector to compare this instance with.</param>
        /// <param name="tolerance">Tolerence to use when comparing the two vectors.</param>
        /// <returns>
        /// -1: The magnitude of this instance is less than the others magnitude.
        /// 0: The magnitude of this instance equals the magnitude of the other.
        /// 1: The magnitude of this instance is greater than the magnitude of the other.
        /// </returns>
        /// <remarks>
        /// Comparing two vectors has no meaning, we are comparing the magnitude of two vectors for convinience.
        /// It would be more accurate to compare the magnitudes explicitly using v1.Magnitude.CompareTo(v2.Magnitude).
        /// </remarks>
        public int CompareTo(object other, double tolerance)
        {
            if (other is Vector3 vector)
            {
                return CompareTo(vector, tolerance);
            }

            throw new ArgumentException(string.Format(NonVectorComparaison, other.GetType()), nameof(other));
        }

        /// <summary>
        /// Checks if a face normal vector represents back face.
        /// Checks if a face is visible, given the line of sight.
        /// </summary>
        /// <param name="lineOfSight">The unit vector representing the direction of sight from a virtual camera.</param>
        /// <returns>True if the vector (as a normal) represents a back-face.</returns>
        public bool IsBackFace(Vector3 lineOfSight) => IsBackFace(this, lineOfSight);

        /// <summary>
        /// Converts this vector to a <see cref="PointF"/>.
        /// Only keeps the X and Y components of this vector.
        /// </summary>
        /// <returns>A point with the X and Y components of this vector.</returns>
        public PointF ToPointF() => new((float)X, (float)Y);

        /// <summary>
        /// Converts this vector to a <see cref="Vector4"/>.
        /// The <see cref="Vector4.W"/> component will be equal to 1.
        /// </summary>
        /// <returns>The Vector3 as a <see cref="Vector4"/>.</returns>
        public Vector4 ToVector4() => new(x, y, z, 1);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <param name="v1">The vector to be normalized.</param>
        /// <returns>The normalized vector3 or vector (NaN,NaN,NaN) if the magnitude is 0 or NaN.</returns>
        private static Vector3 NormalizeOrNaN(Vector3 v1)
        {
            // Find the inverse of the vectors magnitude
            double inverse = 1 / v1.Magnitude;

            // Multiply each component by the inverse of the magnitude
            return new Vector3(v1.X * inverse, v1.Y * inverse, v1.Z * inverse);
        }

        /// <summary>
        /// This method is used to normalize special cases of vectors where the components are infinite and/or zero only.
        /// Other vectors will be returned un-normalized.
        /// </summary>
        /// <param name="v1">The vector to be normalized if it is a special case.</param>
        /// <returns>Normialized special case vectors, NaN or the origional vector.</returns>
        private static Vector3 NormalizeSpecialCasesOrOrigional(Vector3 v1)
        {
            if (double.IsInfinity(v1.Magnitude))
            {
                double x = v1.X == 0 ? 0 :
                    v1.X == -0 ? -0 :
                    double.IsPositiveInfinity(v1.X) ? 1 :
                    double.IsNegativeInfinity(v1.X) ? -1 :
                    double.NaN;

                double y = v1.Y == 0 ? 0 :
                    v1.Y == -0 ? -0 :
                    double.IsPositiveInfinity(v1.Y) ? 1 :
                    double.IsNegativeInfinity(v1.Y) ? -1 :
                    double.NaN;

                double z = v1.Z == 0 ? 0 :
                    v1.Z == -0 ? -0 :
                    double.IsPositiveInfinity(v1.Z) ? 1 :
                    double.IsNegativeInfinity(v1.Z) ? -1 :
                    double.NaN;

                return new(x, y, z);
            }

            return v1;
        }

        #endregion
    }
}
