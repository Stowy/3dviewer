﻿/**
* @file Axis.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Axis struct.
* @version 1.0
* @date 21.05.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace ThreeDViewer.Mathematics
{
    /// <summary>
    /// Struct representing an axis via two points.
    /// </summary>
    public struct Axis
    {
        private readonly Vector3 point0;
        private readonly Vector3 point1;
        private readonly Vector3 fromOrigin;

        /// <summary>
        /// Initializes a new instance of the <see cref="Axis"/> struct.
        /// </summary>
        /// <param name="point0">First point of the axis.</param>
        /// <param name="point1">Second point of the axis.</param>
        public Axis(Vector3 point0, Vector3 point1)
        {
            this.point0 = point0;
            this.point1 = point1;
            fromOrigin = this.point1 - this.point0;
        }

        /// <summary>
        /// Gets the first point of the axis.
        /// </summary>
        public Vector3 Point0 => point0;

        /// <summary>
        /// Gets the second point of the axis.
        /// </summary>
        public Vector3 Point1 => point1;

        /// <summary>
        /// Gets the axis with a point starting from the origin.
        /// Calculated by : <see cref="Point1"/> - <see cref="Point0"/>.
        /// </summary>
        public Vector3 FromOrigin => fromOrigin;
    }
}
