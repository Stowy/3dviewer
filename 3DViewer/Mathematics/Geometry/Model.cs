﻿/**
 * @file Model.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Model struct.
 * @version 1.0
 * @date 27.04.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Mathematics.Geometry
{
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents a 3D Model.
    /// </summary>
    public class Model
    {
        private const int VerticesStart = 10;

        private Face[] faces;

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        /// <param name="faces">Faces of the model.</param>
        public Model(Face[] faces) => this.faces = faces;

        /// <summary>
        /// Gets or sets the faces of the model.
        /// </summary>
        public Face[] Faces { get => faces; set => faces = value; }

        /// <summary>
        /// Gets the center of the model.
        /// </summary>
        public Vector3 Center
        {
            get
            {
                Vector3 center = new(0, 0, 0);
                foreach (Face face in faces)
                {
                    center += face.Center;
                }

                center /= faces.Length;
                return center;
            }
        }

        public static Model operator *(Model model, Matrix4 matrix)
        {
            Face[] newFaces = new Face[model.Faces.Length];
            Parallel.For(0, newFaces.Length, (i) =>
            {
                newFaces[i] = model.Faces[i] * matrix;
            });

            return new(newFaces);
        }

        /// <summary>
        /// Creates a model from a ply file.
        /// </summary>
        /// <param name="path">The path of the ply file.</param>
        /// <returns>The model in the ply file.</returns>
        /// <exception cref="FileNotFoundException">Thrown if the file doesn't exist.</exception>
        /// <exception cref="FileFormatException">Thrown if the file is not in a good format.</exception>
        public static Model FromPly(string path)
        {
            // Check if the file exists
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"The file \"{path}\" doesn't exists.");
            }

            // Read the file
            string[] lines = File.ReadAllLines(path);

            // Check if it has the "ply" magic value
            if (lines[0] != "ply")
            {
                throw new FileFormatException("The file is not in the ply format");
            }

            // Get the line telling the format of the file
            string[] formatLine = lines[1].Split(' ');

            // Check if the format is ascii
            if (formatLine.Length < 3 || formatLine[0] != "format" || formatLine[1] != "ascii")
            {
                throw new FileFormatException("The file is not in ascii format");
            }

            // Read the number of vertices and faces
            string[] verticesNumberLine = lines[3].Split(' ');
            string[] facesNumberLine = lines[7].Split(' ');

            int nbrVertices = int.Parse(verticesNumberLine[2]);
            int nbrFaces = int.Parse(facesNumberLine[2]);

            // Read the vertices
            Vector4[] vertices = new Vector4[nbrVertices];

            Parallel.For(VerticesStart, VerticesStart + nbrVertices, (int i) =>
            {
                string[] verticesLine = lines[i].Split(' ');
                double x = double.Parse(verticesLine[0], CultureInfo.InvariantCulture);
                double y = double.Parse(verticesLine[1], CultureInfo.InvariantCulture);
                double z = double.Parse(verticesLine[2], CultureInfo.InvariantCulture);

                vertices[i - VerticesStart] = new Vector4(x, y, z, 1);
            });

            // Read the face
            Face[] faces = new Face[nbrFaces];
            int facesStart = VerticesStart + nbrVertices;

            Parallel.For(facesStart, facesStart + nbrFaces, (int i) =>
            {
                // The this line of faces
                string[] faceLine = lines[i].Split(' ');

                // Read the number of vertices in this face
                int nbrVerticesInFace = int.Parse(faceLine[0]);

                // Read the index of the vertices and get them from the vertices list
                Vector4[] verticesInFace = new Vector4[nbrVerticesInFace];

                for (int j = 0; j < nbrVerticesInFace; j++)
                {
                    verticesInFace[j] = vertices[int.Parse(faceLine[j + 1])];
                }

                // Store the face in the faces list
                faces[i - facesStart] = new Face(verticesInFace);
            });

            return new Model(faces);
        }

        /// <summary>
        /// Creates a rectangular cuboid from the specified sizes centered on itself.
        /// </summary>
        /// <param name="height">Height of the cuboid.</param>
        /// <param name="width">Width of the cuboid.</param>
        /// <param name="depth">Depth of the cuboid.</param>
        /// <returns>The created cuboid.</returns>
        public static Model CreateRectangularCuboidCentered(double height, double width, double depth)
        {
            double halfHeight = height / 2;
            double halfWidth = width / 2;
            double halfDepth = depth / 2;

            Vector4[] vertices = new Vector4[8]
            {
                new(-halfWidth, -halfHeight, -halfDepth, 1),
                new(-halfWidth, -halfHeight, halfDepth, 1),
                new(-halfWidth, halfHeight, halfDepth, 1),
                new(-halfWidth, halfHeight, -halfDepth, 1),
                new(halfWidth, halfHeight, halfDepth, 1),
                new(halfWidth, halfHeight, -halfDepth, 1),
                new(halfWidth, -halfHeight, halfDepth, 1),
                new(halfWidth, -halfHeight, -halfDepth, 1),
            };

            Face[] faces = new Face[6]
            {
                new(new Vector4[4] { vertices[0], vertices[1], vertices[2], vertices[3] }),
                new(new Vector4[4] { vertices[3], vertices[2], vertices[4], vertices[5] }),
                new(new Vector4[4] { vertices[5], vertices[4], vertices[6], vertices[7] }),
                new(new Vector4[4] { vertices[7], vertices[6], vertices[1], vertices[0] }),
                new(new Vector4[4] { vertices[3], vertices[5], vertices[7], vertices[0] }),
                new(new Vector4[4] { vertices[4], vertices[2], vertices[1], vertices[6] }),
            };

            return new(faces);
        }

        /// <summary>
        /// Creates a rectangular cuboid from the specified sizes centered on its corner.
        /// </summary>
        /// <param name="height">Height of the cuboid.</param>
        /// <param name="width">Width of the cuboid.</param>
        /// <param name="depth">Depth of the cuboid.</param>
        /// <returns>The created cuboid.</returns>
        public static Model CreateRectangularCuboidOffCenter(double height, double width, double depth)
        {
            Vector4[] vertices = new Vector4[8]
            {
                new(0, 0, 0, 1),
                new(0, 0, depth, 1),
                new(0, height, depth, 1),
                new(0, height, 0, 1),
                new(width, height, depth, 1),
                new(width, height, 0, 1),
                new(width, 0, depth, 1),
                new(width, 0, 0, 1),
            };

            Face[] faces = new Face[6]
            {
                new(new Vector4[4] { vertices[0], vertices[1], vertices[2], vertices[3] }),
                new(new Vector4[4] { vertices[3], vertices[2], vertices[4], vertices[5] }),
                new(new Vector4[4] { vertices[5], vertices[4], vertices[6], vertices[7] }),
                new(new Vector4[4] { vertices[7], vertices[6], vertices[1], vertices[0] }),
                new(new Vector4[4] { vertices[3], vertices[5], vertices[7], vertices[0] }),
                new(new Vector4[4] { vertices[4], vertices[2], vertices[1], vertices[6] }),
            };

            return new(faces);
        }
    }
}
