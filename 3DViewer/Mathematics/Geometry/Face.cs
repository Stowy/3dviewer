﻿/**
* @file Face.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Face struct.
* @version 1.0
* @date 06.04.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace ThreeDViewer.Mathematics.Geometry
{
    using System;
    using System.Drawing;
    using ThreeDViewer.Mathematics;

    /// <summary>
    /// Represents one face of a 3D model.
    /// </summary>
    public class Face
    {
        /// <summary>
        /// The minimum amounts of vertices a face should have.
        /// </summary>
        public const int MinVertices = 3;

        private Vector4[] vertices;
        private Color color;

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        /// <param name="vertices">The vertices that the face should contain.</param>
        /// <exception cref="ArgumentException">Thrown when there are less than 3 points.</exception>
        public Face(Vector4[] vertices)
            : this(vertices, Color.White)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        /// <param name="vertices">The vertices that the face should contain.</param>
        /// <param name="color">The color of the face.</param>
        /// <exception cref="ArgumentException">Thrown when there are less than 3 points.</exception>
        public Face(Vector4[] vertices, Color color)
        {
            if (vertices.Length < MinVertices)
            {
                throw new ArgumentOutOfRangeException(nameof(vertices));
            }

            this.vertices = vertices;
            this.color = color;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        /// <param name="vertex0">First point.</param>
        /// <param name="vertex1">Second point.</param>
        /// <param name="vertex2">Third point.</param>
        /// <param name="color">The color of the face.</param>
        public Face(Vector4 vertex0, Vector4 vertex1, Vector4 vertex2, Color color)
            : this(new Vector4[3] { vertex0, vertex1, vertex2 }, color)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> class.
        /// </summary>
        /// <param name="vertex0">First point.</param>
        /// <param name="vertex1">Second point.</param>
        /// <param name="vertex2">Third point.</param>
        /// <param name="vertex3">Fourth point.</param>
        /// <param name="color">The color of the face.</param>
        public Face(Vector4 vertex0, Vector4 vertex1, Vector4 vertex2, Vector4 vertex3, Color color)
            : this(new Vector4[4] { vertex0, vertex1, vertex2, vertex3 }, color)
        {
        }

        /// <summary>
        /// Gets the normal of the face.
        /// </summary>
        public Vector3 Normal
        {
            get
            {
                Vector3 normal = new(0, 0, 0);

                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3 current = vertices[i].ToPhysicalCoords();
                    Vector3 next = vertices[(i + 1) % vertices.Length].ToPhysicalCoords();

                    double x = normal.X + ((current.Y - next.Y) * (current.Z + next.Z));
                    double y = normal.Y + ((current.Z - next.Z) * (current.X + next.X));
                    double z = normal.Z + ((current.X - next.X) * (current.Y + next.Y));

                    normal = new(x, y, z);
                }

                return normal.NormalizeOrDefault();
            }
        }

        /// <summary>
        /// Gets the center of the face.
        /// </summary>
        public Vector3 Center
        {
            get
            {
                Vector3 center = new(0, 0, 0);
                for (int i = 0; i < vertices.Length; i++)
                {
                    center += vertices[i].ToPhysicalCoords();
                }

                center /= vertices.Length;

                return center;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the vertices in this face are clockwise.
        /// </summary>
        public bool IsClockwise
        {
            get
            {
                double sum = 0d;
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3 current = vertices[i].ToPhysicalCoords();

                    // Modulo so that when we are at the last item, next is the first one
                    Vector3 next = vertices[(i + 1) % vertices.Length].ToPhysicalCoords();

                    sum += (next.X - current.X) * (next.Y + current.Y);
                }

                return sum > 0d;
            }
        }

        /// <summary>
        /// Gets or sets the vertices that are presents in the face.
        /// </summary>
        public Vector4[] Vertices
        {
            get => vertices;
            set => vertices = value;
        }

        /// <summary>
        /// Gets or sets the color of the face.
        /// </summary>
        public Color Color
        {
            get => color;
            set => color = value;
        }

        /// <summary>
        /// Gets or sets the vertex at this index.
        /// </summary>
        /// <param name="index">Index of the vertex.</param>
        /// <returns>The vertex at the index.</returns>
        public Vector4 this[int index]
        {
            get => vertices[index];
            set => vertices[index] = value;
        }

        /// <summary>
        /// Multiplies the vertices of the face by the matrix.
        /// </summary>
        /// <param name="f1">The face to multiply.</param>
        /// <param name="m2">The matrix to multiply the face by.</param>
        /// <returns>The multiplied face.</returns>
        public static Face operator *(Face f1, Matrix4 m2)
        {
            Vector4[] newVertices = new Vector4[f1.Vertices.Length];
            for (int i = 0; i < newVertices.Length; i++)
            {
                newVertices[i] = f1.Vertices[i] * m2;
            }

            return new(newVertices, f1.Color);
        }

        /// <summary>
        /// Converts the vertices of this face to physical coordinates.
        /// </summary>
        /// <returns>The vertices of this face as physical coordinates.</returns>
        public Vector3[] ToPhysicalCoords()
        {
            Vector3[] physical = new Vector3[Vertices.Length];

            for (int i = 0; i < Vertices.Length; i++)
            {
                physical[i] = Vertices[i].ToPhysicalCoords();
            }

            return physical;
        }

        /// <summary>
        /// Updates the color (lighting) of the face.
        /// </summary>
        /// <param name="lightDirection">The direction vector of the light.</param>
        /// <param name="lightColor">The color the face will be at 100% lighting.</param>
        public void LightUp(Vector3 lightDirection, Color lightColor)
        {
            double dot = Normal.Dot(lightDirection);

            Color color = Color;
            if (dot > 0)
            {
                int red = (int)(lightColor.R * dot) % 255;
                int green = (int)(lightColor.G * dot) % 255;
                int blue = (int)(lightColor.B * dot) % 255;

                color = Color.FromArgb(red, green, blue);
            }

            Color = color;
        }
    }
}
