﻿/**
 * @file NormalizeVectorException.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the NormalizeVectorException class.
 * @version 1.0
 * @date 11.01.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace ThreeDViewer.Mathematics
{
    using System;

    /// <summary>
    /// Exception that can happen during the normalization of a vector.
    /// </summary>
    [Serializable]
    internal class NormalizeVectorException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NormalizeVectorException"/> class.
        /// </summary>
        public NormalizeVectorException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalizeVectorException"/> class.
        /// </summary>
        /// <param name="message">Message to display in the exception.</param>
        public NormalizeVectorException(string message)
            : base(message)
        {
        }
    }
}
